#include "pluto.h"
//#include "locals.h"
#include "cooling_defs.h"

SELF_SPEC SelfSpec;

void ReadOpacityTables()
{
if (SelfSpec.opac==NULL){

 SelfSpec.opac = ARRAY_4D(NX3_TOT, NX2_TOT, NX1_TOT, 1000, double);	/* nx3,nx2,nx1,nu */
 SelfSpec.opac_ions = ARRAY_4D(NX3_TOT, NX2_TOT, NX1_TOT, 1000, double);	/* nx3,nx2,nx1,nu */
 SelfSpec.opac_dust_ext = ARRAY_4D(NX3_TOT, NX2_TOT, NX1_TOT, 1000, double);	/* nx3,nx2,nx1,nu */
 SelfSpec.Temp = ARRAY_3D(NX3_TOT, NX2_TOT, NX1_TOT, double);
 SelfSpec.nH1  = ARRAY_3D(NX3_TOT, NX2_TOT, NX1_TOT, double);
 SelfSpec.mu   = ARRAY_3D(NX3_TOT, NX2_TOT, NX1_TOT, double);
 SelfSpec.nu   = ARRAY_1D(1000, double);
 SelfSpec.dnu  = ARRAY_1D(1000, double);
 SelfSpec.g_opac = ARRAY_2D(31, 1000, double);
 SelfSpec.logTop = ARRAY_1D(31, double);

 SelfSpec.Inu  = ARRAY_4D(NX3_TOT, NX2_TOT, NX1_TOT, 1000, double);
 SelfSpec.eps_nu = ARRAY_4D(NX3_TOT, NX2_TOT, NX1_TOT, 1000, double);

 SelfSpec.N_totphot      = calloc(NX1_TOT, sizeof(double));
 SelfSpec.N_uvH          = calloc(NX1_TOT, sizeof(double));
 SelfSpec.N_uvHe         = calloc(NX1_TOT, sizeof(double));

/*
 int tidx, cout, i;
 double nu, opacity, T; 
 char infile[100];
 FILE *fin;
*/

/* Reading Opacity tables is needed if the opacity is calculated from equil conditions.
 * In the current version, opacity is calculated instantaneiusly from the ion fracs
 * and their photo-ionisation cross section */
/*
 for (tidx=0; tidx<=30; tidx++){
     T = 3.0 + tidx*0.1;
     SelfSpec.logTop[tidx] = T;
     sprintf(infile, "input_tables/opacities/T_%1.1f.opac",T);
     fin = fopen(infile, "r");
     cout = 0;
     while (fscanf(fin, "%lf %lf", &nu, &opacity) != EOF){
	 SelfSpec.nu[cout]		= nu;
         SelfSpec.g_opac[tidx][cout] 	= opacity;
         cout ++;
     }
     fclose(fin);
 }
*/

} /* end of if opac==null */

}

/* ********************************************** */


