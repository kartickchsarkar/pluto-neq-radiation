#include "pluto.h"
//#include "locals.h"
#include "cooling_defs.h"

DUST Dust;
SELF_SPEC SelfSpec;

/* ************************************************** */
void EvolveDust(const Data *d, double dt)
/* 
 * Evolves dust size (by thermal sputtering) following Draine's book 
 * Eq 25.13. Although the valid temp range is 1e5<T<1e9 we extend this
 * to T>5e3K. This is a crude way to treat dust but will be updated
 * with proper rates in future versions 
 * ************************************************************** */
{
 double a0, ap, T6, nH1;
 int i, j, k;
 j = JBEG;	k=KBEG;

 for (i=0; i<NX1_TOT; i++){
     T6 = SelfSpec.Temp[k][j][i]/1e6;
     nH1 = SelfSpec.nH1[k][j][i];
     a0 = d->Vc[TRC][k][j][i];
     ap = a0 - 1e-6/(1+pow(T6, -3.0)) * nH1 * dt/CONST_yr;
     d->Vc[TRC][k][j][i] = MAX(ap,0.0);
 } 

}


/* ********************************************** */
void GetDustProperties()
/*
 * Reads Dust propoerties like albedo (w), mean scattering angle (cos\theta)
 * and extinction cross-section from coarse grained table obtained from
 * Draine's webpage https://www.astro.princeton.edu/~draine/dust/dustmix.html .
 * The adopted table has R_v = 3.1 (similar to many directions in MW).
 * ********************************************************************** */
{
 Dust.albedo	= calloc(1000, sizeof(double));
 Dust.costheta	= calloc(1000, sizeof(double));
 Dust.sigma_ext	= calloc(1000, sizeof(double));
 Dust.sigma_abs	= calloc(1000, sizeof(double));
 Dust.sigma_scat	= calloc(1000, sizeof(double));
 Dust.sigma_pr	= calloc(1000, sizeof(double));

 double x1, x2, x3, x4, x5, nu[100];
 int i, n;

 n = 0;
 FILE *fdust = fopen(g_dustfile,"r");
 while ( fscanf(fdust, "%lf %lf %lf %lf %lf", &x1, &x2, &x3, &x4, &x5) != EOF){
     nu[n] 				= x1;
     Dust.albedo[n]	= x2;
     Dust.costheta[n] 	= x3;
     Dust.sigma_ext[n]	= x4;  
     n++;
 }
 fclose(fdust);

 for (i=0; i<n; i++){ 
     Dust.sigma_scat[i]	= Dust.albedo[i]*Dust.sigma_ext[i];
     Dust.sigma_abs[i]	= Dust.sigma_ext[i]-Dust.sigma_scat[i];
     Dust.sigma_pr[i]	= Dust.sigma_abs[i] + (1.-Dust.costheta[i])*Dust.sigma_scat[i];
 }

/*
 print1("> Reading dust prop ...\n");
 for (i=0; i<n; i++) print1 ("%2.4e  %2.4f  %2.4f  %2.4e\n", 
			nu[i], Dust.albedo[i], Dust.costheta[i],
			Dust.sigma_ext[i]);
*/

}
