#include "pluto.h"
#include "cooling_defs.h"
//#include "locals.h"

PHOTO_COEFF PhotoCoeffs;

void get_ion(int nv, int *el_id, int *ion_id)
{
 /* for CI, el_id = 6 and ion_id = 0 */

 *el_id = ions_nz[elem_part[nv]];
 *ion_id = (int)rad_rec_z[nv] - 1;
}


/* ************** Get UVB spectra *********************** */
void GetUVB_spec()
{
 /* Allocate the arrays. This is done only once as this whole function is 
  * called only once at the start of the simulation (so extra calculations are
  * okay)
  * UVB_Jnu	= UVB spectra, erg/s/cm^2/Sr/Hz
  * totJnu	= instanteneous spectra including any time varying field
  * nu		= edges of the frequency bands
  * dnu		= width of the frequency bands d\nu_{i} = \nu_{i+1}-\nu_i
  * Pi_Rate	= instanteneous PI rate for an ion at that location
  * UVB_PiRate  = PI rate for the UVB for an ion. constant over time
  * Sigma	= PI cross sections for an ion at a subshell
		= the subshell value goes from 1 to 7
  * ********************************************************************* */
 PhotoCoeffs.UVB_Jnu         = calloc(1000, sizeof(double));
 PhotoCoeffs.locJnu          = ARRAY_1D(1000, double);
 PhotoCoeffs.totJnu          = ARRAY_2D(NX1_TOT, 1000, double);
 PhotoCoeffs.Fnu             = ARRAY_2D(NX1_TOT, 1000, double);
 PhotoCoeffs.radforce        = ARRAY_1D(NX1_TOT, double);
 PhotoCoeffs.psi             = ARRAY_3D(NX1_TOT, 1000, NRAY, double);
 PhotoCoeffs.nu              = calloc(1000, sizeof(double));
 PhotoCoeffs.nuc             = calloc(1000, sizeof(double));
 PhotoCoeffs.dnu             = calloc(1000, sizeof(double));
 PhotoCoeffs.Pi_Rate         = calloc(NIONS,sizeof(double));
 PhotoCoeffs.UVB_PiRate      = calloc(NIONS,sizeof(double));
 PhotoCoeffs.Sigma	     = ARRAY_3D(NIONS, 8, 1000, double);
 PhotoCoeffs.shellintg_Sigma	     = ARRAY_2D(NIONS, 1000, double);

 GetIP();		/* Get Ionisation potentials */

 double nu, dnuP;
 int i, k, Nnu, N, cout=0, dcout=0;
 double x1, x2, x3, *nua, *Jnu;
 char HM12file[128];

 /* using the HM12 file. The frequency bands should match with the
  * bands given in emissivity files. */
 sprintf(HM12file,"%s/HM12.out",g_tablesdir);
 FILE *fUVB = fopen(HM12file,"r");
 if (fUVB == NULL){
     print("  !Error: HM12.out not found in tables' directory\n   Exiting ...\n");
     QUIT_PLUTO(1);
 }

 while( fscanf(fUVB,"%lf %lf %lf", &x1, &x2, &x3) != EOF ) {
     PhotoCoeffs.nu[cout]	= x1;	// frequency in Hz
     PhotoCoeffs.UVB_Jnu[cout]	= x2; 
     cout++;
 }
 
 PhotoCoeffs.Nnu  = Nnu = cout;

 for (k=0; k<Nnu; k++) {
     PhotoCoeffs.locJnu[k]  = 0.0; 
     if (k<Nnu-1){
	 PhotoCoeffs.dnu[k] = PhotoCoeffs.nu[k+1]-PhotoCoeffs.nu[k];
	 PhotoCoeffs.nuc[k] = (PhotoCoeffs.nu[k+1]+PhotoCoeffs.nu[k])/2.0;
     }
 }

 fclose(fUVB);

 /* Set the initial value of the arrays to zero, for sanity. 
  * This will prevent the code to read junk values when the arrays
  * are not yet  calculated. For example, at step=0 or at restart.
  * Even if not, there is not harm in taking caution :-) */

 for (i=0; i<NX1_TOT; i++){
     PhotoCoeffs.radforce[i]	= 0.0;
     for (k=0; k<Nnu; k++){
         PhotoCoeffs.totJnu[i][k]	= 0.0;
	 PhotoCoeffs.Fnu[i][k]		= 0.0;
     }
 }

 return;
}

void GetPhotoIonRate(double *nu, double *Jnu)
/* 
 * Calculates the photoionisation rate for given intensity field
 * nu -> Frequency in Hz
 * Jnu -> radiation flux in erg/s/cm^2/Hz/Sr 
 * Returns the photo-ionisation rate in s^{-1}
 * ************************************************************* */
{
 int i, j, Nnu, shell, el_id, ion_id, nv;
 double nu_mid, Sum, totPiRate;
 Nnu = PhotoCoeffs.Nnu;

 if (PhotoCoeffs.Gamma == NULL){
     PhotoCoeffs.Gamma = ARRAY_3D(27, 27, 8, double);

     for (nv=0; nv<NIONS; nv++){ /* start with zero */
         get_ion(nv, &el_id, &ion_id);
         for (shell=1; shell<=7; shell++){
             PhotoCoeffs.Gamma[el_id][ion_id][shell] = 0.0;
         }
         PhotoCoeffs.Pi_Rate[nv] = 0.0;
     }
 } /* Gamma == NULL ends here */
 
 if (!g_include_pi) return;

 for (nv=0; nv<NIONS; nv++){
   get_ion(nv, &el_id, &ion_id);
   totPiRate = 0.0;
   for (shell=1; shell<=7; shell++){
     Sum = 0.0;
     for (i=0; i<Nnu-1; i++){
       nu_mid = PhotoCoeffs.nuc[i];
       Sum    += Jnu[i]*PhotoCoeffs.Sigma[nv][shell][i]
	         *PhotoCoeffs.dnu[i]/nu_mid;
     }
     PhotoCoeffs.Gamma[el_id][ion_id][shell] = 4.0*CONST_PI/CONST_h*Sum;
     totPiRate += PhotoCoeffs.Gamma[el_id][ion_id][shell];
   } 
   PhotoCoeffs.Pi_Rate[nv] = totPiRate;

   /* Checking if the photo-rates are okay.Checked on 10.03.2019 
    * They are consistent with HM12 paper.
   if (nv==0) print ("Tot PI rate for HI is %2.4e /s\n", PhotoCoeffs.Pi_Rate[nv]);
   if (nv==1) print ("Tot PI rate for HeI is %2.4e /s\n", PhotoCoeffs.Pi_Rate[nv]);
   if (nv==2) print ("Tot PI rate for HeII is %2.4e /s\n", PhotoCoeffs.Pi_Rate[nv]);
   */
 }

 return;
}

/*
void GetIonisationPotentials()
*/
/* *****************************************
 * Save the ionisation potentials of all ions in Hz
 * ************************************************** */
/*
{
 int nv;
 double eVtoHz;
 eVtoHz = CONST_eV/CONST_h;
 
 for (nv=0; nv<NIONS; nv++) PhotoCoeffs.nu0[nv] = 1e5*eVtoHz;		// making unavailable nu0 to be very high 
 
 PhotoCoeffs.nu0[H_I]	= 13.5984	* eVtoHz;

 PhotoCoeffs.nu0[He_I]	= 24.5874	* eVtoHz;
 PhotoCoeffs.nu0[He_II]	= 54.416	* eVtoHz;

 PhotoCoeffs.nu0[C_I]	= 11.2603	* eVtoHz;
 PhotoCoeffs.nu0[C_II]	= 24.383	* eVtoHz;
 PhotoCoeffs.nu0[C_III]	= 47.888	* eVtoHz;
 PhotoCoeffs.nu0[C_IV]	= 64.494	* eVtoHz;
 PhotoCoeffs.nu0[C_V]	= 392.089	* eVtoHz;
 PhotoCoeffs.nu0[C_VI]	= 489.993	* eVtoHz;

 PhotoCoeffs.nu0[N_I]	= 14.5341	* eVtoHz;
 PhotoCoeffs.nu0[N_II]	= 29.601	* eVtoHz;
 PhotoCoeffs.nu0[N_III]	= 47.449	* eVtoHz;
 PhotoCoeffs.nu0[N_IV]	= 77.474	* eVtoHz;
 PhotoCoeffs.nu0[N_V]	= 97.890	* eVtoHz;
 PhotoCoeffs.nu0[N_VI]	= 552.072	* eVtoHz;
 PhotoCoeffs.nu0[N_VII]	= 667.046	* eVtoHz;

 PhotoCoeffs.nu0[O_I]	= 13.6181	* eVtoHz;
 PhotoCoeffs.nu0[O_II]	= 35.121	* eVtoHz;
 PhotoCoeffs.nu0[O_III]	= 54.936	* eVtoHz;
 PhotoCoeffs.nu0[O_IV]	= 77.414	* eVtoHz;
 PhotoCoeffs.nu0[O_V]	= 113.899	* eVtoHz;
 PhotoCoeffs.nu0[O_VI]	= 138.120	* eVtoHz;
 PhotoCoeffs.nu0[O_VII]	= 739.293	* eVtoHz;
 PhotoCoeffs.nu0[O_VIII]= 871.410	* eVtoHz;

 PhotoCoeffs.nu0[Ne_I]	= 21.5645	* eVtoHz;
 PhotoCoeffs.nu0[Ne_II]	= 40.963	* eVtoHz;
 PhotoCoeffs.nu0[Ne_III]= 63.423	* eVtoHz;
 PhotoCoeffs.nu0[Ne_IV]	= 97.117	* eVtoHz;
 PhotoCoeffs.nu0[Ne_V]	= 126.247	* eVtoHz;
 PhotoCoeffs.nu0[Ne_VI]	= 154.214	* eVtoHz;
 PhotoCoeffs.nu0[Ne_VII]= 207.271	* eVtoHz;
 PhotoCoeffs.nu0[Ne_VIII]= 239.0989	* eVtoHz;
 PhotoCoeffs.nu0[Ne_IX]	= 1195.8286	* eVtoHz;
 PhotoCoeffs.nu0[Ne_X]	= 1362.1995	* eVtoHz;

 PhotoCoeffs.nu0[Mg_I]	= 7.6462	* eVtoHz;
 PhotoCoeffs.nu0[Mg_II]	= 15.035	* eVtoHz;
 PhotoCoeffs.nu0[Mg_III]= 80.144	* eVtoHz;
 PhotoCoeffs.nu0[Mg_IV]	= 109.265	* eVtoHz;
 PhotoCoeffs.nu0[Mg_V]	= 141.270	* eVtoHz;
 PhotoCoeffs.nu0[Mg_VI]	= 186.76	* eVtoHz;
 PhotoCoeffs.nu0[Mg_VII]= 225.02	* eVtoHz;
 PhotoCoeffs.nu0[Mg_VIII]= 265.96	* eVtoHz;
 PhotoCoeffs.nu0[Mg_IX]	= 328.06	* eVtoHz;
 PhotoCoeffs.nu0[Mg_X]	= 367.50	* eVtoHz;

 PhotoCoeffs.nu0[Si_I]	= 8.1517	* eVtoHz;
 PhotoCoeffs.nu0[Si_II]	= 16.6346	* eVtoHz;
 PhotoCoeffs.nu0[Si_III]= 33.493	* eVtoHz;
 PhotoCoeffs.nu0[Si_IV]	= 45.142	* eVtoHz;
 PhotoCoeffs.nu0[Si_V]	= 166.767	* eVtoHz;
 PhotoCoeffs.nu0[Si_VI]	= 205.267	* eVtoHz;
 PhotoCoeffs.nu0[Si_VII]= 246.481	* eVtoHz;
 PhotoCoeffs.nu0[Si_VIII]= 303.54	* eVtoHz;
 PhotoCoeffs.nu0[Si_IX]	= 351.12	* eVtoHz;
 PhotoCoeffs.nu0[Si_X]	= 401.37	* eVtoHz;

 PhotoCoeffs.nu0[S_I]	= 10.36		* eVtoHz;
 PhotoCoeffs.nu0[S_II]	= 23.338	* eVtoHz;
 PhotoCoeffs.nu0[S_III]	= 34.79		* eVtoHz;
 PhotoCoeffs.nu0[S_IV]	= 47.222	* eVtoHz;
 PhotoCoeffs.nu0[S_V]	= 72.594	* eVtoHz;
 PhotoCoeffs.nu0[S_VI]	= 88.053	* eVtoHz;
 PhotoCoeffs.nu0[S_VII]	= 280.948	* eVtoHz;
 PhotoCoeffs.nu0[S_VIII]= 328.28	* eVtoHz;
 PhotoCoeffs.nu0[S_IX]	= 379.55	* eVtoHz;
 PhotoCoeffs.nu0[S_X]	= 447.50	* eVtoHz;

 PhotoCoeffs.nu0[Fe_I]	= 7.9024	* eVtoHz;
 PhotoCoeffs.nu0[Fe_II]	= 16.188	* eVtoHz;
 PhotoCoeffs.nu0[Fe_III]= 30.651	* eVtoHz;
 PhotoCoeffs.nu0[Fe_IV]	= 54.801	* eVtoHz;
 PhotoCoeffs.nu0[Fe_V]	= 75.010	* eVtoHz;
 PhotoCoeffs.nu0[Fe_VI]	= 99.063	* eVtoHz;
 PhotoCoeffs.nu0[Fe_VII]= 124.976	* eVtoHz;
 PhotoCoeffs.nu0[Fe_VIII]= 151.06	* eVtoHz;
 PhotoCoeffs.nu0[Fe_IX]	= 233.6 	* eVtoHz;
 PhotoCoeffs.nu0[Fe_X]	= 262.10	* eVtoHz;

}
*/

/* Calculate the enelectron number density */
double ElectronDensity(double *v)
{
 int i;
 double N, n_el;
 static double N_rho;

 N_rho = find_N_rho();
 N	= v[RHO]*N_rho;

 n_el	= (1.0-v[HI])*elem_ab[el_H];
 for (i=1; i<NIONS; i++)
     n_el += v[i+NFLX]*(rad_rec_z[i]-1.0)*elem_ab[elem_part[i]];

 return n_el*N;
}

/* Read the Auger probabilities */
void GetAugerProb()
{
 int i, j, k, nz, state, shell, N, cout;
 double x1, x2, x3, x4, x5, x6, x7, x8, x9, x10;
 char augfile[128];

 if (PhotoCoeffs.Auger_prob == NULL){
     PhotoCoeffs.Auger_prob  = ARRAY_4D(27, 27, 8, 11, double); 
     for (nz=1; nz<27; nz++)
       for (state=0; state<=nz; state++)
	 for (shell=1; shell<=7; shell++)
	   for (N=1; N<=10; N++)
	     PhotoCoeffs.Auger_prob[nz][state][shell][N] = 0.0;
 }

 print1("> Reading Auger Probablity table.\n");

 sprintf(augfile,"%s/Auger_data.dat", g_tablesdir);
 FILE *faug = fopen(augfile,"r");
 while(fscanf(faug, "%d %d %d %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf",
     	&nz, &state, &shell, &x1, &x2, &x3, &x4, &x5, &x6, &x7, &x8, &x9, &x10) != EOF){
      PhotoCoeffs.Auger_prob[nz][state][shell][1] = x1;
      PhotoCoeffs.Auger_prob[nz][state][shell][2] = x2;
      PhotoCoeffs.Auger_prob[nz][state][shell][3] = x3;
      PhotoCoeffs.Auger_prob[nz][state][shell][4] = x4;
      PhotoCoeffs.Auger_prob[nz][state][shell][5] = x5;
      PhotoCoeffs.Auger_prob[nz][state][shell][6] = x6;
      PhotoCoeffs.Auger_prob[nz][state][shell][7] = x7;
      PhotoCoeffs.Auger_prob[nz][state][shell][8] = x8;
      PhotoCoeffs.Auger_prob[nz][state][shell][9] = x9;
      PhotoCoeffs.Auger_prob[nz][state][shell][10] = x10;
//      print("%d  %d  %d  %2.4f %2.4f %2.4f\n", nz, state, shell, PhotoCoeffs.Auger_prob[nz][state][shell][1],
//	PhotoCoeffs.Auger_prob[nz][state][shell][2], PhotoCoeffs.Auger_prob[nz][state][shell][3]);
 }
 fclose(faug);
 print1("> Auger table is read.\n");
 
  /* ------------------------------------------------------------
  * Correction for the finite number of ions less than the full
  * number of ions (if user defined). The following adds the 
  * probablity to eject more number of electrons than the maximum
  * allowed to the maximum allowed ionisation level. The probablity 
  * to eject more electrons than allowed is also made zero.
  * ------------------------------------------------------------- */
 int sp, nions, max_ne_ej;
 double prob;

 for (sp=0; sp<Nspecies; sp++){

   nz = ions_nz[sp];
   nions = ions_num[sp];
   for (state=0; state<nions; state++){
     max_ne_ej = nions-state-1; /* max num of electron possible to 
				 eject due to num of ions constrain */
     for (shell=1; shell<=7; shell++){
       for (N=max_ne_ej+1; N<=10; N++){
         PhotoCoeffs.Auger_prob[nz][state][shell][max_ne_ej] +=  
		       PhotoCoeffs.Auger_prob[nz][state][shell][N];
         PhotoCoeffs.Auger_prob[nz][state][shell][N] = 0.0;
       }
     } /* loop ends for shells of each state*/
   } /* loop ends on states for a given species */

  /* making the auger probability for higher ions zero.
   * They should not enter the equations anyway, still ... */
   for (state=nions; state<=nz; state++)
     for (shell=1; shell<=7; shell++)
       for (N=1; N<=10; N++)
         PhotoCoeffs.Auger_prob[nz][state][shell][N] = 0.0;

 } /* loop ends on individual species */


}

