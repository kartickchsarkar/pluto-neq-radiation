c234567
c  =========================================================================
      subroutine FCN (nsize, time_, x_, dx_)
c  =========================================================================
      implicit none

c     calculates the derivatives dn/dt=f(t,n,T)

c     version of May 3, 2006

c  =========================================================================
c     variable declerations:
c  =========================================================================

c     Input/Output:

      integer          nsize      !size of vector (input)
      double precision time_      !time
      double precision x_(nsize)  !abundances at t  (input)
      double precision dx_(nsize) !derivatives dn/dt calculated here (output)

      double precision   m_H        ! proton mass (gr)

c     Recombination coefficients: (common with Hagai's code)
      COMMON/RECB/re(300),unused(300),unused2(300)
      double precision re, unused, unused2
c     "re" is vector of recombination coefficients: rad + dr + 3bod
c     it is "common" with Hagai's code, and will later be set in "allrec_mat".


c     Radiation field: (need to be defined in main program).
      common/spectrum/nu(5000),Jnu(5000),VecSize  
      double precision nu  ! frquency vector (Hz)
      double precision Jnu ! Rad. field vector 
                                  !(erg s^-1 Hz^-1 sr^-1 cm^-2)
      integer VecSize    ! actual size of vectors (max size is 10,000).
c     this is only here because in is passed away to the function "Gamma"
c     which calculates the photoionization cross section.
      

c     COMMON
c --- Model (WIM/HIM) Parameters:
      common/model/PoverK,T_HIM,nH,currentT,old_time,new_time,epsl,v0, 
     +             epsl_eff      
      double precision PoverK   ! pressure in units of P/k_B (cm^-3 K).
      double precision T_HIM    ! ambient temperature.
      double precision nH       ! H number density
      double precision currentT ! present temperature
      double precision epsl
      double precision old_time
      double precision new_time
      double precision v0       ! flow velocity
      double precision epsl_eff ! effective epsl corrected when timestep forced

      double precision T    ! temperature to use in cross section calculations
      double precision Pres ! pressure at this time

      external x_tot
      double precision x_tot

c     COMMON
c --- photoionization rates for all elements ions and shells
      common/allphot_mat/allphot_vec(1:30,0:30,0:7)      
      double precision allphot_vec


c --- Gas abundances (relative to H):
      common/Gas/X_He, X_C, X_N, X_O, X_Si, X_S, X_Ne, X_Fe, X_Mg
      double precision X_He, X_C, X_N,X_O, X_Si, X_S !abundances
c --- added Feb 14, 2005
      double precision X_Ne, X_Fe
c --- added Apr 14, 2005
      double precision X_Mg

      double precision nHtot  ! hydrogen density (H_0 + H^+)
                 ! Note: Unlike here, in Dgearmain.f ntot is total 
                 ! gas density (H+He+e)

      external Gamma, colion, ct_rec, ct_ion, allrec !atomic data functions
      double precision Gamma, colion, ct_rec, ct_ion
      external he_ct_r, he_ct_i
      double precision he_ct_r, he_ct_i

      external He_alpha1, He_alphaB, H_alphaB,HeII_alphaB
      double precision He_alpha1, He_alphaB, H_alphaB,HeII_alphaB

      external Auger_pop_rate
      double precision Auger_pop_rate

c     More parameters:
      double precision n_e       ! electron density
      double precision allrec_mat(1:30,0:30) !for recombination coefficients
                                    !from Hagai's code.  
      double precision kpc2cm
      double precision k_B

c     define double precision variables for the dencities that are not being
c     calculated (particle conservation):
      double precision xHII, xHeIII, xCVII, xNVIII, xOIX, xSiXV, xSXVII 
      double precision xNeXI, xFeXXVII, xMgXIII

c     Atomic numbers:
      integer H_ , He_, C_, N_, O_, Si_, S_
      integer Ne_, Fe_, Mg_

c     deficiency in # of electrons in ionized states, relative to neutral.
      integer I,II,III,IV,V,VI,VII,VIII,IX,X,XI,XII,XIII
      integer XIV,XV,XVI,XVII
      integer XVIII,XIX,XX,XXI,XXII,XXIII,XXIV,XXV,XXVI,XXVII

c     These are for indices in the vector x: (so x_(SII) is the SII 
c     fractional abundance)
      integer H_I
      integer He_I, He_II
      integer C_I, C_II, C_III, C_IV, C_V, C_VI
      integer N_I, N_II, N_III, N_IV, N_V, N_VI, N_VII
      integer O_I, O_II, O_III, O_IV, O_V, O_VI, O_VII, O_VIII
      integer Si_I,Si_II,Si_III,Si_IV,Si_V,Si_VI,Si_VII,Si_VIII,Si_IX
      integer Si_X,Si_XI,Si_XII,Si_XIII,Si_XIV
      integer S_I, S_II, S_III, S_IV, S_V, S_VI, S_VII, S_VIII, S_IX
      integer S_X, S_XI, S_XII, S_XIII, S_XIV, S_XV, S_XVI
      integer Ne_I, Ne_II, Ne_III, Ne_IV, Ne_V, Ne_VI, Ne_VII, Ne_VIII
      integer Ne_IX, Ne_X
      integer Fe_I, Fe_II, Fe_III, Fe_IV, Fe_V, Fe_VI, Fe_VII, Fe_VIII
      integer Fe_IX, Fe_X, Fe_XI, Fe_XII, Fe_XIII, Fe_XIV, Fe_XV, Fe_XVI
      integer Fe_XVII, Fe_XVIII, Fe_XIX, Fe_XX, Fe_XXI, Fe_XXII
      integer Fe_XXIII, Fe_XXIV, Fe_XXV, Fe_XXVI
      integer Mg_I, Mg_II, Mg_III, Mg_IV, Mg_V, Mg_VI, Mg_VII, Mg_VIII
      integer Mg_IX, Mg_X, Mg_XI, Mg_XII

      external recfer1
      double precision recfer1

c      external H_sec_rate,He_sec_rate,HeII_sec_rate
c      double precision H_sec_rate,He_sec_rate,HeII_sec_rate
c      double precision sec_debug
c  =========================================================================




c  =========================================================================
c     Define parameters and initialize variables
c  =========================================================================

c --- define atomic numbers A:
      parameter (H_=1,He_=2,C_=6,N_=7,O_=8,Si_=14,S_=16)
c --- added Feb 13, 2005
      parameter (Ne_=10,Fe_=26)
      parameter (Mg_=12)

c --- state number: (electron deficiency relative to neutral state)
      parameter (I=0,II=1,III=2,IV=3,V=4,VI=5,VII=6,VIII=7)
      parameter (IX=8,X=9,XI=10,XII=11,XIII=12,XIV=13,XV=14)
      parameter (XVI=15,XVII=16)
c --- added Feb 13, 2005
      parameter (XVIII=17,XIX=18,XX=19,XXI=20,XXII=21,XXIII=22)
      parameter (XXIV=23,XXV=24,XXVI=25,XXVII=26)
c --- Now number of electrons in a state is (A - state) where A
c         is the atomic number.
c     * for example in CIV, num of electrons is 6-3=3
c                         (where A=6, and state=IV=3). 


c --- define meaningfull names for indices in vector n:
      parameter (H_I= 1)
      parameter (He_I= 2, He_II= 3)
      parameter (C_I=4 , C_II= 5, C_III= 6, C_IV= 7, C_V= 8, C_VI= 9)
      parameter (N_I= 10, N_II= 11, N_III= 12, N_IV= 13, N_V= 14)
      parameter (N_VI= 15, N_VII= 16)
      parameter (O_I= 17, O_II= 18, O_III= 19, O_IV= 20, O_V= 21)
      parameter (O_VI= 22, O_VII= 23, O_VIII= 24)
      parameter (Si_I= 25, Si_II= 26, Si_III= 27, Si_IV= 28, Si_V= 29)
      parameter (Si_VI= 30, Si_VII= 31, Si_VIII= 32, Si_IX= 33)
      parameter (Si_X= 34, Si_XI= 35, Si_XII= 36, Si_XIII= 37)
      parameter (Si_XIV= 38)
      parameter (S_I= 39, S_II= 40, S_III= 41, S_IV= 42, S_V= 43)
      parameter (S_VI= 44)
      parameter (S_VII= 45, S_VIII= 46, S_IX= 47, S_X= 48, S_XI= 49)
      parameter (S_XII= 50, S_XIII= 51, S_XIV= 52, S_XV= 53, S_XVI= 54)
c --- added Feb 13, 2005
      parameter (Ne_I=55,Ne_II=56,Ne_III=57,Ne_IV=58,Ne_V=59,Ne_VI=60)
      parameter (Ne_VII=61,Ne_VIII=62,Ne_IX=63,Ne_X=64)
      parameter (Fe_I=65,Fe_II=66,Fe_III=67,Fe_IV=68,Fe_V=69,Fe_VI=70)
      parameter (Fe_VII=71,Fe_VIII=72,Fe_IX=73,Fe_X=74,Fe_XI=75)
      parameter (Fe_XII=76,Fe_XIII=77,Fe_XIV=78,Fe_XV=79,Fe_XVI=80)
      parameter (Fe_XVII=81,Fe_XVIII=82,Fe_XIX=83,Fe_XX=84,Fe_XXI=85)
      parameter (Fe_XXII=86,Fe_XXIII=87,Fe_XXIV=88,Fe_XXV=89,Fe_XXVI=90)
c ---
      parameter (Mg_I=91,Mg_II=92,Mg_III=93,Mg_IV=94,Mg_V=95,Mg_VI=96)
      parameter (Mg_VII=97,Mg_VIII=98,Mg_IX=99,Mg_X=100,Mg_XI=101)
      parameter (Mg_XII=102)



c  =========================================================================
c  =========================================================================


c --- define parameters 
      parameter (kpc2cm=3.08e21,k_B=1.38e-16,m_H=1.6726e-24)  !k:[erg K^-1]

c --- calculate the remaining abundances (particle conservation)

c --- H II:
      xHII    = 1 - x_(H_I)

c --- He III:
      xHeIII  = X_He - x_(He_I) - x_(He_II)

c --- C VII:
      xCVII   = X_C - x_(C_I) - x_(C_II) - x_(C_III) - x_(C_IV) -
     +          x_(C_V) - x_(C_VI)

c --- N VIII:
      xNVIII  = X_N - x_(N_I) - x_(N_II) - x_(N_III) - x_(N_IV) -
     +          x_(N_V) - x_(N_VI) - x_(N_VII)

c --- O IX:
      xOIX    = X_O - x_(O_I) - x_(O_II) - x_(O_III) - x_(O_IV) -
     +          x_(O_V) - x_(O_VI) - x_(O_VII) - x_(O_VIII)

c --- added Feb 13, 2005
c --- Ne XI
      xNeXI   = X_Ne - x_(Ne_I) - x_(Ne_II) - x_(Ne_III) - 
     +          x_(Ne_IV) - x_(Ne_V) - x_(Ne_VI) - x_(Ne_VII) -
     +          x_(Ne_VIII) - x_(Ne_IX) - x_(Ne_X)

c --- Mg XIII:
      xMgXIII = X_Mg - x_(Mg_I) - x_(Mg_II) - x_(Mg_III) - 
     +          x_(Mg_IV) - x_(Mg_V) - x_(Mg_VI) - x_(Mg_VII) - 
     +          x_(Mg_VIII) - x_(Mg_IX) - x_(Mg_X) - x_(Mg_XI) - 
     +          x_(Mg_XII)

c --- Si XV:
      xSiXV   = X_Si - x_(Si_I) - x_(Si_II) - x_(Si_III) - 
     +          x_(Si_IV) - x_(Si_V) - x_(Si_VI) - x_(Si_VII) - 
     +          x_(Si_VIII) - x_(Si_IX) - x_(Si_X) - x_(Si_XI) - 
     +          x_(Si_XII) - x_(Si_XIII) - x_(Si_XIV)

c --- S XVII:
      xSXVII  = X_S - x_(S_I) - x_(S_II) - x_(S_III) - x_(S_IV) -
     +          x_(S_V) - x_(S_VI) -x_(S_VII) - x_(S_VIII) - 
     +          x_(S_IX) - x_(S_X) - x_(S_XI) - x_(S_XII) - 
     +          x_(S_XIII) - x_(S_XIV) - x_(S_XV) -x_(S_XVI)

c --- added Feb 13, 2005
c --- Fe XXVII:
      xFeXXVII = X_Fe - x_(Fe_I) - x_(Fe_II) - x_(Fe_III) - x_(Fe_IV) -
     +          x_(Fe_V) - x_(Fe_VI) -x_(Fe_VII) - x_(Fe_VIII) - 
     +          x_(Fe_IX) - x_(Fe_X) - x_(Fe_XI) - x_(Fe_XII) - 
     +          x_(Fe_XIII) - x_(Fe_XIV) - x_(Fe_XV) -x_(Fe_XVI) -
     +          x_(Fe_XVII) - x_(Fe_XVIII) - x_(Fe_XIX) - x_(Fe_XX) -
     +          x_(Fe_XXI) - x_(Fe_XXII) - x_(Fe_XXIII) - x_(Fe_XXIV) -
     +          x_(Fe_XXV) - x_(Fe_XXVI)


c --- pressure varies over time step. compute current pressure:
      if (time_.ge.new_time) then
         Pres = (1.0-epsl_eff) * PoverK
      else if (time_.gt.old_time) then
         Pres = PoverK + epsl_eff * PoverK / (old_time - new_time)
     *                       * (time_-old_time) 
      else
         Pres = PoverK
      endif
   
      
       T = Pres / (nH * x_tot(x_,nsize))


c --- total hydrogen particle density:
      nHtot = nH

c --- Electron density:  count electrons of all ions
c --- Ne & Fe added Feb 13, 2005
      n_e = nHtot * ( xHII +
     +      x_(He_II) + 2*xHeIII +
     +      x_(C_II) + 2*x_(C_III) + 3*x_(C_IV) +
     +      4*x_(C_V) + 5*x_(C_VI) + 6*xCVII +
     +      x_(N_II) + 2*x_(N_III) + 3*x_(N_IV) + 4*x_(N_V) + 
     +      5*x_(N_VI) + 6*x_(N_VII) + 7*xNVIII +
     +      x_(O_II) + 2*x_(O_III) + 3*x_(O_IV) + 4*x_(O_V) + 
     +      5*x_(O_VI) + 6*x_(O_VII) + 7*x_(O_VIII) + 8*xOIX +
     +      x_(Ne_II) + 2*x_(Ne_III) + 3*x_(Ne_IV) + 4*x_(Ne_V) + 
     +      5*x_(Ne_VI) + 6*x_(Ne_VII) + 7*x_(Ne_VIII) + 8*x_(Ne_IX) +  
     +      9*x_(Ne_X) + 10*xNeXI + 
     +      x_(Mg_II) + 2*x_(Mg_III) + 3*x_(Mg_IV) + 4*x_(Mg_V) + 
     +      5*x_(Mg_VI) + 6*x_(Mg_VII) + 7*x_(Mg_VIII) + 8*x_(Mg_IX) +  
     +      9*x_(Mg_X) + 10*x_(Mg_XI) + 11*x_(Mg_XII) + 12*xMgXIII )

      n_e = n_e + nHtot * (
     +      x_(Si_II) + 2*x_(Si_III) + 3*x_(Si_IV) + 4*x_(Si_V) + 
     +      5*x_(Si_VI) + 6*x_(Si_VII) + 7*x_(Si_VIII) + 8*x_(Si_IX) +  
     +      9*x_(Si_X) + 10*x_(Si_XI) + 11*x_(Si_XII) + 12*x_(Si_XIII) + 
     +      13*x_(Si_XIV) + 14*xSiXV +
     +      x_(S_II) + 2*x_(S_III) + 3*x_(S_IV) + 4*x_(S_V) + 
     +      5*x_(S_VI) + 6*x_(S_VII) + 7*x_(S_VIII) + 8*x_(S_IX) + 
     +      9*x_(S_X) + 10*x_(S_XI) + 11*x_(S_XII) + 12*x_(S_XIII) + 
     +      13*x_(S_XIV)+ 14*x_(S_XV) + 15*x_(S_XVI) + 16*xSXVII +
     +      x_(Fe_II) + 2*x_(Fe_III) + 3*x_(Fe_IV) + 4*x_(Fe_V) + 
     +      5*x_(Fe_VI) + 6*x_(Fe_VII) + 7*x_(Fe_VIII) + 8*x_(Fe_IX) + 
     +      9*x_(Fe_X) + 10*x_(Fe_XI) + 11*x_(Fe_XII) + 12*x_(Fe_XIII) + 
     +      13*x_(Fe_XIV)+ 14*x_(Fe_XV) + 15*x_(Fe_XVI) + 
     +      16*x_(Fe_XVII) + 17*x_(Fe_XVIII) + 18*x_(Fe_XIX) +
     +      19*x_(Fe_XX) + 20*x_(Fe_XXI) + 21*x_(Fe_XXII) +
     +      23*x_(Fe_XXIV) + 24*x_(Fe_XXV) + 25*x_(Fe_XXVI) + 
     +      26*xFeXXVII )

c -----------------------------------------------------------
c --- NOTE:
c     max number of continuation lines is 19 !!!!!!!!!!!!!
c -----------------------------------------------------------



c --- Get recombination coefficients: rad + dielectronic + 3 body.
      call allrec(T,n_e)  ! now common vector re() contains data.
      call allrec_mat_set(re,allrec_mat) 
c     now allrec_mat(C,IV) is the CIV->CIII rec. coefficient. 
 
      call allphot_mat_set(allphot_vec)
c     now allphot_vec(C,IV,3) is photoionization from 2p shell in CIV
c     shell indices: 0: all, 1: 1s, 2: 2s, 3: 2p, 4: 3s, 5: 3p, 6: 3d, 7: 4s

c  =========================================================================
c     Calculate derivatives:
c  =========================================================================

c================
c     Hydrogen
c================

      dx_(H_I)  = xHII * n_e * allrec_mat(H_,II)    !all recombinations
      dx_(He_I) =  x_(He_II) * n_e * allrec_mat(He_,II)  
      dx_(He_II)= - x_(He_II) * n_e * allrec_mat(He_,II)
 
      dx_(H_I)  = dx_(H_I) 
     +          - x_(H_I)  * Gamma(H_,I)              !photionization
     +          - x_(H_I)  * n_e * colion(H_,I,T)     !col. ionization

      dx_(H_I)  = dx_(H_I) 
     +          - x_(He_II)    * x_(H_I) * nHtot * ct_rec(He_,II,T) ! H CT
     +          - xHeIII       * x_(H_I) * nHtot * ct_rec(He_,III,T) 

      dx_(H_I)  = dx_(H_I) 
     +          - x_(C_II)     * x_(H_I) * nHtot * ct_rec(C_,II,T)
     +          - x_(C_III)    * x_(H_I) * nHtot * ct_rec(C_,III,T)
     +          - x_(C_IV)     * x_(H_I) * nHtot * ct_rec(C_,IV,T) 
     +          - x_(C_V)      * x_(H_I) * nHtot * ct_rec(C_,V,T)  
     +          - x_(C_VI)     * x_(H_I) * nHtot * ct_rec(C_,VI,T)
     +          - xCVII        * x_(H_I) * nHtot * ct_rec(C_,VII,T)

      dx_(H_I)  = dx_(H_I) 
     +          - x_(N_II)     * x_(H_I) * nHtot * ct_rec(N_,II,T)
     +          - x_(N_III)    * x_(H_I) * nHtot * ct_rec(N_,III,T)
     +          - x_(N_IV)     * x_(H_I) * nHtot * ct_rec(N_,IV,T)
     +          - x_(N_V)      * x_(H_I) * nHtot * ct_rec(N_,V,T)
     +          - x_(N_VI)     * x_(H_I) * nHtot * ct_rec(N_,VI,T) 
     +          - x_(N_VII)    * x_(H_I) * nHtot * ct_rec(N_,VII,T)
     +          - xNVIII       * x_(H_I) * nHtot * ct_rec(N_,VIII,T)

      dx_(H_I)  = dx_(H_I) 
     +          - x_(O_II)     * x_(H_I) * nHtot * ct_rec(O_,II,T)
     +          - x_(O_III)    * x_(H_I) * nHtot * ct_rec(O_,III,T)
     +          - x_(O_IV)     * x_(H_I) * nHtot * ct_rec(O_,IV,T)
     +          - x_(O_V)      * x_(H_I) * nHtot * ct_rec(O_,V,T)
     +          - x_(O_VI)     * x_(H_I) * nHtot * ct_rec(O_,VI,T)
     +          - x_(O_VII)    * x_(H_I) * nHtot * ct_rec(O_,VII,T)
     +          - x_(O_VIII)   * x_(H_I) * nHtot * ct_rec(O_,VIII,T)
     +          - xOIX         * x_(H_I) * nHtot * ct_rec(O_,IX,T)

      dx_(H_I)  = dx_(H_I) 
     +          - x_(Ne_II)    * x_(H_I) * nHtot * ct_rec(Ne_,II,T)
     +          - x_(Ne_III)   * x_(H_I) * nHtot * ct_rec(Ne_,III,T)
     +          - x_(Ne_IV)    * x_(H_I) * nHtot * ct_rec(Ne_,IV,T)
     +          - x_(Ne_V)     * x_(H_I) * nHtot * ct_rec(Ne_,V,T)
     +          - x_(Ne_VI)    * x_(H_I) * nHtot * ct_rec(Ne_,VI,T)
     +          - x_(Ne_VII)   * x_(H_I) * nHtot * ct_rec(Ne_,VII,T)
     +          - x_(Ne_VIII)  * x_(H_I) * nHtot * ct_rec(Ne_,VIII,T)
     +          - x_(Ne_IX)    * x_(H_I) * nHtot * ct_rec(Ne_,IX,T)
     +          - x_(Ne_X)     * x_(H_I) * nHtot * ct_rec(Ne_,X,T)
     +          - xNeXI        * x_(H_I) * nHtot * ct_rec(Ne_,XI,T)

      dx_(H_I)  = dx_(H_I) 
     +          - x_(Mg_II)    * x_(H_I) * nHtot * ct_rec(Mg_,II,T)
     +          - x_(Mg_III)   * x_(H_I) * nHtot * ct_rec(Mg_,III,T)
     +          - x_(Mg_IV)    * x_(H_I) * nHtot * ct_rec(Mg_,IV,T)
     +          - x_(Mg_V)     * x_(H_I) * nHtot * ct_rec(Mg_,V,T)
     +          - x_(Mg_VI)    * x_(H_I) * nHtot * ct_rec(Mg_,VI,T)
     +          - x_(Mg_VII)   * x_(H_I) * nHtot * ct_rec(Mg_,VII,T)
     +          - x_(Mg_VIII)  * x_(H_I) * nHtot * ct_rec(Mg_,VIII,T)
     +          - x_(Mg_IX)    * x_(H_I) * nHtot * ct_rec(Mg_,IX,T)
     +          - x_(Mg_X)     * x_(H_I) * nHtot * ct_rec(Mg_,X,T)
     +          - x_(Mg_XI)    * x_(H_I) * nHtot * ct_rec(Mg_,XI,T)
     +          - x_(Mg_XII)   * x_(H_I) * nHtot * ct_rec(Mg_,XII,T)
     +          - xMgXIII      * x_(H_I) * nHtot * ct_rec(Mg_,XIII,T)

      dx_(H_I)  = dx_(H_I) 
     +          - x_(Si_II)    * x_(H_I) * nHtot * ct_rec(Si_,II,T)
     +          - x_(Si_III)   * x_(H_I) * nHtot * ct_rec(Si_,III,T)
     +          - x_(Si_IV)    * x_(H_I) * nHtot * ct_rec(Si_,IV,T)
     +          - x_(Si_V)     * x_(H_I) * nHtot * ct_rec(Si_,V,T)
     +          - x_(Si_VI)    * x_(H_I) * nHtot * ct_rec(Si_,VI,T)
     +          - x_(Si_VII)   * x_(H_I) * nHtot * ct_rec(Si_,VII,T)
     +          - x_(Si_VIII)  * x_(H_I) * nHtot * ct_rec(Si_,VIII,T)
     +          - x_(Si_IX)    * x_(H_I) * nHtot * ct_rec(Si_,IX,T)
     +          - x_(Si_X)     * x_(H_I) * nHtot * ct_rec(Si_,X,T)
     +          - x_(Si_XI)    * x_(H_I) * nHtot * ct_rec(Si_,XI,T)
     +          - x_(Si_XII)   * x_(H_I) * nHtot * ct_rec(Si_,XII,T)
     +          - x_(Si_XIII)  * x_(H_I) * nHtot * ct_rec(Si_,XIII,T)
     +          - x_(Si_XIV)   * x_(H_I) * nHtot * ct_rec(Si_,XIV,T)
     +          - xSiXV        * x_(H_I) * nHtot * ct_rec(Si_,XV,T)

      dx_(H_I)  = dx_(H_I) 
     +          - x_(S_II)     * x_(H_I) * nHtot * ct_rec(S_,II,T)
     +          - x_(S_III)    * x_(H_I) * nHtot * ct_rec(S_,III,T)
     +          - x_(S_IV)     * x_(H_I) * nHtot * ct_rec(S_,IV,T)
     +          - x_(S_V)      * x_(H_I) * nHtot * ct_rec(S_,V,T)
     +          - x_(S_VI)     * x_(H_I) * nHtot * ct_rec(S_,VI,T)
     +          - x_(S_VII)    * x_(H_I) * nHtot * ct_rec(S_,VII,T)
     +          - x_(S_VIII)   * x_(H_I) * nHtot * ct_rec(S_,VIII,T)
     +          - x_(S_IX)     * x_(H_I) * nHtot * ct_rec(S_,IX,T)
     +          - x_(S_X)      * x_(H_I) * nHtot * ct_rec(S_,X,T)
     +          - x_(S_XI)     * x_(H_I) * nHtot * ct_rec(S_,XI,T)
     +          - x_(S_XII)    * x_(H_I) * nHtot * ct_rec(S_,XII,T)
     +          - x_(S_XIII)   * x_(H_I) * nHtot * ct_rec(S_,XIII,T)
     +          - x_(S_XIV)    * x_(H_I) * nHtot * ct_rec(S_,XIV,T)
     +          - x_(S_XV)     * x_(H_I) * nHtot * ct_rec(S_,XV,T)
     +          - x_(S_XVI)    * x_(H_I) * nHtot * ct_rec(S_,XVI,T)
     +          - xSXVII       * x_(H_I) * nHtot * ct_rec(S_,XVII,T) 

      dx_(H_I)  = dx_(H_I) 
     +          - x_(Fe_II)    * x_(H_I) * nHtot * ct_rec(Fe_,II,T)
     +          - x_(Fe_III)   * x_(H_I) * nHtot * ct_rec(Fe_,III,T)
     +          - x_(Fe_IV)    * x_(H_I) * nHtot * ct_rec(Fe_,IV,T)
     +          - x_(Fe_V)     * x_(H_I) * nHtot * ct_rec(Fe_,V,T)
     +          - x_(Fe_VI)    * x_(H_I) * nHtot * ct_rec(Fe_,VI,T)
     +          - x_(Fe_VII)   * x_(H_I) * nHtot * ct_rec(Fe_,VII,T)
     +          - x_(Fe_VIII)  * x_(H_I) * nHtot * ct_rec(Fe_,VIII,T)
     +          - x_(Fe_IX)    * x_(H_I) * nHtot * ct_rec(Fe_,IX,T) 
     +          - x_(Fe_X)     * x_(H_I) * nHtot * ct_rec(Fe_,X,T)
     +          - x_(Fe_XI)    * x_(H_I) * nHtot * ct_rec(Fe_,XI,T)
     +          - x_(Fe_XII)   * x_(H_I) * nHtot * ct_rec(Fe_,XII,T)
     +          - x_(Fe_XIII)  * x_(H_I) * nHtot * ct_rec(Fe_,XIII,T)
     +          - x_(Fe_XIV)   * x_(H_I) * nHtot * ct_rec(Fe_,XIV,T)
     +          - x_(Fe_XV)    * x_(H_I) * nHtot * ct_rec(Fe_,XV,T)
     +          - x_(Fe_XVI)   * x_(H_I) * nHtot * ct_rec(Fe_,XVI,T)
     +          - x_(Fe_XVII)  * x_(H_I) * nHtot * ct_rec(Fe_,XVII,T)
     +          - x_(Fe_XVIII) * x_(H_I) * nHtot * ct_rec(Fe_,XVIII,T)
     +          - x_(Fe_XIX)   * x_(H_I) * nHtot * ct_rec(Fe_,XIX,T)
      dx_(H_I)  = dx_(H_I) 
     +          - x_(Fe_XX)    * x_(H_I) * nHtot * ct_rec(Fe_,XX,T)
     +          - x_(Fe_XXI)   * x_(H_I) * nHtot * ct_rec(Fe_,XXI,T)
     +          - x_(Fe_XXII)  * x_(H_I) * nHtot * ct_rec(Fe_,XXII,T)
     +          - x_(Fe_XXIII) * x_(H_I) * nHtot * ct_rec(Fe_,XXIII,T)
     +          - x_(Fe_XXIV)  * x_(H_I) * nHtot * ct_rec(Fe_,XXIV,T)
     +          - x_(Fe_XXV)   * x_(H_I) * nHtot * ct_rec(Fe_,XXV,T)
     +          - x_(Fe_XXVI)  * x_(H_I) * nHtot * ct_rec(Fe_,XXVI,T)
     +          - xFeXXVII     * x_(H_I) * nHtot * ct_rec(Fe_,XXVII,T)




      dx_(H_I)  = dx_(H_I) 
     +          + x_(He_I)     * xHII * nHtot * ct_ion(He_,I,T)     
     +          + x_(He_II)    * xHII * nHtot * ct_ion(He_,II,T)     
    

      dx_(H_I)  = dx_(H_I) 
     +          + x_(C_I)      * xHII * nHtot * ct_ion(C_,I,T)
     +          + x_(C_II)     * xHII * nHtot * ct_ion(C_,II,T)
     +          + x_(C_III)    * xHII * nHtot * ct_ion(C_,III,T)
     +          + x_(C_IV)     * xHII * nHtot * ct_ion(C_,IV,T)
     +          + x_(C_V)      * xHII * nHtot * ct_ion(C_,V,T)
     +          + x_(C_VI)     * xHII * nHtot * ct_ion(C_,VI,T)

      dx_(H_I)  = dx_(H_I) 
     +          + x_(N_I)      * xHII * nHtot * ct_ion(N_,I,T) 
     +          + x_(N_II)     * xHII * nHtot * ct_ion(N_,II,T)
     +          + x_(N_III)    * xHII * nHtot * ct_ion(N_,III,T)
     +          + x_(N_IV)     * xHII * nHtot * ct_ion(N_,IV,T)
     +          + x_(N_V)      * xHII * nHtot * ct_ion(N_,V,T)
     +          + x_(N_VI)     * xHII * nHtot * ct_ion(N_,VI,T)
     +          + x_(N_VII)    * xHII * nHtot * ct_ion(N_,VII,T)

      dx_(H_I)  = dx_(H_I) 
     +          + x_(O_I)      * xHII * nHtot * ct_ion(O_,I,T)
     +          + x_(O_II)     * xHII * nHtot * ct_ion(O_,II,T) 
     +          + x_(O_III)    * xHII * nHtot * ct_ion(O_,III,T)
     +          + x_(O_IV)     * xHII * nHtot * ct_ion(O_,IV,T) 
     +          + x_(O_V)      * xHII * nHtot * ct_ion(O_,V,T)
     +          + x_(O_VI)     * xHII * nHtot * ct_ion(O_,VI,T) 
     +          + x_(O_VII)    * xHII * nHtot * ct_ion(O_,VII,T)
     +          + x_(O_VIII)   * xHII * nHtot * ct_ion(O_,VIII,T)

      dx_(H_I)  = dx_(H_I) 
     +          + x_(Ne_I)     * xHII * nHtot * ct_ion(Ne_,I,T) 
     +          + x_(Ne_II)    * xHII * nHtot * ct_ion(Ne_,II,T) 
     +          + x_(Ne_III)   * xHII * nHtot * ct_ion(Ne_,III,T)
     +          + x_(Ne_IV)    * xHII * nHtot * ct_ion(Ne_,IV,T)
     +          + x_(Ne_V)     * xHII * nHtot * ct_ion(Ne_,V,T)
     +          + x_(Ne_VI)    * xHII * nHtot * ct_ion(Ne_,VI,T)
     +          + x_(Ne_VII)   * xHII * nHtot * ct_ion(Ne_,VII,T)
     +          + x_(Ne_VIII)  * xHII * nHtot * ct_ion(Ne_,VIII,T)
     +          + x_(Ne_IX)    * xHII * nHtot * ct_ion(Ne_,IX,T) 
     +          + x_(Ne_X)     * xHII * nHtot * ct_ion(Ne_,X,T)

      dx_(H_I)  = dx_(H_I) 
     +          + x_(Mg_I)    * xHII * nHtot * ct_ion(Mg_,I,T)
     +          + x_(Mg_II)   * xHII * nHtot * ct_ion(Mg_,II,T)
     +          + x_(Mg_III)  * xHII * nHtot * ct_ion(Mg_,III,T)
     +          + x_(Mg_IV)   * xHII * nHtot * ct_ion(Mg_,IV,T)
     +          + x_(Mg_V)    * xHII * nHtot * ct_ion(Mg_,V,T)
     +          + x_(Mg_VI)   * xHII * nHtot * ct_ion(Mg_,VI,T)
     +          + x_(Mg_VII)  * xHII * nHtot * ct_ion(Mg_,VII,T)
     +          + x_(Mg_VIII) * xHII * nHtot * ct_ion(Mg_,VIII,T)
     +          + x_(Mg_IX)   * xHII * nHtot * ct_ion(Mg_,IX,T)
     +          + x_(Mg_X)    * xHII * nHtot * ct_ion(Mg_,X,T)
     +          + x_(Mg_XI)   * xHII * nHtot * ct_ion(Mg_,XI,T)
     +          + x_(Mg_XII)  * xHII * nHtot * ct_ion(Mg_,XII,T)

      dx_(H_I)  = dx_(H_I) 
     +          + x_(Si_I)    * xHII * nHtot * ct_ion(Si_,I,T)
     +          + x_(Si_II)   * xHII * nHtot * ct_ion(Si_,II,T)
     +          + x_(Si_III)  * xHII * nHtot * ct_ion(Si_,III,T)
     +          + x_(Si_IV)   * xHII * nHtot * ct_ion(Si_,IV,T)
     +          + x_(Si_V)    * xHII * nHtot * ct_ion(Si_,V,T)
     +          + x_(Si_VI)   * xHII * nHtot * ct_ion(Si_,VI,T)
     +          + x_(Si_VII)  * xHII * nHtot * ct_ion(Si_,VII,T)
     +          + x_(Si_VIII) * xHII * nHtot * ct_ion(Si_,VIII,T)
     +          + x_(Si_IX)   * xHII * nHtot * ct_ion(Si_,IX,T)
     +          + x_(Si_X)    * xHII * nHtot * ct_ion(Si_,X,T)
     +          + x_(Si_XI)   * xHII * nHtot * ct_ion(Si_,XI,T)
     +          + x_(Si_XII)  * xHII * nHtot * ct_ion(Si_,XII,T)
     +          + x_(Si_XIII) * xHII * nHtot * ct_ion(Si_,XIII,T) 
     +          + x_(Si_XIV)  * xHII * nHtot * ct_ion(Si_,XIV,T) 

      dx_(H_I)  = dx_(H_I) 
     +          + x_(S_I)     * xHII * nHtot * ct_ion(S_,I,T)
     +          + x_(S_II)    * xHII * nHtot * ct_ion(S_,II,T)
     +          + x_(S_III)   * xHII * nHtot * ct_ion(S_,III,T)
     +          + x_(S_IV)    * xHII * nHtot * ct_ion(S_,IV,T)
     +          + x_(S_V)     * xHII * nHtot * ct_ion(S_,V,T)
     +          + x_(S_VI)    * xHII * nHtot * ct_ion(S_,VI,T)
     +          + x_(S_VII)   * xHII * nHtot * ct_ion(S_,VII,T)
     +          + x_(S_VIII)  * xHII * nHtot * ct_ion(S_,VIII,T) 
     +          + x_(S_IX)    * xHII * nHtot * ct_ion(S_,IX,T)
     +          + x_(S_X)     * xHII * nHtot * ct_ion(S_,X,T)
     +          + x_(S_XI)    * xHII * nHtot * ct_ion(S_,XI,T)
     +          + x_(S_XII)   * xHII * nHtot * ct_ion(S_,XII,T)
     +          + x_(S_XIII)  * xHII * nHtot * ct_ion(S_,XIII,T)
     +          + x_(S_XIV)   * xHII * nHtot * ct_ion(S_,XIV,T)
     +          + x_(S_XV)    * xHII * nHtot * ct_ion(S_,XV,T)
     +          + x_(S_XVI)   * xHII * nHtot * ct_ion(S_,XVI,T)

      dx_(H_I)  = dx_(H_I) 
     +          + x_(Fe_I)     * xHII * nHtot * ct_ion(Fe_,I,T)
     +          + x_(Fe_II)    * xHII * nHtot * ct_ion(Fe_,II,T)
     +          + x_(Fe_III)   * xHII * nHtot * ct_ion(Fe_,III,T)
     +          + x_(Fe_IV)    * xHII * nHtot * ct_ion(Fe_,IV,T) 
     +          + x_(Fe_V)     * xHII * nHtot * ct_ion(Fe_,V,T)
     +          + x_(Fe_VI)    * xHII * nHtot * ct_ion(Fe_,VI,T)
     +          + x_(Fe_VII)   * xHII * nHtot * ct_ion(Fe_,VII,T)
     +          + x_(Fe_VIII)  * xHII * nHtot * ct_ion(Fe_,VIII,T)
     +          + x_(Fe_IX)    * xHII * nHtot * ct_ion(Fe_,IX,T) 
     +          + x_(Fe_X)     * xHII * nHtot * ct_ion(Fe_,X,T)
     +          + x_(Fe_XI)    * xHII * nHtot * ct_ion(Fe_,XI,T)
     +          + x_(Fe_XII)   * xHII * nHtot * ct_ion(Fe_,XII,T)
     +          + x_(Fe_XIII)  * xHII * nHtot * ct_ion(Fe_,XIII,T)
     +          + x_(Fe_XIV)   * xHII * nHtot * ct_ion(Fe_,XIV,T)
     +          + x_(Fe_XV)    * xHII * nHtot * ct_ion(Fe_,XV,T)
     +          + x_(Fe_XVI)   * xHII * nHtot * ct_ion(Fe_,XVI,T)
     +          + x_(Fe_XVII)  * xHII * nHtot * ct_ion(Fe_,XVII,T)
     +          + x_(Fe_XVIII) * xHII * nHtot * ct_ion(Fe_,XVIII,T)
      dx_(H_I)  = dx_(H_I) 
     +          + x_(Fe_XIX)   * xHII * nHtot * ct_ion(Fe_,XIX,T)
     +          + x_(Fe_XX)    * xHII * nHtot * ct_ion(Fe_,XX,T)
     +          + x_(Fe_XXI)   * xHII * nHtot * ct_ion(Fe_,XXI,T)
     +          + x_(Fe_XXII)  * xHII * nHtot * ct_ion(Fe_,XXII,T)
     +          + x_(Fe_XXIII) * xHII * nHtot * ct_ion(Fe_,XXIII,T)
     +          + x_(Fe_XXIV)  * xHII * nHtot * ct_ion(Fe_,XXIV,T) 
     +          + x_(Fe_XXV)   * xHII * nHtot * ct_ion(Fe_,XXV,T) 
     +          + x_(Fe_XXVI)  * xHII * nHtot * ct_ion(Fe_,XXVI,T) 


c      if (n_e/nHtot.le.0.1) then !add secondary ionizations
c         dx_(H_I) = dx_(H_I) 
c     +            - x_(H_I) * nHtot * H_sec_rate(x_,nsize,n_e/nHtot)
c      endif


c================
c     Helium
c================

      dx_(He_I)   = dx_(He_I)
     +       + x_(He_II) * x_(H_I) * nHtot * ct_rec(He_,II,T) !H CT "rec" 
     +       - x_(He_I)  * xHII * nHtot * ct_ion(He_,I,T)     !H CT "ionization"
     +       - x_(He_I)  * Gamma(He_,I)                      !photoionization
     +       - x_(He_I)  * n_e * colion(He_,I,T)             !col. ionization


      dx_(He_I)  = dx_(He_I) 
     +          - x_(C_II)     * x_(He_I) * nHtot * he_ct_r(C_,II,T)
     +          - x_(C_III)    * x_(He_I) * nHtot * he_ct_r(C_,III,T)
     +          - x_(C_IV)     * x_(He_I) * nHtot * he_ct_r(C_,IV,T) 
     +          - x_(C_V)      * x_(He_I) * nHtot * he_ct_r(C_,V,T)  
     +          - x_(C_VI)     * x_(He_I) * nHtot * he_ct_r(C_,VI,T)
     +          - xCVII        * x_(He_I) * nHtot * he_ct_r(C_,VII,T)

      dx_(He_I)  = dx_(He_I) 
     +          - x_(N_II)     * x_(He_I) * nHtot * he_ct_r(N_,II,T)
     +          - x_(N_III)    * x_(He_I) * nHtot * he_ct_r(N_,III,T)
     +          - x_(N_IV)     * x_(He_I) * nHtot * he_ct_r(N_,IV,T)
     +          - x_(N_V)      * x_(He_I) * nHtot * he_ct_r(N_,V,T)
     +          - x_(N_VI)     * x_(He_I) * nHtot * he_ct_r(N_,VI,T) 
     +          - x_(N_VII)    * x_(He_I) * nHtot * he_ct_r(N_,VII,T)
     +          - xNVIII       * x_(He_I) * nHtot * he_ct_r(N_,VIII,T)

      dx_(He_I)  = dx_(He_I) 
     +          - x_(O_II)     * x_(He_I) * nHtot * he_ct_r(O_,II,T)
     +          - x_(O_III)    * x_(He_I) * nHtot * he_ct_r(O_,III,T)
     +          - x_(O_IV)     * x_(He_I) * nHtot * he_ct_r(O_,IV,T)
     +          - x_(O_V)      * x_(He_I) * nHtot * he_ct_r(O_,V,T)
     +          - x_(O_VI)     * x_(He_I) * nHtot * he_ct_r(O_,VI,T)
     +          - x_(O_VII)    * x_(He_I) * nHtot * he_ct_r(O_,VII,T)
     +          - x_(O_VIII)   * x_(He_I) * nHtot * he_ct_r(O_,VIII,T)
     +          - xOIX         * x_(He_I) * nHtot * he_ct_r(O_,IX,T)

      dx_(He_I)  = dx_(He_I) 
     +          - x_(Ne_II)    * x_(He_I) * nHtot * he_ct_r(Ne_,II,T)
     +          - x_(Ne_III)   * x_(He_I) * nHtot * he_ct_r(Ne_,III,T)
     +          - x_(Ne_IV)    * x_(He_I) * nHtot * he_ct_r(Ne_,IV,T)
     +          - x_(Ne_V)     * x_(He_I) * nHtot * he_ct_r(Ne_,V,T)
     +          - x_(Ne_VI)    * x_(He_I) * nHtot * he_ct_r(Ne_,VI,T)
     +          - x_(Ne_VII)   * x_(He_I) * nHtot * he_ct_r(Ne_,VII,T)
     +          - x_(Ne_VIII)  * x_(He_I) * nHtot * he_ct_r(Ne_,VIII,T)
     +          - x_(Ne_IX)    * x_(He_I) * nHtot * he_ct_r(Ne_,IX,T)
     +          - x_(Ne_X)     * x_(He_I) * nHtot * he_ct_r(Ne_,X,T)
     +          - xNeXI        * x_(He_I) * nHtot * he_ct_r(Ne_,XI,T)

      dx_(He_I)  = dx_(He_I) 
     +          - x_(Mg_II)    * x_(He_I) * nHtot * he_ct_r(Mg_,II,T)
     +          - x_(Mg_III)   * x_(He_I) * nHtot * he_ct_r(Mg_,III,T)
     +          - x_(Mg_IV)    * x_(He_I) * nHtot * he_ct_r(Mg_,IV,T)
     +          - x_(Mg_V)     * x_(He_I) * nHtot * he_ct_r(Mg_,V,T)
     +          - x_(Mg_VI)    * x_(He_I) * nHtot * he_ct_r(Mg_,VI,T)
     +          - x_(Mg_VII)   * x_(He_I) * nHtot * he_ct_r(Mg_,VII,T)
     +          - x_(Mg_VIII)  * x_(He_I) * nHtot * he_ct_r(Mg_,VIII,T)
     +          - x_(Mg_IX)    * x_(He_I) * nHtot * he_ct_r(Mg_,IX,T)
     +          - x_(Mg_X)     * x_(He_I) * nHtot * he_ct_r(Mg_,X,T)
     +          - x_(Mg_XI)    * x_(He_I) * nHtot * he_ct_r(Mg_,XI,T)
     +          - x_(Mg_XII)   * x_(He_I) * nHtot * he_ct_r(Mg_,XII,T)
     +          - xMgXIII      * x_(He_I) * nHtot * he_ct_r(Mg_,XIII,T)

      dx_(He_I)  = dx_(He_I) 
     +          - x_(Si_II)    * x_(He_I) * nHtot * he_ct_r(Si_,II,T)
     +          - x_(Si_III)   * x_(He_I) * nHtot * he_ct_r(Si_,III,T)
     +          - x_(Si_IV)    * x_(He_I) * nHtot * he_ct_r(Si_,IV,T)
     +          - x_(Si_V)     * x_(He_I) * nHtot * he_ct_r(Si_,V,T)
     +          - x_(Si_VI)    * x_(He_I) * nHtot * he_ct_r(Si_,VI,T)
     +          - x_(Si_VII)   * x_(He_I) * nHtot * he_ct_r(Si_,VII,T)
     +          - x_(Si_VIII)  * x_(He_I) * nHtot * he_ct_r(Si_,VIII,T)
     +          - x_(Si_IX)    * x_(He_I) * nHtot * he_ct_r(Si_,IX,T)
     +          - x_(Si_X)     * x_(He_I) * nHtot * he_ct_r(Si_,X,T)
     +          - x_(Si_XI)    * x_(He_I) * nHtot * he_ct_r(Si_,XI,T)
     +          - x_(Si_XII)   * x_(He_I) * nHtot * he_ct_r(Si_,XII,T)
     +          - x_(Si_XIII)  * x_(He_I) * nHtot * he_ct_r(Si_,XIII,T)
     +          - x_(Si_XIV)   * x_(He_I) * nHtot * he_ct_r(Si_,XIV,T)
     +          - xSiXV        * x_(He_I) * nHtot * he_ct_r(Si_,XV,T)

      dx_(He_I)  = dx_(He_I) 
     +          - x_(S_II)     * x_(He_I) * nHtot * he_ct_r(S_,II,T)
     +          - x_(S_III)    * x_(He_I) * nHtot * he_ct_r(S_,III,T)
     +          - x_(S_IV)     * x_(He_I) * nHtot * he_ct_r(S_,IV,T)
     +          - x_(S_V)      * x_(He_I) * nHtot * he_ct_r(S_,V,T)
     +          - x_(S_VI)     * x_(He_I) * nHtot * he_ct_r(S_,VI,T)
     +          - x_(S_VII)    * x_(He_I) * nHtot * he_ct_r(S_,VII,T)
     +          - x_(S_VIII)   * x_(He_I) * nHtot * he_ct_r(S_,VIII,T)
     +          - x_(S_IX)     * x_(He_I) * nHtot * he_ct_r(S_,IX,T)
     +          - x_(S_X)      * x_(He_I) * nHtot * he_ct_r(S_,X,T)
     +          - x_(S_XI)     * x_(He_I) * nHtot * he_ct_r(S_,XI,T)
     +          - x_(S_XII)    * x_(He_I) * nHtot * he_ct_r(S_,XII,T)
     +          - x_(S_XIII)   * x_(He_I) * nHtot * he_ct_r(S_,XIII,T)
     +          - x_(S_XIV)    * x_(He_I) * nHtot * he_ct_r(S_,XIV,T)
     +          - x_(S_XV)     * x_(He_I) * nHtot * he_ct_r(S_,XV,T)
     +          - x_(S_XVI)    * x_(He_I) * nHtot * he_ct_r(S_,XVI,T)
     +          - xSXVII       * x_(He_I) * nHtot * he_ct_r(S_,XVII,T) 

      dx_(He_I)  = dx_(He_I) 
     +          - x_(Fe_II)    * x_(He_I) * nHtot * he_ct_r(Fe_,II,T)
     +          - x_(Fe_III)   * x_(He_I) * nHtot * he_ct_r(Fe_,III,T)
     +          - x_(Fe_IV)    * x_(He_I) * nHtot * he_ct_r(Fe_,IV,T)
     +          - x_(Fe_V)     * x_(He_I) * nHtot * he_ct_r(Fe_,V,T)
     +          - x_(Fe_VI)    * x_(He_I) * nHtot * he_ct_r(Fe_,VI,T)
     +          - x_(Fe_VII)   * x_(He_I) * nHtot * he_ct_r(Fe_,VII,T)
     +          - x_(Fe_VIII)  * x_(He_I) * nHtot * he_ct_r(Fe_,VIII,T)
     +          - x_(Fe_IX)    * x_(He_I) * nHtot * he_ct_r(Fe_,IX,T) 
     +          - x_(Fe_X)     * x_(He_I) * nHtot * he_ct_r(Fe_,X,T)
     +          - x_(Fe_XI)    * x_(He_I) * nHtot * he_ct_r(Fe_,XI,T)
     +          - x_(Fe_XII)   * x_(He_I) * nHtot * he_ct_r(Fe_,XII,T)
     +          - x_(Fe_XIII)  * x_(He_I) * nHtot * he_ct_r(Fe_,XIII,T)
     +          - x_(Fe_XIV)   * x_(He_I) * nHtot * he_ct_r(Fe_,XIV,T)
     +          - x_(Fe_XV)    * x_(He_I) * nHtot * he_ct_r(Fe_,XV,T)
     +          - x_(Fe_XVI)   * x_(He_I) * nHtot * he_ct_r(Fe_,XVI,T)
     +          - x_(Fe_XVII)  * x_(He_I) * nHtot * he_ct_r(Fe_,XVII,T)
     +          - x_(Fe_XVIII) * x_(He_I) * nHtot * he_ct_r(Fe_,XVIII,T)
     +          - x_(Fe_XIX)   * x_(He_I) * nHtot * he_ct_r(Fe_,XIX,T)
      dx_(He_I)  = dx_(He_I) 
     +          - x_(Fe_XX)    * x_(He_I) * nHtot * he_ct_r(Fe_,XX,T)
     +          - x_(Fe_XXI)   * x_(He_I) * nHtot * he_ct_r(Fe_,XXI,T)
     +          - x_(Fe_XXII)  * x_(He_I) * nHtot * he_ct_r(Fe_,XXII,T)
     +          - x_(Fe_XXIII) * x_(He_I) * nHtot * he_ct_r(Fe_,XXIII,T)
     +          - x_(Fe_XXIV)  * x_(He_I) * nHtot * he_ct_r(Fe_,XXIV,T)
     +          - x_(Fe_XXV)   * x_(He_I) * nHtot * he_ct_r(Fe_,XXV,T)
     +          - x_(Fe_XXVI)  * x_(He_I) * nHtot * he_ct_r(Fe_,XXVI,T)
     +          - xFeXXVII     * x_(He_I) * nHtot * he_ct_r(Fe_,XXVII,T)

 

      dx_(He_I)  = dx_(He_I) 
     +          + x_(C_I)      * x_(He_II) * nHtot * he_ct_i(C_,I,T)
     +          + x_(C_II)     * x_(He_II) * nHtot * he_ct_i(C_,II,T)
     +          + x_(C_III)    * x_(He_II) * nHtot * he_ct_i(C_,III,T)
     +          + x_(C_IV)     * x_(He_II) * nHtot * he_ct_i(C_,IV,T)
     +          + x_(C_V)      * x_(He_II) * nHtot * he_ct_i(C_,V,T)
     +          + x_(C_VI)     * x_(He_II) * nHtot * he_ct_i(C_,VI,T)

      dx_(He_I)  = dx_(He_I) 
     +          + x_(N_I)      * x_(He_II) * nHtot * he_ct_i(N_,I,T) 
     +          + x_(N_II)     * x_(He_II) * nHtot * he_ct_i(N_,II,T)
     +          + x_(N_III)    * x_(He_II) * nHtot * he_ct_i(N_,III,T)
     +          + x_(N_IV)     * x_(He_II) * nHtot * he_ct_i(N_,IV,T)
     +          + x_(N_V)      * x_(He_II) * nHtot * he_ct_i(N_,V,T)
     +          + x_(N_VI)     * x_(He_II) * nHtot * he_ct_i(N_,VI,T)
     +          + x_(N_VII)    * x_(He_II) * nHtot * he_ct_i(N_,VII,T)

      dx_(He_I)  = dx_(He_I) 
     +          + x_(O_I)      * x_(He_II) * nHtot * he_ct_i(O_,I,T)
     +          + x_(O_II)     * x_(He_II) * nHtot * he_ct_i(O_,II,T) 
     +          + x_(O_III)    * x_(He_II) * nHtot * he_ct_i(O_,III,T)
     +          + x_(O_IV)     * x_(He_II) * nHtot * he_ct_i(O_,IV,T) 
     +          + x_(O_V)      * x_(He_II) * nHtot * he_ct_i(O_,V,T)
     +          + x_(O_VI)     * x_(He_II) * nHtot * he_ct_i(O_,VI,T) 
     +          + x_(O_VII)    * x_(He_II) * nHtot * he_ct_i(O_,VII,T)
     +          + x_(O_VIII)   * x_(He_II) * nHtot * he_ct_i(O_,VIII,T)

      dx_(He_I)  = dx_(He_I) 
     +          + x_(Ne_I)     * x_(He_II) * nHtot * he_ct_i(Ne_,I,T) 
     +          + x_(Ne_II)    * x_(He_II) * nHtot * he_ct_i(Ne_,II,T) 
     +          + x_(Ne_III)   * x_(He_II) * nHtot * he_ct_i(Ne_,III,T)
     +          + x_(Ne_IV)    * x_(He_II) * nHtot * he_ct_i(Ne_,IV,T)
     +          + x_(Ne_V)     * x_(He_II) * nHtot * he_ct_i(Ne_,V,T)
     +          + x_(Ne_VI)    * x_(He_II) * nHtot * he_ct_i(Ne_,VI,T)
     +          + x_(Ne_VII)   * x_(He_II) * nHtot * he_ct_i(Ne_,VII,T)
     +          + x_(Ne_VIII)  * x_(He_II) * nHtot * he_ct_i(Ne_,VIII,T)
     +          + x_(Ne_IX)    * x_(He_II) * nHtot * he_ct_i(Ne_,IX,T) 
     +          + x_(Ne_X)     * x_(He_II) * nHtot * he_ct_i(Ne_,X,T)

      dx_(He_I)  = dx_(He_I) 
     +          + x_(Mg_I)    * x_(He_II) * nHtot * he_ct_i(Mg_,I,T)
     +          + x_(Mg_II)   * x_(He_II) * nHtot * he_ct_i(Mg_,II,T)
     +          + x_(Mg_III)  * x_(He_II) * nHtot * he_ct_i(Mg_,III,T)
     +          + x_(Mg_IV)   * x_(He_II) * nHtot * he_ct_i(Mg_,IV,T)
     +          + x_(Mg_V)    * x_(He_II) * nHtot * he_ct_i(Mg_,V,T)
     +          + x_(Mg_VI)   * x_(He_II) * nHtot * he_ct_i(Mg_,VI,T)
     +          + x_(Mg_VII)  * x_(He_II) * nHtot * he_ct_i(Mg_,VII,T)
     +          + x_(Mg_VIII) * x_(He_II) * nHtot * he_ct_i(Mg_,VIII,T)
     +          + x_(Mg_IX)   * x_(He_II) * nHtot * he_ct_i(Mg_,IX,T)
     +          + x_(Mg_X)    * x_(He_II) * nHtot * he_ct_i(Mg_,X,T)
     +          + x_(Mg_XI)   * x_(He_II) * nHtot * he_ct_i(Mg_,XI,T)
     +          + x_(Mg_XII)  * x_(He_II) * nHtot * he_ct_i(Mg_,XII,T)

      dx_(He_I)  = dx_(He_I) 
     +          + x_(Si_I)    * x_(He_II) * nHtot * he_ct_i(Si_,I,T)
     +          + x_(Si_II)   * x_(He_II) * nHtot * he_ct_i(Si_,II,T)
     +          + x_(Si_III)  * x_(He_II) * nHtot * he_ct_i(Si_,III,T)
     +          + x_(Si_IV)   * x_(He_II) * nHtot * he_ct_i(Si_,IV,T)
     +          + x_(Si_V)    * x_(He_II) * nHtot * he_ct_i(Si_,V,T)
     +          + x_(Si_VI)   * x_(He_II) * nHtot * he_ct_i(Si_,VI,T)
     +          + x_(Si_VII)  * x_(He_II) * nHtot * he_ct_i(Si_,VII,T)
     +          + x_(Si_VIII) * x_(He_II) * nHtot * he_ct_i(Si_,VIII,T)
     +          + x_(Si_IX)   * x_(He_II) * nHtot * he_ct_i(Si_,IX,T)
     +          + x_(Si_X)    * x_(He_II) * nHtot * he_ct_i(Si_,X,T)
     +          + x_(Si_XI)   * x_(He_II) * nHtot * he_ct_i(Si_,XI,T)
     +          + x_(Si_XII)  * x_(He_II) * nHtot * he_ct_i(Si_,XII,T)
     +          + x_(Si_XIII) * x_(He_II) * nHtot * he_ct_i(Si_,XIII,T) 
     +          + x_(Si_XIV)  * x_(He_II) * nHtot * he_ct_i(Si_,XIV,T) 

      dx_(He_I)  = dx_(He_I) 
     +          + x_(S_I)     * x_(He_II) * nHtot * he_ct_i(S_,I,T)
     +          + x_(S_II)    * x_(He_II) * nHtot * he_ct_i(S_,II,T)
     +          + x_(S_III)   * x_(He_II) * nHtot * he_ct_i(S_,III,T)
     +          + x_(S_IV)    * x_(He_II) * nHtot * he_ct_i(S_,IV,T)
     +          + x_(S_V)     * x_(He_II) * nHtot * he_ct_i(S_,V,T)
     +          + x_(S_VI)    * x_(He_II) * nHtot * he_ct_i(S_,VI,T)
     +          + x_(S_VII)   * x_(He_II) * nHtot * he_ct_i(S_,VII,T)
     +          + x_(S_VIII)  * x_(He_II) * nHtot * he_ct_i(S_,VIII,T) 
     +          + x_(S_IX)    * x_(He_II) * nHtot * he_ct_i(S_,IX,T)
     +          + x_(S_X)     * x_(He_II) * nHtot * he_ct_i(S_,X,T)
     +          + x_(S_XI)    * x_(He_II) * nHtot * he_ct_i(S_,XI,T)
     +          + x_(S_XII)   * x_(He_II) * nHtot * he_ct_i(S_,XII,T)
     +          + x_(S_XIII)  * x_(He_II) * nHtot * he_ct_i(S_,XIII,T)
     +          + x_(S_XIV)   * x_(He_II) * nHtot * he_ct_i(S_,XIV,T)
     +          + x_(S_XV)    * x_(He_II) * nHtot * he_ct_i(S_,XV,T)
     +          + x_(S_XVI)   * x_(He_II) * nHtot * he_ct_i(S_,XVI,T)

      dx_(He_I)  = dx_(He_I) 
     +         + x_(Fe_I)     * x_(He_II) * nHtot * he_ct_i(Fe_,I,T)
     +         + x_(Fe_II)    * x_(He_II) * nHtot * he_ct_i(Fe_,II,T)
     +         + x_(Fe_III)   * x_(He_II) * nHtot * he_ct_i(Fe_,III,T)
     +         + x_(Fe_IV)    * x_(He_II) * nHtot * he_ct_i(Fe_,IV,T) 
     +         + x_(Fe_V)     * x_(He_II) * nHtot * he_ct_i(Fe_,V,T)
     +         + x_(Fe_VI)    * x_(He_II) * nHtot * he_ct_i(Fe_,VI,T)
     +         + x_(Fe_VII)   * x_(He_II) * nHtot * he_ct_i(Fe_,VII,T)
     +         + x_(Fe_VIII)  * x_(He_II) * nHtot * he_ct_i(Fe_,VIII,T)
     +         + x_(Fe_IX)    * x_(He_II) * nHtot * he_ct_i(Fe_,IX,T) 
     +         + x_(Fe_X)     * x_(He_II) * nHtot * he_ct_i(Fe_,X,T)
     +         + x_(Fe_XI)    * x_(He_II) * nHtot * he_ct_i(Fe_,XI,T)
     +         + x_(Fe_XII)   * x_(He_II) * nHtot * he_ct_i(Fe_,XII,T)
     +         + x_(Fe_XIII)  * x_(He_II) * nHtot * he_ct_i(Fe_,XIII,T)
     +         + x_(Fe_XIV)   * x_(He_II) * nHtot * he_ct_i(Fe_,XIV,T)
     +         + x_(Fe_XV)    * x_(He_II) * nHtot * he_ct_i(Fe_,XV,T)
     +         + x_(Fe_XVI)   * x_(He_II) * nHtot * he_ct_i(Fe_,XVI,T)
     +         + x_(Fe_XVII)  * x_(He_II) * nHtot * he_ct_i(Fe_,XVII,T)
     +         + x_(Fe_XVIII) * x_(He_II) * nHtot * he_ct_i(Fe_,XVIII,T)
      dx_(He_I)  = dx_(He_I) 
     +         + x_(Fe_XIX)   * x_(He_II) * nHtot * he_ct_i(Fe_,XIX,T)
     +         + x_(Fe_XX)    * x_(He_II) * nHtot * he_ct_i(Fe_,XX,T)
     +         + x_(Fe_XXI)   * x_(He_II) * nHtot * he_ct_i(Fe_,XXI,T)
     +         + x_(Fe_XXII)  * x_(He_II) * nHtot * he_ct_i(Fe_,XXII,T)
     +         + x_(Fe_XXIII) * x_(He_II) * nHtot * he_ct_i(Fe_,XXIII,T)
     +         + x_(Fe_XXIV)  * x_(He_II) * nHtot * he_ct_i(Fe_,XXIV,T) 
     +         + x_(Fe_XXV)   * x_(He_II) * nHtot * he_ct_i(Fe_,XXV,T) 
     +         + x_(Fe_XXVI)  * x_(He_II) * nHtot * he_ct_i(Fe_,XXVI,T) 


       dx_(He_II)  = dx_(He_II) +
     +         xHeIII   * n_e * allrec_mat(He_,III)          !rad + 3bod + dr

      dx_(He_II)  = dx_(He_II)
     +       + xHeIII   * x_(H_I) * nHtot * ct_rec(He_,III,T) !H CT "rec" 
     +       - x_(He_II) * x_(H_I) * nHtot * ct_rec(He_,II,T) !H CT "rec" 
     +       + x_(He_I)  * xHII * nHtot * ct_ion(He_,I,T)     !H CT "ionization"
     +       - x_(He_II) * xHII * nHtot * ct_ion(He_,II,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, He_II,He_,II)         !photoI+Auger
     +       - x_(He_II) * Gamma(He_,II)                     !photoionization
     +       + x_(He_I)  * n_e * colion(He_,I,T)             !col. ionization 
     +       - x_(He_II) * n_e * colion(He_,II,T)            !col. ionization 

      dx_(He_II)  = dx_(He_II) 
     +          + x_(C_II)     * x_(He_I) * nHtot * he_ct_r(C_,II,T)
     +          + x_(C_III)    * x_(He_I) * nHtot * he_ct_r(C_,III,T)
     +          + x_(C_IV)     * x_(He_I) * nHtot * he_ct_r(C_,IV,T) 
     +          + x_(C_V)      * x_(He_I) * nHtot * he_ct_r(C_,V,T)  
     +          + x_(C_VI)     * x_(He_I) * nHtot * he_ct_r(C_,VI,T)
     +          + xCVII        * x_(He_I) * nHtot * he_ct_r(C_,VII,T)

      dx_(He_II)  = dx_(He_II) 
     +          + x_(N_II)     * x_(He_I) * nHtot * he_ct_r(N_,II,T)
     +          + x_(N_III)    * x_(He_I) * nHtot * he_ct_r(N_,III,T)
     +          + x_(N_IV)     * x_(He_I) * nHtot * he_ct_r(N_,IV,T)
     +          + x_(N_V)      * x_(He_I) * nHtot * he_ct_r(N_,V,T)
     +          + x_(N_VI)     * x_(He_I) * nHtot * he_ct_r(N_,VI,T) 
     +          + x_(N_VII)    * x_(He_I) * nHtot * he_ct_r(N_,VII,T)
     +          + xNVIII       * x_(He_I) * nHtot * he_ct_r(N_,VIII,T)

      dx_(He_II)  = dx_(He_II) 
     +          + x_(O_II)     * x_(He_I) * nHtot * he_ct_r(O_,II,T)
     +          + x_(O_III)    * x_(He_I) * nHtot * he_ct_r(O_,III,T)
     +          + x_(O_IV)     * x_(He_I) * nHtot * he_ct_r(O_,IV,T)
     +          + x_(O_V)      * x_(He_I) * nHtot * he_ct_r(O_,V,T)
     +          + x_(O_VI)     * x_(He_I) * nHtot * he_ct_r(O_,VI,T)
     +          + x_(O_VII)    * x_(He_I) * nHtot * he_ct_r(O_,VII,T)
     +          + x_(O_VIII)   * x_(He_I) * nHtot * he_ct_r(O_,VIII,T)
     +          + xOIX         * x_(He_I) * nHtot * he_ct_r(O_,IX,T)

      dx_(He_II)  = dx_(He_II) 
     +          + x_(Ne_II)    * x_(He_I) * nHtot * he_ct_r(Ne_,II,T)
     +          + x_(Ne_III)   * x_(He_I) * nHtot * he_ct_r(Ne_,III,T)
     +          + x_(Ne_IV)    * x_(He_I) * nHtot * he_ct_r(Ne_,IV,T)
     +          + x_(Ne_V)     * x_(He_I) * nHtot * he_ct_r(Ne_,V,T)
     +          + x_(Ne_VI)    * x_(He_I) * nHtot * he_ct_r(Ne_,VI,T)
     +          + x_(Ne_VII)   * x_(He_I) * nHtot * he_ct_r(Ne_,VII,T)
     +          + x_(Ne_VIII)  * x_(He_I) * nHtot * he_ct_r(Ne_,VIII,T)
     +          + x_(Ne_IX)    * x_(He_I) * nHtot * he_ct_r(Ne_,IX,T)
     +          + x_(Ne_X)     * x_(He_I) * nHtot * he_ct_r(Ne_,X,T)
     +          + xNeXI        * x_(He_I) * nHtot * he_ct_r(Ne_,XI,T)

      dx_(He_II)  = dx_(He_II) 
     +          + x_(Mg_II)    * x_(He_I) * nHtot * he_ct_r(Mg_,II,T)
     +          + x_(Mg_III)   * x_(He_I) * nHtot * he_ct_r(Mg_,III,T)
     +          + x_(Mg_IV)    * x_(He_I) * nHtot * he_ct_r(Mg_,IV,T)
     +          + x_(Mg_V)     * x_(He_I) * nHtot * he_ct_r(Mg_,V,T)
     +          + x_(Mg_VI)    * x_(He_I) * nHtot * he_ct_r(Mg_,VI,T)
     +          + x_(Mg_VII)   * x_(He_I) * nHtot * he_ct_r(Mg_,VII,T)
     +          + x_(Mg_VIII)  * x_(He_I) * nHtot * he_ct_r(Mg_,VIII,T)
     +          + x_(Mg_IX)    * x_(He_I) * nHtot * he_ct_r(Mg_,IX,T)
     +          + x_(Mg_X)     * x_(He_I) * nHtot * he_ct_r(Mg_,X,T)
     +          + x_(Mg_XI)    * x_(He_I) * nHtot * he_ct_r(Mg_,XI,T)
     +          + x_(Mg_XII)   * x_(He_I) * nHtot * he_ct_r(Mg_,XII,T)
     +          + xMgXIII      * x_(He_I) * nHtot * he_ct_r(Mg_,XIII,T)

      dx_(He_II)  = dx_(He_II) 
     +          + x_(Si_II)    * x_(He_I) * nHtot * he_ct_r(Si_,II,T)
     +          + x_(Si_III)   * x_(He_I) * nHtot * he_ct_r(Si_,III,T)
     +          + x_(Si_IV)    * x_(He_I) * nHtot * he_ct_r(Si_,IV,T)
     +          + x_(Si_V)     * x_(He_I) * nHtot * he_ct_r(Si_,V,T)
     +          + x_(Si_VI)    * x_(He_I) * nHtot * he_ct_r(Si_,VI,T)
     +          + x_(Si_VII)   * x_(He_I) * nHtot * he_ct_r(Si_,VII,T)
     +          + x_(Si_VIII)  * x_(He_I) * nHtot * he_ct_r(Si_,VIII,T)
     +          + x_(Si_IX)    * x_(He_I) * nHtot * he_ct_r(Si_,IX,T)
     +          + x_(Si_X)     * x_(He_I) * nHtot * he_ct_r(Si_,X,T)
     +          + x_(Si_XI)    * x_(He_I) * nHtot * he_ct_r(Si_,XI,T)
     +          + x_(Si_XII)   * x_(He_I) * nHtot * he_ct_r(Si_,XII,T)
     +          + x_(Si_XIII)  * x_(He_I) * nHtot * he_ct_r(Si_,XIII,T)
     +          + x_(Si_XIV)   * x_(He_I) * nHtot * he_ct_r(Si_,XIV,T)
     +          + xSiXV        * x_(He_I) * nHtot * he_ct_r(Si_,XV,T)

      dx_(He_II)  = dx_(He_II) 
     +          + x_(S_II)     * x_(He_I) * nHtot * he_ct_r(S_,II,T)
     +          + x_(S_III)    * x_(He_I) * nHtot * he_ct_r(S_,III,T)
     +          + x_(S_IV)     * x_(He_I) * nHtot * he_ct_r(S_,IV,T)
     +          + x_(S_V)      * x_(He_I) * nHtot * he_ct_r(S_,V,T)
     +          + x_(S_VI)     * x_(He_I) * nHtot * he_ct_r(S_,VI,T)
     +          + x_(S_VII)    * x_(He_I) * nHtot * he_ct_r(S_,VII,T)
     +          + x_(S_VIII)   * x_(He_I) * nHtot * he_ct_r(S_,VIII,T)
     +          + x_(S_IX)     * x_(He_I) * nHtot * he_ct_r(S_,IX,T)
     +          + x_(S_X)      * x_(He_I) * nHtot * he_ct_r(S_,X,T)
     +          + x_(S_XI)     * x_(He_I) * nHtot * he_ct_r(S_,XI,T)
     +          + x_(S_XII)    * x_(He_I) * nHtot * he_ct_r(S_,XII,T)
     +          + x_(S_XIII)   * x_(He_I) * nHtot * he_ct_r(S_,XIII,T)
     +          + x_(S_XIV)    * x_(He_I) * nHtot * he_ct_r(S_,XIV,T)
     +          + x_(S_XV)     * x_(He_I) * nHtot * he_ct_r(S_,XV,T)
     +          + x_(S_XVI)    * x_(He_I) * nHtot * he_ct_r(S_,XVI,T)
     +          + xSXVII       * x_(He_I) * nHtot * he_ct_r(S_,XVII,T) 

      dx_(He_II)  = dx_(He_II) 
     +          + x_(Fe_II)    * x_(He_I) * nHtot * he_ct_r(Fe_,II,T)
     +          + x_(Fe_III)   * x_(He_I) * nHtot * he_ct_r(Fe_,III,T)
     +          + x_(Fe_IV)    * x_(He_I) * nHtot * he_ct_r(Fe_,IV,T)
     +          + x_(Fe_V)     * x_(He_I) * nHtot * he_ct_r(Fe_,V,T)
     +          + x_(Fe_VI)    * x_(He_I) * nHtot * he_ct_r(Fe_,VI,T)
     +          + x_(Fe_VII)   * x_(He_I) * nHtot * he_ct_r(Fe_,VII,T)
     +          + x_(Fe_VIII)  * x_(He_I) * nHtot * he_ct_r(Fe_,VIII,T)
     +          + x_(Fe_IX)    * x_(He_I) * nHtot * he_ct_r(Fe_,IX,T) 
     +          + x_(Fe_X)     * x_(He_I) * nHtot * he_ct_r(Fe_,X,T)
     +          + x_(Fe_XI)    * x_(He_I) * nHtot * he_ct_r(Fe_,XI,T)
     +          + x_(Fe_XII)   * x_(He_I) * nHtot * he_ct_r(Fe_,XII,T)
     +          + x_(Fe_XIII)  * x_(He_I) * nHtot * he_ct_r(Fe_,XIII,T)
     +          + x_(Fe_XIV)   * x_(He_I) * nHtot * he_ct_r(Fe_,XIV,T)
     +          + x_(Fe_XV)    * x_(He_I) * nHtot * he_ct_r(Fe_,XV,T)
     +          + x_(Fe_XVI)   * x_(He_I) * nHtot * he_ct_r(Fe_,XVI,T)
     +          + x_(Fe_XVII)  * x_(He_I) * nHtot * he_ct_r(Fe_,XVII,T)
     +          + x_(Fe_XVIII) * x_(He_I) * nHtot * he_ct_r(Fe_,XVIII,T)
     +          + x_(Fe_XIX)   * x_(He_I) * nHtot * he_ct_r(Fe_,XIX,T)
      dx_(He_II)  = dx_(He_II) 
     +          + x_(Fe_XX)    * x_(He_I) * nHtot * he_ct_r(Fe_,XX,T)
     +          + x_(Fe_XXI)   * x_(He_I) * nHtot * he_ct_r(Fe_,XXI,T)
     +          + x_(Fe_XXII)  * x_(He_I) * nHtot * he_ct_r(Fe_,XXII,T)
     +          + x_(Fe_XXIII) * x_(He_I) * nHtot * he_ct_r(Fe_,XXIII,T)
     +          + x_(Fe_XXIV)  * x_(He_I) * nHtot * he_ct_r(Fe_,XXIV,T)
     +          + x_(Fe_XXV)   * x_(He_I) * nHtot * he_ct_r(Fe_,XXV,T)
     +          + x_(Fe_XXVI)  * x_(He_I) * nHtot * he_ct_r(Fe_,XXVI,T)
     +          + xFeXXVII     * x_(He_I) * nHtot * he_ct_r(Fe_,XXVII,T)



      dx_(He_II)  = dx_(He_II) 
     +          - x_(C_I)      * x_(He_II) * nHtot * he_ct_i(C_,I,T)
     +          - x_(C_II)     * x_(He_II) * nHtot * he_ct_i(C_,II,T)
     +          - x_(C_III)    * x_(He_II) * nHtot * he_ct_i(C_,III,T)
     +          - x_(C_IV)     * x_(He_II) * nHtot * he_ct_i(C_,IV,T)
     +          - x_(C_V)      * x_(He_II) * nHtot * he_ct_i(C_,V,T)
     +          - x_(C_VI)     * x_(He_II) * nHtot * he_ct_i(C_,VI,T)

      dx_(He_II)  = dx_(He_II) 
     +          - x_(N_I)      * x_(He_II) * nHtot * he_ct_i(N_,I,T) 
     +          - x_(N_II)     * x_(He_II) * nHtot * he_ct_i(N_,II,T)
     +          - x_(N_III)    * x_(He_II) * nHtot * he_ct_i(N_,III,T)
     +          - x_(N_IV)     * x_(He_II) * nHtot * he_ct_i(N_,IV,T)
     +          - x_(N_V)      * x_(He_II) * nHtot * he_ct_i(N_,V,T)
     +          - x_(N_VI)     * x_(He_II) * nHtot * he_ct_i(N_,VI,T)
     +          - x_(N_VII)    * x_(He_II) * nHtot * he_ct_i(N_,VII,T)

      dx_(He_II)  = dx_(He_II) 
     +          - x_(O_I)      * x_(He_II) * nHtot * he_ct_i(O_,I,T)
     +          - x_(O_II)     * x_(He_II) * nHtot * he_ct_i(O_,II,T) 
     +          - x_(O_III)    * x_(He_II) * nHtot * he_ct_i(O_,III,T)
     +          - x_(O_IV)     * x_(He_II) * nHtot * he_ct_i(O_,IV,T) 
     +          - x_(O_V)      * x_(He_II) * nHtot * he_ct_i(O_,V,T)
     +          - x_(O_VI)     * x_(He_II) * nHtot * he_ct_i(O_,VI,T) 
     +          - x_(O_VII)    * x_(He_II) * nHtot * he_ct_i(O_,VII,T)
     +          - x_(O_VIII)   * x_(He_II) * nHtot * he_ct_i(O_,VIII,T)

      dx_(He_II)  = dx_(He_II) 
     +          - x_(Ne_I)     * x_(He_II) * nHtot * he_ct_i(Ne_,I,T) 
     +          - x_(Ne_II)    * x_(He_II) * nHtot * he_ct_i(Ne_,II,T) 
     +          - x_(Ne_III)   * x_(He_II) * nHtot * he_ct_i(Ne_,III,T)
     +          - x_(Ne_IV)    * x_(He_II) * nHtot * he_ct_i(Ne_,IV,T)
     +          - x_(Ne_V)     * x_(He_II) * nHtot * he_ct_i(Ne_,V,T)
     +          - x_(Ne_VI)    * x_(He_II) * nHtot * he_ct_i(Ne_,VI,T)
     +          - x_(Ne_VII)   * x_(He_II) * nHtot * he_ct_i(Ne_,VII,T)
     +          - x_(Ne_VIII)  * x_(He_II) * nHtot * he_ct_i(Ne_,VIII,T)
     +          - x_(Ne_IX)    * x_(He_II) * nHtot * he_ct_i(Ne_,IX,T) 
     +          - x_(Ne_X)     * x_(He_II) * nHtot * he_ct_i(Ne_,X,T)

      dx_(He_II)  = dx_(He_II) 
     +          - x_(Mg_I)    * x_(He_II) * nHtot * he_ct_i(Mg_,I,T)
     +          - x_(Mg_II)   * x_(He_II) * nHtot * he_ct_i(Mg_,II,T)
     +          - x_(Mg_III)  * x_(He_II) * nHtot * he_ct_i(Mg_,III,T)
     +          - x_(Mg_IV)   * x_(He_II) * nHtot * he_ct_i(Mg_,IV,T)
     +          - x_(Mg_V)    * x_(He_II) * nHtot * he_ct_i(Mg_,V,T)
     +          - x_(Mg_VI)   * x_(He_II) * nHtot * he_ct_i(Mg_,VI,T)
     +          - x_(Mg_VII)  * x_(He_II) * nHtot * he_ct_i(Mg_,VII,T)
     +          - x_(Mg_VIII) * x_(He_II) * nHtot * he_ct_i(Mg_,VIII,T)
     +          - x_(Mg_IX)   * x_(He_II) * nHtot * he_ct_i(Mg_,IX,T)
     +          - x_(Mg_X)    * x_(He_II) * nHtot * he_ct_i(Mg_,X,T)
     +          - x_(Mg_XI)   * x_(He_II) * nHtot * he_ct_i(Mg_,XI,T)
     +          - x_(Mg_XII)  * x_(He_II) * nHtot * he_ct_i(Mg_,XII,T)

      dx_(He_II)  = dx_(He_II) 
     +          - x_(Si_I)    * x_(He_II) * nHtot * he_ct_i(Si_,I,T)
     +          - x_(Si_II)   * x_(He_II) * nHtot * he_ct_i(Si_,II,T)
     +          - x_(Si_III)  * x_(He_II) * nHtot * he_ct_i(Si_,III,T)
     +          - x_(Si_IV)   * x_(He_II) * nHtot * he_ct_i(Si_,IV,T)
     +          - x_(Si_V)    * x_(He_II) * nHtot * he_ct_i(Si_,V,T)
     +          - x_(Si_VI)   * x_(He_II) * nHtot * he_ct_i(Si_,VI,T)
     +          - x_(Si_VII)  * x_(He_II) * nHtot * he_ct_i(Si_,VII,T)
     +          - x_(Si_VIII) * x_(He_II) * nHtot * he_ct_i(Si_,VIII,T)
     +          - x_(Si_IX)   * x_(He_II) * nHtot * he_ct_i(Si_,IX,T)
     +          - x_(Si_X)    * x_(He_II) * nHtot * he_ct_i(Si_,X,T)
     +          - x_(Si_XI)   * x_(He_II) * nHtot * he_ct_i(Si_,XI,T)
     +          - x_(Si_XII)  * x_(He_II) * nHtot * he_ct_i(Si_,XII,T)
     +          - x_(Si_XIII) * x_(He_II) * nHtot * he_ct_i(Si_,XIII,T) 
     +          - x_(Si_XIV)  * x_(He_II) * nHtot * he_ct_i(Si_,XIV,T) 

      dx_(He_II)  = dx_(He_II) 
     +          - x_(S_I)     * x_(He_II) * nHtot * he_ct_i(S_,I,T)
     +          - x_(S_II)    * x_(He_II) * nHtot * he_ct_i(S_,II,T)
     +          - x_(S_III)   * x_(He_II) * nHtot * he_ct_i(S_,III,T)
     +          - x_(S_IV)    * x_(He_II) * nHtot * he_ct_i(S_,IV,T)
     +          - x_(S_V)     * x_(He_II) * nHtot * he_ct_i(S_,V,T)
     +          - x_(S_VI)    * x_(He_II) * nHtot * he_ct_i(S_,VI,T)
     +          - x_(S_VII)   * x_(He_II) * nHtot * he_ct_i(S_,VII,T)
     +          - x_(S_VIII)  * x_(He_II) * nHtot * he_ct_i(S_,VIII,T) 
     +          - x_(S_IX)    * x_(He_II) * nHtot * he_ct_i(S_,IX,T)
     +          - x_(S_X)     * x_(He_II) * nHtot * he_ct_i(S_,X,T)
     +          - x_(S_XI)    * x_(He_II) * nHtot * he_ct_i(S_,XI,T)
     +          - x_(S_XII)   * x_(He_II) * nHtot * he_ct_i(S_,XII,T)
     +          - x_(S_XIII)  * x_(He_II) * nHtot * he_ct_i(S_,XIII,T)
     +          - x_(S_XIV)   * x_(He_II) * nHtot * he_ct_i(S_,XIV,T)
     +          - x_(S_XV)    * x_(He_II) * nHtot * he_ct_i(S_,XV,T)
     +          - x_(S_XVI)   * x_(He_II) * nHtot * he_ct_i(S_,XVI,T)

      dx_(He_II)  = dx_(He_II) 
     +         - x_(Fe_I)     * x_(He_II) * nHtot * he_ct_i(Fe_,I,T)
     +         - x_(Fe_II)    * x_(He_II) * nHtot * he_ct_i(Fe_,II,T)
     +         - x_(Fe_III)   * x_(He_II) * nHtot * he_ct_i(Fe_,III,T)
     +         - x_(Fe_IV)    * x_(He_II) * nHtot * he_ct_i(Fe_,IV,T) 
     +         - x_(Fe_V)     * x_(He_II) * nHtot * he_ct_i(Fe_,V,T)
     +         - x_(Fe_VI)    * x_(He_II) * nHtot * he_ct_i(Fe_,VI,T)
     +         - x_(Fe_VII)   * x_(He_II) * nHtot * he_ct_i(Fe_,VII,T)
     +         - x_(Fe_VIII)  * x_(He_II) * nHtot * he_ct_i(Fe_,VIII,T)
     +         - x_(Fe_IX)    * x_(He_II) * nHtot * he_ct_i(Fe_,IX,T) 
     +         - x_(Fe_X)     * x_(He_II) * nHtot * he_ct_i(Fe_,X,T)
     +         - x_(Fe_XI)    * x_(He_II) * nHtot * he_ct_i(Fe_,XI,T)
     +         - x_(Fe_XII)   * x_(He_II) * nHtot * he_ct_i(Fe_,XII,T)
     +         - x_(Fe_XIII)  * x_(He_II) * nHtot * he_ct_i(Fe_,XIII,T)
     +         - x_(Fe_XIV)   * x_(He_II) * nHtot * he_ct_i(Fe_,XIV,T)
     +         - x_(Fe_XV)    * x_(He_II) * nHtot * he_ct_i(Fe_,XV,T)
     +         - x_(Fe_XVI)   * x_(He_II) * nHtot * he_ct_i(Fe_,XVI,T)
     +         - x_(Fe_XVII)  * x_(He_II) * nHtot * he_ct_i(Fe_,XVII,T)
     +         - x_(Fe_XVIII) * x_(He_II) * nHtot * he_ct_i(Fe_,XVIII,T)
      dx_(He_II)  = dx_(He_II) 
     +         - x_(Fe_XIX)   * x_(He_II) * nHtot * he_ct_i(Fe_,XIX,T)
     +         - x_(Fe_XX)    * x_(He_II) * nHtot * he_ct_i(Fe_,XX,T)
     +         - x_(Fe_XXI)   * x_(He_II) * nHtot * he_ct_i(Fe_,XXI,T)
     +         - x_(Fe_XXII)  * x_(He_II) * nHtot * he_ct_i(Fe_,XXII,T)
     +         - x_(Fe_XXIII) * x_(He_II) * nHtot * he_ct_i(Fe_,XXIII,T)
     +         - x_(Fe_XXIV)  * x_(He_II) * nHtot * he_ct_i(Fe_,XXIV,T) 
     +         - x_(Fe_XXV)   * x_(He_II) * nHtot * he_ct_i(Fe_,XXV,T) 
     +         - x_(Fe_XXVI)  * x_(He_II) * nHtot * he_ct_i(Fe_,XXVI,T) 


c      if (n_e/nHtot.le.0.1) then !add secondary ionizations
c         dx_(He_I) = dx_(He_I) 
c     +            - x_(He_I) * nHtot * He_sec_rate(x_,nsize,n_e/nHtot)
c
c         dx_(He_II) = dx_(He_II) 
c     +            + x_(He_I) * nHtot * He_sec_rate(x_,nsize,n_e/nHtot)
c     +            - x_(He_II) * nHtot *HeII_sec_rate(x_,nsize,n_e/nHtot)
c      endif

c================
c     Carbon
c================

      dx_(C_I)   = 
     +         x_(C_II) * n_e * allrec_mat(C_,II)          !rad + 3bod + dr
     +       + x_(C_II) * x_(H_I) * nHtot * ct_rec(C_,II,T) !H CT "rec" 
     +       - x_(C_I)  * xHII * nHtot * ct_ion(C_,I,T)     !H CT "ionization"
     +       - x_(C_I)  * Gamma(C_,I)                      !photoionization
     +       - x_(C_I)  * n_e * colion(C_,I,T)             !col. ionization
     +       + x_(C_II) * x_(He_I) * nHtot * he_ct_r(C_,II,T) !He CT rec
     +       - x_(C_I)  * x_(He_II)* nHtot * he_ct_i(C_,I,T)  !He CT ion

      dx_(C_II)  = 
     +         x_(C_III) * n_e * allrec_mat(C_,III)          !rad + 3bod + dr
     +       - x_(C_II)  * n_e * allrec_mat(C_,II)           !rad + 3bod + dr
     +       + x_(C_III) * x_(H_I) * nHtot * ct_rec(C_,III,T) !H CT "rec"
     +       - x_(C_II)  * x_(H_I) * nHtot * ct_rec(C_,II,T)  !H CT "rec"
     +       + x_(C_I)   * xHII * nHtot * ct_ion(C_,I,T)      !H CT "ionization"
     +       - x_(C_II)  * xHII * nHtot * ct_ion(C_,II,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, C_II,C_,II)           !photoI+Auger
     +       - x_(C_II)  * Gamma(C_,II)                      !photoionization
     +       + x_(C_I)   * n_e * colion(C_,I,T)              !col. ionization
     +       - x_(C_II)  * n_e * colion(C_,II,T)             !col. ionization
     +       + x_(C_III) * x_(He_I) * nHtot * he_ct_r(C_,III,T) !He CT "rec"
     +       - x_(C_II)  * x_(He_I) * nHtot * he_ct_r(C_,II,T)  !He CT "rec"
     +       + x_(C_I)   * x_(He_II) * nHtot * he_ct_i(C_,I,T)  !He CT "ionization"
     +       - x_(C_II)  * x_(He_II) * nHtot * he_ct_i(C_,II,T) !He CT "ionization"

      dx_(C_III) = 
     +         x_(C_IV)  * n_e * allrec_mat(C_,IV)           !rad + 3bod + dr
     +       - x_(C_III) * n_e * allrec_mat(C_,III)          !rad + 3bod + dr
     +       + x_(C_IV)  * x_(H_I) * nHtot * ct_rec(C_,IV,T)  !H CT "rec"
     +       - x_(C_III) * x_(H_I) * nHtot * ct_rec(C_,III,T) !H CT "rec" 
     +       + x_(C_II)  * xHII * nHtot * ct_ion(C_,II,T)     !H CT "ionization"
     +       - x_(C_III) * xHII * nHtot * ct_ion(C_,III,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, C_III,C_,III)         !photoI+Auger
     +       - x_(C_III) * Gamma(C_,III)                     !photoionization
     +       + x_(C_II)  * n_e * colion(C_,II,T)             !col. ionization
     +       - x_(C_III) * n_e * colion(C_,III,T)            !col. ionization
     +       + x_(C_IV)  * x_(He_I) * nHtot * he_ct_r(C_,IV,T)  !He CT "rec"
     +       - x_(C_III) * x_(He_I) * nHtot * he_ct_r(C_,III,T) !He CT "rec" 
     +       + x_(C_II)  * x_(He_II) * nHtot * he_ct_i(C_,II,T) !He CT "ionization"
     +       - x_(C_III) * x_(He_II) * nHtot * he_ct_i(C_,III,T)!He CT "ionization"

      dx_(C_IV)  =
     +         x_(C_V)   * n_e * allrec_mat(C_,V)           !rad + 3bod + dr
     +       - x_(C_IV)  * n_e * allrec_mat(C_,IV)          !rad + 3bod + dr
     +       + x_(C_V)   * x_(H_I) * nHtot * ct_rec(C_,V,T)  !H CT "rec"
     +       - x_(C_IV)  * x_(H_I) * nHtot * ct_rec(C_,IV,T) !H CT "rec" 
     +       + x_(C_III) * xHII * nHtot * ct_ion(C_,III,T)   !H CT "ionization"
     +       - x_(C_IV)  * xHII * nHtot * ct_ion(C_,IV,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, C_IV,C_,IV)      !photoI+Auger
     +       - x_(C_IV)  * Gamma(C_,IV)                     !photoionization
     +       + x_(C_III) * n_e * colion(C_,III,T)           !col. ionization
     +       - x_(C_IV)  * n_e * colion(C_,IV,T)            !col. ionization
     +       + x_(C_V)   * x_(He_I) * nHtot * he_ct_r(C_,V,T)  !He CT "rec"
     +       - x_(C_IV)  * x_(He_I) * nHtot * he_ct_r(C_,IV,T) !He CT "rec" 
     +       + x_(C_III) * x_(He_II) * nHtot * he_ct_i(C_,III,T)!He CT "ionization"
     +       - x_(C_IV)  * x_(He_II) * nHtot * he_ct_i(C_,IV,T) !He CT "ionization"

      dx_(C_V)   =
     +         x_(C_VI) * n_e * allrec_mat(C_,VI)          !rad + 3bod + dr
     +       - x_(C_V)  * n_e * allrec_mat(C_,V)           !rad + 3bod + dr
     +       + x_(C_VI) * x_(H_I) * nHtot * ct_rec(C_,VI,T) !H CT "rec"
     +       - x_(C_V)  * x_(H_I) * nHtot * ct_rec(C_,V,T)  !H CT "rec" 
     +       + x_(C_IV) * xHII * nHtot * ct_ion(C_,IV,T)    !H CT "ionization"
     +       - x_(C_V)  * xHII * nHtot * ct_ion(C_,V,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, C_V,C_,V)         !photoI+Auger
     +       - x_(C_V)  * Gamma(C_,V)                      !photoionization
     +       + x_(C_IV) * n_e * colion(C_,IV,T)            !col. ionization
     +       - x_(C_V)  * n_e * colion(C_,V,T)             !col. ionization
     +       + x_(C_VI) * x_(He_I) * nHtot * he_ct_r(C_,VI,T) !He CT "rec"
     +       - x_(C_V)  * x_(He_I) * nHtot * he_ct_r(C_,V,T)  !He CT "rec" 
     +       + x_(C_IV) * x_(He_II) * nHtot * he_ct_i(C_,IV,T)!He CT "ionization"
     +       - x_(C_V)  * x_(He_II) * nHtot * he_ct_i(C_,V,T) !He CT "ionization"

      dx_(C_VI)  =
     +         xCVII * n_e * allrec_mat(C_,VII)             !rad + 3bod + dr
     +       - x_(C_VI)  * n_e * allrec_mat(C_,VI)          !rad + 3bod + dr
     +       + xCVII * x_(H_I) * nHtot * ct_rec(C_,VII,T)    !H CT "rec"
     +       - x_(C_VI)  * x_(H_I) * nHtot * ct_rec(C_,VI,T) !H CT "rec" 
     +       + x_(C_V)   * xHII * nHtot * ct_ion(C_,V,T)     !H CT "ionization"
     +       - x_(C_VI)  * xHII * nHtot * ct_ion(C_,VI,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, C_VI,C_,VI)         !photoI+Auger
     +       - x_(C_VI)  * Gamma(C_,VI)                     !photoionization
     +       + x_(C_V)   * n_e * colion(C_,V,T)             !col. ionization
     +       - x_(C_VI)  * n_e * colion(C_,VI,T)            !col. ionization
     +       + xCVII * x_(He_I) * nHtot * he_ct_r(C_,VII,T)    !He CT "rec"
     +       - x_(C_VI)  * x_(He_I) * nHtot * he_ct_r(C_,VI,T) !He CT "rec" 
     +       + x_(C_V)   * x_(He_II) * nHtot * he_ct_i(C_,V,T) !He CT "ionization"
     +       - x_(C_VI)  * x_(He_II) * nHtot * he_ct_i(C_,VI,T)!He CT "ionization"


c================
c     Nitrogen
c================

      dx_(N_I)   =  
     +         x_(N_II) * n_e * allrec_mat(N_,II)          !rad + 3bod + dr
     +       + x_(N_II) * x_(H_I) * nHtot * ct_rec(N_,II,T) !H CT "rec" 
     +       - x_(N_I)  * xHII * nHtot * ct_ion(N_,I,T)     !H CT "ionization"
     +       - x_(N_I)  * Gamma(N_,I)                      !photoionization
     +       - x_(N_I)  * n_e * colion(N_,I,T)             !col. ionization
     +       + x_(N_II) * x_(He_I) * nHtot * he_ct_r(N_,II,T) !He CT "rec" 
     +       - x_(N_I)  * x_(He_II) * nHtot * he_ct_i(N_,I,T) !He CT "ionization"

      dx_(N_II)  = 
     +         x_(N_III) * n_e * allrec_mat(N_,III)          !rad + 3bod + dr
     +       - x_(N_II)  * n_e * allrec_mat(N_,II)           !rad + 3bod + dr
     +       + x_(N_III) * x_(H_I) * nHtot * ct_rec(N_,III,T) !H CT "rec"
     +       - x_(N_II)  * x_(H_I) * nHtot * ct_rec(N_,II,T)  !H CT "rec"
     +       + x_(N_I)   * xHII * nHtot * ct_ion(N_,I,T)      !H CT "ionization"
     +       - x_(N_II)  * xHII * nHtot * ct_ion(N_,II,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, N_II,N_,II)           !photoI+Auger
     +       - x_(N_II)  * Gamma(N_,II)                      !photoionization
     +       + x_(N_I)   * n_e * colion(N_,I,T)              !col. ionization
     +       - x_(N_II)  * n_e * colion(N_,II,T)             !col. ionization
     +       + x_(N_III) * x_(He_I) * nHtot * he_ct_r(N_,III,T) !He CT "rec"
     +       - x_(N_II)  * x_(He_I) * nHtot * he_ct_r(N_,II,T)  !He CT "rec"
     +       + x_(N_I)   * x_(He_II) * nHtot * he_ct_i(N_,I,T)  !He CT "ionization"
     +       - x_(N_II)  * x_(He_II) * nHtot * he_ct_i(N_,II,T) !He CT "ionization"

      dx_(N_III) = 
     +         x_(N_IV)  * n_e * allrec_mat(N_,IV)           !rad + 3bod + dr
     +       - x_(N_III) * n_e * allrec_mat(N_,III)          !rad + 3bod + dr
     +       + x_(N_IV)  * x_(H_I) * nHtot * ct_rec(N_,IV,T)  !H CT "rec"
     +       - x_(N_III) * x_(H_I) * nHtot * ct_rec(N_,III,T) !H CT "rec" 
     +       + x_(N_II)  * xHII * nHtot * ct_ion(N_,II,T)     !H CT "ionization"
     +       - x_(N_III) * xHII * nHtot * ct_ion(N_,III,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, N_III,N_,III)         !photoI+Auger
     +       - x_(N_III) * Gamma(N_,III)                     !photoionization
     +       + x_(N_II)  * n_e * colion(N_,II,T)             !col. ionization
     +       - x_(N_III) * n_e * colion(N_,III,T)            !col. ionization
     +       + x_(N_IV)  * x_(He_I) * nHtot * he_ct_r(N_,IV,T)  !He CT "rec"
     +       - x_(N_III) * x_(He_I) * nHtot * he_ct_r(N_,III,T) !He CT "rec" 
     +       + x_(N_II)  * x_(He_II) * nHtot * he_ct_i(N_,II,T) !He CT "ionization"
     +       - x_(N_III) * x_(He_II) * nHtot * he_ct_i(N_,III,T)!He CT "ionization"

      dx_(N_IV)  = 
     +         x_(N_V)   * n_e * allrec_mat(N_,V)           !rad + 3bod + dr
     +       - x_(N_IV)  * n_e * allrec_mat(N_,IV)          !rad + 3bod + dr
     +       + x_(N_V)   * x_(H_I) * nHtot * ct_rec(N_,V,T)  !H CT "rec"
     +       - x_(N_IV)  * x_(H_I) * nHtot * ct_rec(N_,IV,T) !H CT "rec" 
     +       + x_(N_III) * xHII * nHtot * ct_ion(N_,III,T)   !H CT "ionization"
     +       - x_(N_IV)  * xHII * nHtot * ct_ion(N_,IV,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, N_IV,N_,IV)         !photoI+Auger
     +       - x_(N_IV)  * Gamma(N_,IV)                     !photoionization
     +       + x_(N_III) * n_e * colion(N_,III,T)           !col. ionization
     +       - x_(N_IV)  * n_e * colion(N_,IV,T)            !col. ionization
     +       + x_(N_V)   * x_(He_I) * nHtot * he_ct_r(N_,V,T)  !He CT "rec"
     +       - x_(N_IV)  * x_(He_I) * nHtot * he_ct_r(N_,IV,T) !He CT "rec" 
     +       + x_(N_III) * x_(He_II) * nHtot * he_ct_i(N_,III,T)!He CT "ionization"
     +       - x_(N_IV)  * x_(He_II) * nHtot * he_ct_i(N_,IV,T) !He CT "ionization"

      dx_(N_V)   = 
     +         x_(N_VI) * n_e * allrec_mat(N_,VI)          !rad + 3bod + dr
     +       - x_(N_V)  * n_e * allrec_mat(N_,V)           !rad + 3bod + dr
     +       + x_(N_VI) * x_(H_I) * nHtot * ct_rec(N_,VI,T) !H CT "rec"
     +       - x_(N_V)  * x_(H_I) * nHtot * ct_rec(N_,V,T)  !H CT "rec" 
     +       + x_(N_IV) * xHII * nHtot * ct_ion(N_,IV,T)    !H CT "ionization"
     +       - x_(N_V)  * xHII * nHtot * ct_ion(N_,V,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, N_V,N_,V)       !photoI+Auger
     +       - x_(N_V)  * Gamma(N_,V)                      !photoionization
     +       + x_(N_IV) * n_e * colion(N_,IV,T)            !col. ionization
     +       - x_(N_V)  * n_e * colion(N_,V,T)             !col. ionization
     +       + x_(N_VI) * x_(He_I) * nHtot * he_ct_r(N_,VI,T) !He CT "rec"
     +       - x_(N_V)  * x_(He_I) * nHtot * he_ct_r(N_,V,T)  !He CT "rec" 
     +       + x_(N_IV) * x_(He_II) * nHtot * he_ct_i(N_,IV,T)!He CT "ionization"
     +       - x_(N_V)  * x_(He_II) * nHtot * he_ct_i(N_,V,T) !He CT "ionization"

      dx_(N_VI)  = 
     +         x_(N_VII) * n_e * allrec_mat(N_,VII)          !rad + 3bod + dr
     +       - x_(N_VI)  * n_e * allrec_mat(N_,VI)           !rad + 3bod + dr
     +       + x_(N_VII) * x_(H_I) * nHtot * ct_rec(N_,VII,T) !H CT "rec"
     +       - x_(N_VI)  * x_(H_I) * nHtot * ct_rec(N_,VI,T)  !H CT "rec" 
     +       + x_(N_V)   * xHII * nHtot * ct_ion(N_,V,T)      !H CT "ionization"
     +       - x_(N_VI)  * xHII * nHtot * ct_ion(N_,VI,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, N_VI,N_,VI)         !photoI+Auger
     +       - x_(N_VI)  * Gamma(N_,VI)                      !photoionization
     +       + x_(N_V)   * n_e * colion(N_,V,T)              !col. ionization
     +       - x_(N_VI)  * n_e * colion(N_,VI,T)             !col. ionization
     +       + x_(N_VII) * x_(He_I) * nHtot * he_ct_r(N_,VII,T) !He CT "rec"
     +       - x_(N_VI)  * x_(He_I) * nHtot * he_ct_r(N_,VI,T)  !He CT "rec" 
     +       + x_(N_V)   * x_(He_II) * nHtot * he_ct_i(N_,V,T)  !He CT "ionization"
     +       - x_(N_VI)  * x_(He_II) * nHtot * he_ct_i(N_,VI,T) !He CT "ionization"

      dx_(N_VII) = 
     +         xNVIII * n_e * allrec_mat(N_,VIII)             !rad + 3bod + dr
     +       - x_(N_VII)  * n_e * allrec_mat(N_,VII)          !rad + 3bod + dr
     +       + xNVIII * x_(H_I) * nHtot * ct_rec(N_,VIII,T)    !H CT "rec"
     +       - x_(N_VII)  * x_(H_I) * nHtot * ct_rec(N_,VII,T) !H CT "rec" 
     +       + x_(N_VI)   * xHII * nHtot * ct_ion(N_,VI,T)     !H CT "ionization"
     +       - x_(N_VII)  * xHII * nHtot * ct_ion(N_,VII,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, N_VII,N_,VII)         !photoI+Auger
     +       - x_(N_VII)  * Gamma(N_,VII)                     !photoionization
     +       + x_(N_VI)   * n_e * colion(N_,VI,T)             !col. ionization
     +       - x_(N_VII)  * n_e * colion(N_,VII,T)            !col. ionization
     +       + xNVIII * x_(He_I) * nHtot * he_ct_r(N_,VIII,T)    !He CT "rec"
     +       - x_(N_VII)  * x_(He_I) * nHtot * he_ct_r(N_,VII,T) !He CT "rec" 
     +       + x_(N_VI)   * x_(He_II) * nHtot * he_ct_i(N_,VI,T) !He CT "ionization"
     +       - x_(N_VII)  * x_(He_II) * nHtot * he_ct_i(N_,VII,T)!He CT "ionization"

c================
c     Oxygen
c================

      dx_(O_I)   = 
     +         x_(O_II) * n_e * allrec_mat(O_,II)          !rad + 3bod + dr
     +       + x_(O_II) * x_(H_I) * nHtot * ct_rec(O_,II,T) !H CT "rec" 
     +       - x_(O_I)  * xHII * nHtot * ct_ion(O_,I,T)     !H CT "ionization"
     +       - x_(O_I)  * Gamma(O_,I)                      !photoionization
     +       - x_(O_I)  * n_e * colion(O_,I,T)             !col. ionization
     +       + x_(O_II) * x_(He_I) * nHtot * he_ct_r(O_,II,T) !He CT "rec" 
     +       - x_(O_I)  * x_(He_II) * nHtot * he_ct_i(O_,I,T) !He CT "ionization"

      dx_(O_II)  = 
     +         x_(O_III) * n_e * allrec_mat(O_,III)          !rad + 3bod + dr
     +       - x_(O_II)  * n_e * allrec_mat(O_,II)           !rad + 3bod + dr
     +       + x_(O_III) * x_(H_I) * nHtot * ct_rec(O_,III,T) !H CT "rec"
     +       - x_(O_II)  * x_(H_I) * nHtot * ct_rec(O_,II,T)  !H CT "rec"
     +       + x_(O_I)   * xHII * nHtot * ct_ion(O_,I,T)      !H CT "ionization"
     +       - x_(O_II)  * xHII * nHtot * ct_ion(O_,II,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, O_II,O_,II)         !photoI+Auger
     +       - x_(O_II)  * Gamma(O_,II)                      !photoionization
     +       + x_(O_I)   * n_e * colion(O_,I,T)              !col. ionization
     +       - x_(O_II)  * n_e * colion(O_,II,T)             !col. ionization
     +       + x_(O_III) * x_(He_I) * nHtot * he_ct_r(O_,III,T) !He CT "rec"
     +       - x_(O_II)  * x_(He_I) * nHtot * he_ct_r(O_,II,T)  !He CT "rec"
     +       + x_(O_I)   * x_(He_II) * nHtot * he_ct_i(O_,I,T)  !He CT "ionization"
     +       - x_(O_II)  * x_(He_II) * nHtot * he_ct_i(O_,II,T) !He CT "ionization"

      dx_(O_III) = 
     +         x_(O_IV)  * n_e * allrec_mat(O_,IV)           !rad + 3bod + dr
     +       - x_(O_III) * n_e * allrec_mat(O_,III)          !rad + 3bod + dr
     +       + x_(O_IV)  * x_(H_I) * nHtot * ct_rec(O_,IV,T)  !H CT "rec"
     +       - x_(O_III) * x_(H_I) * nHtot * ct_rec(O_,III,T) !H CT "rec" 
     +       + x_(O_II)  * xHII * nHtot * ct_ion(O_,II,T)     !H CT "ionization"
     +       - x_(O_III) * xHII * nHtot * ct_ion(O_,III,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, O_III,O_,III)         !photoI+Auger
     +       - x_(O_III) * Gamma(O_,III)                     !photoionization
     +       + x_(O_II)  * n_e * colion(O_,II,T)             !col. ionization
     +       - x_(O_III) * n_e * colion(O_,III,T)            !col. ionization
     +       + x_(O_IV)  * x_(He_I) * nHtot * he_ct_r(O_,IV,T)  !He CT "rec"
     +       - x_(O_III) * x_(He_I) * nHtot * he_ct_r(O_,III,T) !He CT "rec" 
     +       + x_(O_II)  * x_(He_II) * nHtot * he_ct_i(O_,II,T) !He CT "ionization"
     +       - x_(O_III) * x_(He_II) * nHtot * he_ct_i(O_,III,T)!He CT "ionization"

      dx_(O_IV)  = 
     +         x_(O_V)   * n_e * allrec_mat(O_,V)           !rad + 3bod + dr
     +       - x_(O_IV)  * n_e * allrec_mat(O_,IV)          !rad + 3bod + dr
     +       + x_(O_V)   * x_(H_I) * nHtot * ct_rec(O_,V,T)  !H CT "rec"
     +       - x_(O_IV)  * x_(H_I) * nHtot * ct_rec(O_,IV,T) !H CT "rec" 
     +       + x_(O_III) * xHII * nHtot * ct_ion(O_,III,T)   !H CT "ionization"
     +       - x_(O_IV)  * xHII * nHtot * ct_ion(O_,IV,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, O_IV,O_,IV)         !photoI+Auger
     +       - x_(O_IV)  * Gamma(O_,IV)                     !photoionization
     +       + x_(O_III) * n_e * colion(O_,III,T)           !col. ionization
     +       - x_(O_IV)  * n_e * colion(O_,IV,T)            !col. ionization
     +       + x_(O_V)   * x_(He_I) * nHtot * he_ct_r(O_,V,T)  !He CT "rec"
     +       - x_(O_IV)  * x_(He_I) * nHtot * he_ct_r(O_,IV,T) !He CT "rec" 
     +       + x_(O_III) * x_(He_II) * nHtot * he_ct_i(O_,III,T)!He CT "ionization"
     +       - x_(O_IV)  * x_(He_II) * nHtot * he_ct_i(O_,IV,T) !He CT "ionization"

      dx_(O_V)   = 
     +         x_(O_VI) * n_e * allrec_mat(O_,VI)          !rad + 3bod + dr
     +       - x_(O_V)  * n_e * allrec_mat(O_,V)           !rad + 3bod + dr
     +       + x_(O_VI) * x_(H_I) * nHtot * ct_rec(O_,VI,T) !H CT "rec"
     +       - x_(O_V)  * x_(H_I) * nHtot * ct_rec(O_,V,T)  !H CT "rec" 
     +       + x_(O_IV) * xHII * nHtot * ct_ion(O_,IV,T)    !H CT "ionization"
     +       - x_(O_V)  * xHII * nHtot * ct_ion(O_,V,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, O_V,O_,V)         !photoI+Auger
     +       - x_(O_V)  * Gamma(O_,V)                      !photoionization
     +       + x_(O_IV) * n_e * colion(O_,IV,T)            !col. ionization
     +       - x_(O_V)  * n_e * colion(O_,V,T)             !col. ionization
     +       + x_(O_VI) * x_(He_I) * nHtot * he_ct_r(O_,VI,T) !He CT "rec"
     +       - x_(O_V)  * x_(He_I) * nHtot * he_ct_r(O_,V,T)  !He CT "rec" 
     +       + x_(O_IV) * x_(He_II) * nHtot * he_ct_i(O_,IV,T)!He CT "ionization"
     +       - x_(O_V)  * x_(He_II) * nHtot * he_ct_i(O_,V,T) !He CT "ionization"

      dx_(O_VI)  = 
     +         x_(O_VII) * n_e * allrec_mat(O_,VII)          !rad + 3bod + dr
     +       - x_(O_VI)  * n_e * allrec_mat(O_,VI)           !rad + 3bod + dr
     +       + x_(O_VII) * x_(H_I) * nHtot * ct_rec(O_,VII,T) !H CT "rec"
     +       - x_(O_VI)  * x_(H_I) * nHtot * ct_rec(O_,VI,T)  !H CT "rec" 
     +       + x_(O_V)   * xHII * nHtot * ct_ion(O_,V,T)      !H CT "ionization"
     +       - x_(O_VI)  * xHII * nHtot * ct_ion(O_,VI,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, O_VI,O_,VI)         !photoI+Auger
     +       - x_(O_VI)  * Gamma(O_,VI)                      !photoionization
     +       + x_(O_V)   * n_e * colion(O_,V,T)              !col. ionization
     +       - x_(O_VI)  * n_e * colion(O_,VI,T)             !col. ionization
     +       + x_(O_VII) * x_(He_I) * nHtot * he_ct_r(O_,VII,T) !He CT "rec"
     +       - x_(O_VI)  * x_(He_I) * nHtot * he_ct_r(O_,VI,T)  !He CT "rec" 
     +       + x_(O_V)   * x_(He_II) * nHtot * he_ct_i(O_,V,T)  !He CT "ionization"
     +       - x_(O_VI)  * x_(He_II) * nHtot * he_ct_i(O_,VI,T) !He CT "ionization"

      dx_(O_VII) = 
     +         x_(O_VIII) * n_e * allrec_mat(O_,VIII)          !rad + 3bod + dr
     +       - x_(O_VII)  * n_e * allrec_mat(O_,VII)           !rad + 3bod + dr
     +       + x_(O_VIII) * x_(H_I) * nHtot * ct_rec(O_,VIII,T) !H CT "rec"
     +       - x_(O_VII)  * x_(H_I) * nHtot * ct_rec(O_,VII,T)  !H CT "rec" 
     +       + x_(O_VI)   * xHII * nHtot * ct_ion(O_,VI,T)      !H CT "ionization"
     +       - x_(O_VII)  * xHII * nHtot * ct_ion(O_,VII,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, O_VII,O_,VII)         !photoI+Auger
     +       - x_(O_VII)  * Gamma(O_,VII)                      !photoionization
     +       + x_(O_VI)   * n_e * colion(O_,VI,T)              !col. ionization
     +       - x_(O_VII)  * n_e * colion(O_,VII,T)             !col. ionization
     +       + x_(O_VIII) * x_(He_I) * nHtot * he_ct_r(O_,VIII,T) !He CT "rec"
     +       - x_(O_VII)  * x_(He_I) * nHtot * he_ct_r(O_,VII,T)  !He CT "rec" 
     +       + x_(O_VI)   * x_(He_II) * nHtot * he_ct_i(O_,VI,T)  !He CT "ionization"
     +       - x_(O_VII)  * x_(He_II) * nHtot * he_ct_i(O_,VII,T) !He CT "ionization"

      dx_(O_VIII) =
     +         xOIX   * n_e * allrec_mat(O_,IX)                !rad + 3bod + dr
     +       - x_(O_VIII) * n_e * allrec_mat(O_,VIII)          !rad + 3bod + dr
     +       + xOIX   * x_(H_I) * nHtot * ct_rec(O_,IX,T)       !H CT "rec"
     +       - x_(O_VIII) * x_(H_I) * nHtot * ct_rec(O_,VIII,T) !H CT "rec" 
     +       + x_(O_VII)  * xHII * nHtot * ct_ion(O_,VII,T)     !H CT "ionization"
     +       - x_(O_VIII) * xHII * nHtot * ct_ion(O_,VIII,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, O_VIII,O_,VIII)         !photoI+Auger
     +       - x_(O_VIII) * Gamma(O_,VIII)                     !photoionization
     +       + x_(O_VII)  * n_e * colion(O_,VII,T)             !col. ionization
     +       - x_(O_VIII) * n_e * colion(O_,VIII,T)            !col. ionization
     +       + xOIX   * x_(He_I) * nHtot * he_ct_r(O_,IX,T)       !He CT "rec"
     +       - x_(O_VIII) * x_(He_I) * nHtot * he_ct_r(O_,VIII,T) !He CT "rec" 
     +       + x_(O_VII)  * x_(He_II) * nHtot * he_ct_i(O_,VII,T) !He CT "ionization"
     +       - x_(O_VIII) * x_(He_II) * nHtot * he_ct_i(O_,VIII,T)!He CT "ionization"

c===============================
c     Neon - added Feb 13, 2005
c==============================

      dx_(Ne_I)   = 
     +         x_(Ne_II) * n_e * allrec_mat(Ne_,II)          !rad + 3bod + dr
     +       + x_(Ne_II) * x_(H_I) * nHtot * ct_rec(Ne_,II,T) !H CT "rec" 
     +       - x_(Ne_I)  * xHII * nHtot * ct_ion(Ne_,I,T)     !H CT "ionization"
     +       - x_(Ne_I)  * Gamma(Ne_,I)                      !photoionization
     +       - x_(Ne_I)  * n_e * colion(Ne_,I,T)             !col. ionization
     +       + x_(Ne_II) * x_(He_I) * nHtot * he_ct_r(Ne_,II,T) !He CT "rec" 
     +       - x_(Ne_I)  * x_(He_II) * nHtot * he_ct_i(Ne_,I,T) !He CT "ionization"

      dx_(Ne_II)  = 
     +         x_(Ne_III) * n_e * allrec_mat(Ne_,III)          !rad + 3bod + dr
     +       - x_(Ne_II)  * n_e * allrec_mat(Ne_,II)           !rad + 3bod + dr
     +       + x_(Ne_III) * x_(H_I) * nHtot * ct_rec(Ne_,III,T) !H CT "rec"
     +       - x_(Ne_II)  * x_(H_I) * nHtot * ct_rec(Ne_,II,T)  !H CT "rec"
     +       + x_(Ne_I)   * xHII * nHtot * ct_ion(Ne_,I,T)      !H CT "ionization"
     +       - x_(Ne_II)  * xHII * nHtot * ct_ion(Ne_,II,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Ne_II,Ne_,II)         !photoI+Auger
     +       - x_(Ne_II)  * Gamma(Ne_,II)                      !photoionization
     +       + x_(Ne_I)   * n_e * colion(Ne_,I,T)              !col. ionization
     +       - x_(Ne_II)  * n_e * colion(Ne_,II,T)             !col. ionization
     +       + x_(Ne_III) * x_(He_I) * nHtot * he_ct_r(Ne_,III,T) !He CT "rec"
     +       - x_(Ne_II)  * x_(He_I) * nHtot * he_ct_r(Ne_,II,T)  !He CT "rec"
     +       + x_(Ne_I)   * x_(He_II) * nHtot * he_ct_i(Ne_,I,T)  !He CT "ionization"
     +       - x_(Ne_II)  * x_(He_II) * nHtot * he_ct_i(Ne_,II,T) !He CT "ionization"

      dx_(Ne_III) = 
     +         x_(Ne_IV)  * n_e * allrec_mat(Ne_,IV)           !rad + 3bod + dr
     +       - x_(Ne_III) * n_e * allrec_mat(Ne_,III)          !rad + 3bod + dr
     +       + x_(Ne_IV)  * x_(H_I) * nHtot * ct_rec(Ne_,IV,T)  !H CT "rec"
     +       - x_(Ne_III) * x_(H_I) * nHtot * ct_rec(Ne_,III,T) !H CT "rec" 
     +       + x_(Ne_II)  * xHII * nHtot * ct_ion(Ne_,II,T)     !H CT "ionization"
     +       - x_(Ne_III) * xHII * nHtot * ct_ion(Ne_,III,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Ne_III,Ne_,III)         !photoI+Auger
     +       - x_(Ne_III) * Gamma(Ne_,III)                     !photoionization
     +       + x_(Ne_II)  * n_e * colion(Ne_,II,T)             !col. ionization
     +       - x_(Ne_III) * n_e * colion(Ne_,III,T)            !col. ionization
     +       + x_(Ne_IV)  * x_(He_I) * nHtot * he_ct_r(Ne_,IV,T)  !He CT "rec"
     +       - x_(Ne_III) * x_(He_I) * nHtot * he_ct_r(Ne_,III,T) !He CT "rec" 
     +       + x_(Ne_II)  * x_(He_II) * nHtot * he_ct_i(Ne_,II,T) !He CT "ionization"
     +       - x_(Ne_III) * x_(He_II) * nHtot * he_ct_i(Ne_,III,T)!He CT "ionization"

      dx_(Ne_IV)  = 
     +         x_(Ne_V)   * n_e * allrec_mat(Ne_,V)           !rad + 3bod + dr
     +       - x_(Ne_IV)  * n_e * allrec_mat(Ne_,IV)          !rad + 3bod + dr
     +       + x_(Ne_V)   * x_(H_I) * nHtot * ct_rec(Ne_,V,T)  !H CT "rec"
     +       - x_(Ne_IV)  * x_(H_I) * nHtot * ct_rec(Ne_,IV,T) !H CT "rec" 
     +       + x_(Ne_III) * xHII * nHtot * ct_ion(Ne_,III,T)   !H CT "ionization"
     +       - x_(Ne_IV)  * xHII * nHtot * ct_ion(Ne_,IV,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Ne_IV,Ne_,IV)         !photoI+Auger
     +       - x_(Ne_IV)  * Gamma(Ne_,IV)                     !photoionization
     +       + x_(Ne_III) * n_e * colion(Ne_,III,T)           !col. ionization
     +       - x_(Ne_IV)  * n_e * colion(Ne_,IV,T)            !col. ionization
     +       + x_(Ne_V)   * x_(He_I) * nHtot * he_ct_r(Ne_,V,T)  !He CT "rec"
     +       - x_(Ne_IV)  * x_(He_I) * nHtot * he_ct_r(Ne_,IV,T) !He CT "rec" 
     +       + x_(Ne_III) * x_(He_II) * nHtot * he_ct_i(Ne_,III,T) !He CT "ionization"
     +       - x_(Ne_IV)  * x_(He_II) * nHtot * he_ct_i(Ne_,IV,T)  !He CT "ionization"

      dx_(Ne_V)  = 
     +         x_(Ne_VI) * n_e * allrec_mat(Ne_,VI)          !rad + 3bod + dr
     +       - x_(Ne_V)  * n_e * allrec_mat(Ne_,V)           !rad + 3bod + dr
     +       + x_(Ne_VI) * x_(H_I) * nHtot * ct_rec(Ne_,VI,T) !H CT "rec"
     +       - x_(Ne_V)  * x_(H_I) * nHtot * ct_rec(Ne_,V,T)  !H CT "rec" 
     +       + x_(Ne_IV) * xHII * nHtot * ct_ion(Ne_,IV,T)    !H CT "ionization"
     +       - x_(Ne_V)  * xHII * nHtot * ct_ion(Ne_,V,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Ne_V,Ne_,V)         !photoI+Auger
     +       - x_(Ne_V)  * Gamma(Ne_,V)                      !photoionization
     +       + x_(Ne_IV) * n_e * colion(Ne_,IV,T)            !col. ionization
     +       - x_(Ne_V)  * n_e * colion(Ne_,V,T)             !col. ionization
     +       + x_(Ne_VI) * x_(He_I) * nHtot * he_ct_r(Ne_,VI,T) !He CT "rec"
     +       - x_(Ne_V)  * x_(He_I) * nHtot * he_ct_r(Ne_,V,T)  !He CT "rec" 
     +       + x_(Ne_IV) * x_(He_II) * nHtot * he_ct_i(Ne_,IV,T)!He CT "ionization"
     +       - x_(Ne_V)  * x_(He_II) * nHtot * he_ct_i(Ne_,V,T) !He CT "ionization"

      dx_(Ne_VI)  =
     +         x_(Ne_VII) * n_e * allrec_mat(Ne_,VII)          !rad + 3bod + dr
     +       - x_(Ne_VI)  * n_e * allrec_mat(Ne_,VI)           !rad + 3bod + dr
     +       + x_(Ne_VII) * x_(H_I) * nHtot * ct_rec(Ne_,VII,T) !H CT "rec"
     +       - x_(Ne_VI)  * x_(H_I) * nHtot * ct_rec(Ne_,VI,T)  !H CT "rec" 
     +       + x_(Ne_V)   * xHII * nHtot * ct_ion(Ne_,V,T)      !H CT "ionization"
     +       - x_(Ne_VI)  * xHII * nHtot * ct_ion(Ne_,VI,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Ne_VI,Ne_,VI)         !photoI+Auger
     +       - x_(Ne_VI)  * Gamma(Ne_,VI)                      !photoionization
     +       + x_(Ne_V)   * n_e * colion(Ne_,V,T)              !col. ionization
     +       - x_(Ne_VI)  * n_e * colion(Ne_,VI,T)             !col. ionization
     +       + x_(Ne_VII) * x_(He_I) * nHtot * he_ct_r(Ne_,VII,T) !He CT "rec"
     +       - x_(Ne_VI)  * x_(He_I) * nHtot * he_ct_r(Ne_,VI,T)  !He CT "rec" 
     +       + x_(Ne_V)   * x_(He_II) * nHtot * he_ct_i(Ne_,V,T)  !He CT "ionization"
     +       - x_(Ne_VI)  * x_(He_II) * nHtot * he_ct_i(Ne_,VI,T) !He CT "ionization"

      dx_(Ne_VII) =
     +         x_(Ne_VIII) * n_e * allrec_mat(Ne_,VIII)        !rad + 3bod + dr
     +       - x_(Ne_VII)  * n_e * allrec_mat(Ne_,VII)         !rad + 3bod + dr
     +       + x_(Ne_VIII) * x_(H_I) * nHtot * ct_rec(Ne_,VIII,T)!H CT "rec"
     +       - x_(Ne_VII)  * x_(H_I) * nHtot * ct_rec(Ne_,VII,T) !H CT "rec" 
     +       + x_(Ne_VI)   * xHII * nHtot * ct_ion(Ne_,VI,T)    !H CT "ionization"
     +       - x_(Ne_VII)  * xHII * nHtot * ct_ion(Ne_,VII,T)   !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Ne_VII,Ne_,VII)         !photoI+Auger
     +       - x_(Ne_VII)  * Gamma(Ne_,VII)                    !photoionization
     +       + x_(Ne_VI)   * n_e * colion(Ne_,VI,T)            !col. ionization
     +       - x_(Ne_VII)  * n_e * colion(Ne_,VII,T)           !col. ionization
     +       + x_(Ne_VIII) * x_(He_I) * nHtot * he_ct_r(Ne_,VIII,T)!He CT "rec"
     +       - x_(Ne_VII)  * x_(He_I) * nHtot * he_ct_r(Ne_,VII,T) !He CT "rec" 
     +       + x_(Ne_VI)   * x_(He_II) * nHtot * he_ct_i(Ne_,VI,T) !He CT "ionization"
     +       - x_(Ne_VII)  * x_(He_II) * nHtot * he_ct_i(Ne_,VII,T)!He CT "ionization"

      dx_(Ne_VIII)=
     +         x_(Ne_IX)   * n_e * allrec_mat(Ne_,IX)          !rad + 3bod + dr
     +       - x_(Ne_VIII) * n_e * allrec_mat(Ne_,VIII)        !rad + 3bod + dr
     +       + x_(Ne_IX)   * x_(H_I) * nHtot * ct_rec(Ne_,IX,T)  !H CT "rec"
     +       - x_(Ne_VIII) * x_(H_I) * nHtot * ct_rec(Ne_,VIII,T)!H CT "rec" 
     +       + x_(Ne_VII)  * xHII * nHtot * ct_ion(Ne_,VII,T)   !H CT "ionization"
     +       - x_(Ne_VIII) * xHII * nHtot * ct_ion(Ne_,VIII,T)  !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Ne_VIII,Ne_,VIII)         !photoI+Auger
     +       - x_(Ne_VIII) * Gamma(Ne_,VIII)                   !photoionization
     +       + x_(Ne_VII)  * n_e * colion(Ne_,VII,T)           !col. ionization
     +       - x_(Ne_VIII) * n_e * colion(Ne_,VIII,T)          !col. ionization
     +       + x_(Ne_IX)   * x_(He_I) * nHtot * he_ct_r(Ne_,IX,T)  !He CT "rec"
     +       - x_(Ne_VIII) * x_(He_I) * nHtot * he_ct_r(Ne_,VIII,T)!He CT "rec" 
     +       + x_(Ne_VII)  * x_(He_II) * nHtot * he_ct_i(Ne_,VII,T) !He CT "ionization"
     +       - x_(Ne_VIII) * x_(He_II) * nHtot * he_ct_i(Ne_,VIII,T)!He CT "ionization"

      dx_(Ne_IX)   =
     +         x_(Ne_X)    * n_e * allrec_mat(Ne_,X)           !rad + 3bod + dr
     +       - x_(Ne_IX)   * n_e * allrec_mat(Ne_,IX)          !rad + 3bod + dr
     +       + x_(Ne_X)    * x_(H_I) * nHtot * ct_rec(Ne_,X,T)  !H CT "rec"
     +       - x_(Ne_IX)   * x_(H_I) * nHtot * ct_rec(Ne_,IX,T) !H CT "rec" 
     +       + x_(Ne_VIII) * xHII * nHtot * ct_ion(Ne_,VIII,T)  !H CT "ionization"
     +       - x_(Ne_IX)   * xHII * nHtot * ct_ion(Ne_,IX,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Ne_IX,Ne_,IX)         !photoI+Auger
     +       - x_(Ne_IX)   * Gamma(Ne_,IX)                     !photoionization
     +       + x_(Ne_VIII) * n_e * colion(Ne_,VIII,T)          !col. ionization
     +       - x_(Ne_IX)   * n_e * colion(Ne_,IX,T)            !col. ionization
     +       + x_(Ne_X)    * x_(He_I) * nHtot * he_ct_r(Ne_,X,T)  !He CT "rec"
     +       - x_(Ne_IX)   * x_(He_I) * nHtot * he_ct_r(Ne_,IX,T) !He CT "rec" 
     +       + x_(Ne_VIII) * x_(He_II) * nHtot * he_ct_i(Ne_,VIII,T)!He CT "ionization"
     +       - x_(Ne_IX)   * x_(He_II) * nHtot * he_ct_i(Ne_,IX,T)  !He CT "ionization"

      dx_(Ne_X) = 
     +         xNeXI     * n_e * allrec_mat(Ne_,XI)          !rad + 3bod + dr
     +       - x_(Ne_X)  * n_e * allrec_mat(Ne_,X)           !rad + 3bod + dr
     +       + xNeXI     * x_(H_I) * nHtot * ct_rec(Ne_,XI,T) !H CT "rec"
     +       - x_(Ne_X)  * x_(H_I) * nHtot * ct_rec(Ne_,X,T)  !H CT "rec" 
     +       + x_(Ne_IX) * xHII * nHtot * ct_ion(Ne_,IX,T)    !H CT "ionization"
     +       - x_(Ne_X)  * xHII * nHtot * ct_ion(Ne_,X,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Ne_X,Ne_,X)         !photoI+Auger
     +       - x_(Ne_X)  * Gamma(Ne_,X)                      !photoionization
     +       + x_(Ne_IX) * n_e * colion(Ne_,IX,T)            !col. ionization
     +       - x_(Ne_X)  * n_e * colion(Ne_,X,T)             !col. ionization
     +       + xNeXI     * x_(He_I) * nHtot * he_ct_r(Ne_,XI,T) !He CT "rec"
     +       - x_(Ne_X)  * x_(He_I) * nHtot * he_ct_r(Ne_,X,T)  !He CT "rec" 
     +       + x_(Ne_IX) * x_(He_II) * nHtot * he_ct_i(Ne_,IX,T)!He CT "ionization"
     +       - x_(Ne_X)  * x_(He_II) * nHtot * he_ct_i(Ne_,X,T) !He CT "ionization"


c================
c     Magnesium
c================

      dx_(Mg_I)   = 
     +         x_(Mg_II) * n_e * allrec_mat(Mg_,II)          !rad + 3bod + dr
     +       + x_(Mg_II) * x_(H_I) * nHtot * ct_rec(Mg_,II,T) !H CT "rec" 
     +       - x_(Mg_I)  * xHII * nHtot * ct_ion(Mg_,I,T)     !H CT "ionization"
     +       - x_(Mg_I)  * Gamma(Mg_,I)                      !photoionization
     +       - x_(Mg_I)  * n_e * colion(Mg_,I,T)             !col. ionization
     +       + x_(Mg_II) * x_(He_I) * nHtot * he_ct_r(Mg_,II,T) !He CT "rec" 
     +       - x_(Mg_I)  * x_(He_II) * nHtot * he_ct_i(Mg_,I,T) !He CT "ionization"

      dx_(Mg_II)  = 
     +         x_(Mg_III) * n_e * allrec_mat(Mg_,III)          !rad + 3bod + dr
     +       - x_(Mg_II)  * n_e * allrec_mat(Mg_,II)           !rad + 3bod + dr
     +       + x_(Mg_III) * x_(H_I) * nHtot * ct_rec(Mg_,III,T) !H CT "rec"
     +       - x_(Mg_II)  * x_(H_I) * nHtot * ct_rec(Mg_,II,T)  !H CT "rec"
     +       + x_(Mg_I)   * xHII * nHtot * ct_ion(Mg_,I,T)      !H CT "ionization"
     +       - x_(Mg_II)  * xHII * nHtot * ct_ion(Mg_,II,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Mg_II,Mg_,II)           !photoI+Auger
     +       - x_(Mg_II)  * Gamma(Mg_,II)                      !photoionization
     +       + x_(Mg_I)   * n_e * colion(Mg_,I,T)              !col. ionization
     +       - x_(Mg_II)  * n_e * colion(Mg_,II,T)             !col. ionization
     +       + x_(Mg_III) * x_(He_I) * nHtot * he_ct_r(Mg_,III,T) !He CT "rec"
     +       - x_(Mg_II)  * x_(He_I) * nHtot * he_ct_r(Mg_,II,T)  !He CT "rec"
     +       + x_(Mg_I)   * x_(He_II) * nHtot * he_ct_i(Mg_,I,T)  !He CT "ionization"
     +       - x_(Mg_II)  * x_(He_II) * nHtot * he_ct_i(Mg_,II,T) !He CT "ionization"

      dx_(Mg_III) = 
     +         x_(Mg_IV)  * n_e * allrec_mat(Mg_,IV)           !rad + 3bod + dr
     +       - x_(Mg_III) * n_e * allrec_mat(Mg_,III)          !rad + 3bod + dr
     +       + x_(Mg_IV)  * x_(H_I) * nHtot * ct_rec(Mg_,IV,T)  !H CT "rec"
     +       - x_(Mg_III) * x_(H_I) * nHtot * ct_rec(Mg_,III,T) !H CT "rec" 
     +       + x_(Mg_II)  * xHII * nHtot * ct_ion(Mg_,II,T)     !H CT "ionization"
     +       - x_(Mg_III) * xHII * nHtot * ct_ion(Mg_,III,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Mg_III,Mg_,III)           !photoI+Auger
     +       - x_(Mg_III) * Gamma(Mg_,III)                     !photoionization
     +       + x_(Mg_II)  * n_e * colion(Mg_,II,T)             !col. ionization
     +       - x_(Mg_III) * n_e * colion(Mg_,III,T)            !col. ionization
     +       + x_(Mg_IV)  * x_(He_I) * nHtot * he_ct_r(Mg_,IV,T)  !He CT "rec"
     +       - x_(Mg_III) * x_(He_I) * nHtot * he_ct_r(Mg_,III,T) !He CT "rec" 
     +       + x_(Mg_II)  * x_(He_II) * nHtot * he_ct_i(Mg_,II,T) !He CT "ionization"
     +       - x_(Mg_III) * x_(He_II) * nHtot * he_ct_i(Mg_,III,T)!He CT "ionization"

      dx_(Mg_IV)  = 
     +         x_(Mg_V)   * n_e * allrec_mat(Mg_,V)           !rad + 3bod + dr
     +       - x_(Mg_IV)  * n_e * allrec_mat(Mg_,IV)          !rad + 3bod + dr
     +       + x_(Mg_V)   * x_(H_I) * nHtot * ct_rec(Mg_,V,T)  !H CT "rec"
     +       - x_(Mg_IV)  * x_(H_I) * nHtot * ct_rec(Mg_,IV,T) !H CT "rec" 
     +       + x_(Mg_III) * xHII * nHtot * ct_ion(Mg_,III,T)   !H CT "ionization"
     +       - x_(Mg_IV)  * xHII * nHtot * ct_ion(Mg_,IV,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Mg_IV,Mg_,IV)           !photoI+Auger
     +       - x_(Mg_IV)  * Gamma(Mg_,IV)                     !photoionization
     +       + x_(Mg_III) * n_e * colion(Mg_,III,T)           !col. ionization
     +       - x_(Mg_IV)  * n_e * colion(Mg_,IV,T)            !col. ionization
     +       + x_(Mg_V)   * x_(He_I) * nHtot * he_ct_r(Mg_,V,T)  !He CT "rec"
     +       - x_(Mg_IV)  * x_(He_I) * nHtot * he_ct_r(Mg_,IV,T) !He CT "rec" 
     +       + x_(Mg_III) * x_(He_II) * nHtot * he_ct_i(Mg_,III,T)!He CT "ionization"
     +       - x_(Mg_IV)  * x_(He_II) * nHtot * he_ct_i(Mg_,IV,T) !He CT "ionization"

      dx_(Mg_V)  = 
     +         x_(Mg_VI) * n_e * allrec_mat(Mg_,VI)          !rad + 3bod + dr
     +       - x_(Mg_V)  * n_e * allrec_mat(Mg_,V)           !rad + 3bod + dr
     +       + x_(Mg_VI) * x_(H_I) * nHtot * ct_rec(Mg_,VI,T) !H CT "rec"
     +       - x_(Mg_V)  * x_(H_I) * nHtot * ct_rec(Mg_,V,T)  !H CT "rec" 
     +       + x_(Mg_IV) * xHII * nHtot * ct_ion(Mg_,IV,T)    !H CT "ionization"
     +       - x_(Mg_V)  * xHII * nHtot * ct_ion(Mg_,V,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Mg_V,Mg_,V)           !photoI+Auger
     +       - x_(Mg_V)  * Gamma(Mg_,V)                      !photoionization
     +       + x_(Mg_IV) * n_e * colion(Mg_,IV,T)            !col. ionization
     +       - x_(Mg_V)  * n_e * colion(Mg_,V,T)             !col. ionization
     +       + x_(Mg_VI) * x_(He_I) * nHtot * he_ct_r(Mg_,VI,T) !He CT "rec"
     +       - x_(Mg_V)  * x_(He_I) * nHtot * he_ct_r(Mg_,V,T)  !He CT "rec" 
     +       + x_(Mg_IV) * x_(He_II) * nHtot * he_ct_i(Mg_,IV,T)!He CT "ionization"
     +       - x_(Mg_V)  * x_(He_II) * nHtot * he_ct_i(Mg_,V,T) !He CT "ionization"

      dx_(Mg_VI)  =
     +         x_(Mg_VII) * n_e * allrec_mat(Mg_,VII)          !rad + 3bod + dr
     +       - x_(Mg_VI)  * n_e * allrec_mat(Mg_,VI)           !rad + 3bod + dr
     +       + x_(Mg_VII) * x_(H_I) * nHtot * ct_rec(Mg_,VII,T) !H CT "rec"
     +       - x_(Mg_VI)  * x_(H_I) * nHtot * ct_rec(Mg_,VI,T)  !H CT "rec" 
     +       + x_(Mg_V)   * xHII * nHtot * ct_ion(Mg_,V,T)      !H CT "ionization"
     +       - x_(Mg_VI)  * xHII * nHtot * ct_ion(Mg_,VI,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Mg_VI,Mg_,VI)           !photoI+Auger
     +       - x_(Mg_VI)  * Gamma(Mg_,VI)                      !photoionization
     +       + x_(Mg_V)   * n_e * colion(Mg_,V,T)              !col. ionization
     +       - x_(Mg_VI)  * n_e * colion(Mg_,VI,T)             !col. ionization
     +       + x_(Mg_VII) * x_(He_I) * nHtot * he_ct_r(Mg_,VII,T) !He CT "rec"
     +       - x_(Mg_VI)  * x_(He_I) * nHtot * he_ct_r(Mg_,VI,T)  !He CT "rec" 
     +       + x_(Mg_V)   * x_(He_II) * nHtot * he_ct_i(Mg_,V,T)  !He CT "ionization"
     +       - x_(Mg_VI)  * x_(He_II) * nHtot * he_ct_i(Mg_,VI,T) !He CT "ionization"

      dx_(Mg_VII) =
     +         x_(Mg_VIII) * n_e * allrec_mat(Mg_,VIII)        !rad + 3bod + dr
     +       - x_(Mg_VII)  * n_e * allrec_mat(Mg_,VII)         !rad + 3bod + dr
     +       + x_(Mg_VIII) * x_(H_I) * nHtot * ct_rec(Mg_,VIII,T)!H CT "rec"
     +       - x_(Mg_VII)  * x_(H_I) * nHtot * ct_rec(Mg_,VII,T) !H CT "rec" 
     +       + x_(Mg_VI)   * xHII * nHtot * ct_ion(Mg_,VI,T)    !H CT "ionization"
     +       - x_(Mg_VII)  * xHII * nHtot * ct_ion(Mg_,VII,T)   !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Mg_VII,Mg_,VII)           !photoI+Auger
     +       - x_(Mg_VII)  * Gamma(Mg_,VII)                    !photoionization
     +       + x_(Mg_VI)   * n_e * colion(Mg_,VI,T)            !col. ionization
     +       - x_(Mg_VII)  * n_e * colion(Mg_,VII,T)           !col. ionization
     +       + x_(Mg_VIII) * x_(He_I) * nHtot * he_ct_r(Mg_,VIII,T)!He CT "rec"
     +       - x_(Mg_VII)  * x_(He_I) * nHtot * he_ct_r(Mg_,VII,T) !He CT "rec" 
     +       + x_(Mg_VI)   * x_(He_II) * nHtot * he_ct_i(Mg_,VI,T) !He CT "ionization"
     +       - x_(Mg_VII)  * x_(He_II) * nHtot * he_ct_i(Mg_,VII,T)!He CT "ionization"

      dx_(Mg_VIII)=
     +         x_(Mg_IX)   * n_e * allrec_mat(Mg_,IX)          !rad + 3bod + dr
     +       - x_(Mg_VIII) * n_e * allrec_mat(Mg_,VIII)        !rad + 3bod + dr
     +       + x_(Mg_IX)   * x_(H_I) * nHtot * ct_rec(Mg_,IX,T)  !H CT "rec"
     +       - x_(Mg_VIII) * x_(H_I) * nHtot * ct_rec(Mg_,VIII,T)!H CT "rec" 
     +       + x_(Mg_VII)  * xHII * nHtot * ct_ion(Mg_,VII,T)   !H CT "ionization"
     +       - x_(Mg_VIII) * xHII * nHtot * ct_ion(Mg_,VIII,T)  !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Mg_VIII,Mg_,VIII)           !photoI+Auger
     +       - x_(Mg_VIII) * Gamma(Mg_,VIII)                   !photoionization
     +       + x_(Mg_VII)  * n_e * colion(Mg_,VII,T)           !col. ionization
     +       - x_(Mg_VIII) * n_e * colion(Mg_,VIII,T)          !col. ionization
     +       + x_(Mg_IX)   * x_(He_I) * nHtot * he_ct_r(Mg_,IX,T)  !He CT "rec"
     +       - x_(Mg_VIII) * x_(He_I) * nHtot * he_ct_r(Mg_,VIII,T)!He CT "rec" 
     +       + x_(Mg_VII)  * x_(He_II) * nHtot * he_ct_i(Mg_,VII,T) !He CT "ionization"
     +       - x_(Mg_VIII) * x_(He_II) * nHtot * he_ct_i(Mg_,VIII,T)!He CT "ionization"

      dx_(Mg_IX)   =
     +         x_(Mg_X)    * n_e * allrec_mat(Mg_,X)           !rad + 3bod + dr
     +       - x_(Mg_IX)   * n_e * allrec_mat(Mg_,IX)          !rad + 3bod + dr
     +       + x_(Mg_X)    * x_(H_I) * nHtot * ct_rec(Mg_,X,T)  !H CT "rec"
     +       - x_(Mg_IX)   * x_(H_I) * nHtot * ct_rec(Mg_,IX,T) !H CT "rec" 
     +       + x_(Mg_VIII) * xHII * nHtot * ct_ion(Mg_,VIII,T)  !H CT "ionization"
     +       - x_(Mg_IX)   * xHII * nHtot * ct_ion(Mg_,IX,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Mg_IX,Mg_,IX)           !photoI+Auger
     +       - x_(Mg_IX)   * Gamma(Mg_,IX)                     !photoionization
     +       + x_(Mg_VIII) * n_e * colion(Mg_,VIII,T)          !col. ionization
     +       - x_(Mg_IX)   * n_e * colion(Mg_,IX,T)            !col. ionization
     +       + x_(Mg_X)    * x_(He_I) * nHtot * he_ct_r(Mg_,X,T)  !He CT "rec"
     +       - x_(Mg_IX)   * x_(He_I) * nHtot * he_ct_r(Mg_,IX,T) !He CT "rec" 
     +       + x_(Mg_VIII) * x_(He_II) * nHtot * he_ct_i(Mg_,VIII,T)!He CT "ionization"
     +       - x_(Mg_IX)   * x_(He_II) * nHtot * he_ct_i(Mg_,IX,T)  !He CT "ionization"

      dx_(Mg_X)   = 
     +         x_(Mg_XI) * n_e * allrec_mat(Mg_,XI)          !rad + 3bod + dr
     +       - x_(Mg_X)  * n_e * allrec_mat(Mg_,X)           !rad + 3bod + dr
     +       + x_(Mg_XI) * x_(H_I) * nHtot * ct_rec(Mg_,XI,T) !H CT "rec"
     +       - x_(Mg_X)  * x_(H_I) * nHtot * ct_rec(Mg_,X,T)  !H CT "rec" 
     +       + x_(Mg_IX) * xHII * nHtot * ct_ion(Mg_,IX,T)    !H CT "ionization"
     +       - x_(Mg_X)  * xHII * nHtot * ct_ion(Mg_,X,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Mg_X,Mg_,X)           !photoI+Auger
     +       - x_(Mg_X)  * Gamma(Mg_,X)                      !photoionization
     +       + x_(Mg_IX) * n_e * colion(Mg_,IX,T)            !col. ionization
     +       - x_(Mg_X)  * n_e * colion(Mg_,X,T)             !col. ionization
     +       + x_(Mg_XI) * x_(He_I) * nHtot * he_ct_r(Mg_,XI,T) !He CT "rec"
     +       - x_(Mg_X)  * x_(He_I) * nHtot * he_ct_r(Mg_,X,T)  !He CT "rec" 
     +       + x_(Mg_IX) * x_(He_II) * nHtot * he_ct_i(Mg_,IX,T)!He CT "ionization"
     +       - x_(Mg_X)  * x_(He_II) * nHtot * he_ct_i(Mg_,X,T) !He CT "ionization"

      dx_(Mg_XI)  = 
     +         x_(Mg_XII) * n_e * allrec_mat(Mg_,XII)          !rad + 3bod + dr
     +       - x_(Mg_XI)  * n_e * allrec_mat(Mg_,XI)           !rad + 3bod + dr
     +       + x_(Mg_XII) * x_(H_I) * nHtot * ct_rec(Mg_,XII,T) !H CT "rec"
     +       - x_(Mg_XI)  * x_(H_I) * nHtot * ct_rec(Mg_,XI,T)  !H CT "rec" 
     +       + x_(Mg_X)   * xHII * nHtot * ct_ion(Mg_,X,T)      !H CT "ionization"
     +       - x_(Mg_XI)  * xHII * nHtot * ct_ion(Mg_,XI,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Mg_XI,Mg_,XI)           !photoI+Auger
     +       - x_(Mg_XI)  * Gamma(Mg_,XI)                      !photoionization
     +       + x_(Mg_X)   * n_e * colion(Mg_,X,T)              !col. ionization
     +       - x_(Mg_XI)  * n_e * colion(Mg_,XI,T)             !col. ionization
     +       + x_(Mg_XII) * x_(He_I) * nHtot * he_ct_r(Mg_,XII,T) !He CT "rec"
     +       - x_(Mg_XI)  * x_(He_I) * nHtot * he_ct_r(Mg_,XI,T)  !He CT "rec" 
     +       + x_(Mg_X)   * x_(He_II) * nHtot * he_ct_i(Mg_,X,T)  !He CT "ionization"
     +       - x_(Mg_XI)  * x_(He_II) * nHtot * he_ct_i(Mg_,XI,T) !He CT "ionization"

      dx_(Mg_XII) = 
     +         xMgXIII    * n_e * allrec_mat(Mg_,XIII)        !rad + 3bod + dr
     +       - x_(Mg_XII) * n_e * allrec_mat(Mg_,XII)         !rad + 3bod + dr
     +       + xMgXIII    * x_(H_I) * nHtot * ct_rec(Mg_,XIII,T) !H CT "rec"
     +       - x_(Mg_XII) * x_(H_I) * nHtot * ct_rec(Mg_,XII,T)  !H CT "rec" 
     +       + x_(Mg_XI)  * xHII * nHtot * ct_ion(Mg_,XI,T)    !H CT "ionization"
     +       - x_(Mg_XII) * xHII * nHtot * ct_ion(Mg_,XII,T)   !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Mg_XII,Mg_,XII)           !photoI+Auger
     +       - x_(Mg_XII) * Gamma(Mg_,XII)                    !photoionization
     +       + x_(Mg_XI)  * n_e * colion(Mg_,XI,T)            !col. ionization
     +       - x_(Mg_XII) * n_e * colion(Mg_,XII,T)           !col. ionization
     +       + xMgXIII    * x_(He_I) * nHtot * he_ct_r(Mg_,XIII,T) !He CT "rec"
     +       - x_(Mg_XII) * x_(He_I) * nHtot * he_ct_r(Mg_,XII,T)  !He CT "rec" 
     +       + x_(Mg_XI)  * x_(He_II) * nHtot * he_ct_i(Mg_,XI,T)  !He CT "ionization"
     +       - x_(Mg_XII) * x_(He_II) * nHtot * he_ct_i(Mg_,XII,T) !He CT "ionization"


c================
c     Silicon
c================

      dx_(Si_I)   = 
     +         x_(Si_II) * n_e * allrec_mat(Si_,II)          !rad + 3bod + dr
     +       + x_(Si_II) * x_(H_I) * nHtot * ct_rec(Si_,II,T) !H CT "rec" 
     +       - x_(Si_I)  * xHII * nHtot * ct_ion(Si_,I,T)     !H CT "ionization"
     +       - x_(Si_I)  * Gamma(Si_,I)                      !photoionization
     +       - x_(Si_I)  * n_e * colion(Si_,I,T)             !col. ionization
     +       + x_(Si_II) * x_(He_I) * nHtot * he_ct_r(Si_,II,T) !He CT "rec" 
     +       - x_(Si_I)  * x_(He_II) * nHtot * he_ct_i(Si_,I,T) !He CT "ionization"

      dx_(Si_II)  = 
     +         x_(Si_III) * n_e * allrec_mat(Si_,III)          !rad + 3bod + dr
     +       - x_(Si_II)  * n_e * allrec_mat(Si_,II)           !rad + 3bod + dr
     +       + x_(Si_III) * x_(H_I) * nHtot * ct_rec(Si_,III,T) !H CT "rec"
     +       - x_(Si_II)  * x_(H_I) * nHtot * ct_rec(Si_,II,T)  !H CT "rec"
     +       + x_(Si_I)   * xHII * nHtot * ct_ion(Si_,I,T)      !H CT "ionization"
     +       - x_(Si_II)  * xHII * nHtot * ct_ion(Si_,II,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Si_II,Si_,II)           !photoI+Auger
     +       - x_(Si_II)  * Gamma(Si_,II)                      !photoionization
     +       + x_(Si_I)   * n_e * colion(Si_,I,T)              !col. ionization
     +       - x_(Si_II)  * n_e * colion(Si_,II,T)             !col. ionization
     +       + x_(Si_III) * x_(He_I) * nHtot * he_ct_r(Si_,III,T) !He CT "rec"
     +       - x_(Si_II)  * x_(He_I) * nHtot * he_ct_r(Si_,II,T)  !He CT "rec"
     +       + x_(Si_I)   * x_(He_II) * nHtot * he_ct_i(Si_,I,T)  !He CT "ionization"
     +       - x_(Si_II)  * x_(He_II) * nHtot * he_ct_i(Si_,II,T) !He CT "ionization"

      dx_(Si_III) = 
     +         x_(Si_IV)  * n_e * allrec_mat(Si_,IV)           !rad + 3bod + dr
     +       - x_(Si_III) * n_e * allrec_mat(Si_,III)          !rad + 3bod + dr
     +       + x_(Si_IV)  * x_(H_I) * nHtot * ct_rec(Si_,IV,T)  !H CT "rec"
     +       - x_(Si_III) * x_(H_I) * nHtot * ct_rec(Si_,III,T) !H CT "rec" 
     +       + x_(Si_II)  * xHII * nHtot * ct_ion(Si_,II,T)     !H CT "ionization"
     +       - x_(Si_III) * xHII * nHtot * ct_ion(Si_,III,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Si_III,Si_,III)           !photoI+Auger
     +       - x_(Si_III) * Gamma(Si_,III)                     !photoionization
     +       + x_(Si_II)  * n_e * colion(Si_,II,T)             !col. ionization
     +       - x_(Si_III) * n_e * colion(Si_,III,T)            !col. ionization
     +       + x_(Si_IV)  * x_(He_I) * nHtot * he_ct_r(Si_,IV,T)  !He CT "rec"
     +       - x_(Si_III) * x_(He_I) * nHtot * he_ct_r(Si_,III,T) !He CT "rec" 
     +       + x_(Si_II)  * x_(He_II) * nHtot * he_ct_i(Si_,II,T) !He CT "ionization"
     +       - x_(Si_III) * x_(He_II) * nHtot * he_ct_i(Si_,III,T)!He CT "ionization"

      dx_(Si_IV)  = 
     +         x_(Si_V)   * n_e * allrec_mat(Si_,V)           !rad + 3bod + dr
     +       - x_(Si_IV)  * n_e * allrec_mat(Si_,IV)          !rad + 3bod + dr
     +       + x_(Si_V)   * x_(H_I) * nHtot * ct_rec(Si_,V,T)  !H CT "rec"
     +       - x_(Si_IV)  * x_(H_I) * nHtot * ct_rec(Si_,IV,T) !H CT "rec" 
     +       + x_(Si_III) * xHII * nHtot * ct_ion(Si_,III,T)   !H CT "ionization"
     +       - x_(Si_IV)  * xHII * nHtot * ct_ion(Si_,IV,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Si_IV,Si_,IV)           !photoI+Auger
     +       - x_(Si_IV)  * Gamma(Si_,IV)                     !photoionization
     +       + x_(Si_III) * n_e * colion(Si_,III,T)           !col. ionization
     +       - x_(Si_IV)  * n_e * colion(Si_,IV,T)            !col. ionization
     +       + x_(Si_V)   * x_(He_I) * nHtot * he_ct_r(Si_,V,T)  !He CT "rec"
     +       - x_(Si_IV)  * x_(He_I) * nHtot * he_ct_r(Si_,IV,T) !He CT "rec" 
     +       + x_(Si_III) * x_(He_II) * nHtot * he_ct_i(Si_,III,T)!He CT "ionization"
     +       - x_(Si_IV)  * x_(He_II) * nHtot * he_ct_i(Si_,IV,T) !He CT "ionization"

      dx_(Si_V)  = 
     +         x_(Si_VI) * n_e * allrec_mat(Si_,VI)          !rad + 3bod + dr
     +       - x_(Si_V)  * n_e * allrec_mat(Si_,V)           !rad + 3bod + dr
     +       + x_(Si_VI) * x_(H_I) * nHtot * ct_rec(Si_,VI,T) !H CT "rec"
     +       - x_(Si_V)  * x_(H_I) * nHtot * ct_rec(Si_,V,T)  !H CT "rec" 
     +       + x_(Si_IV) * xHII * nHtot * ct_ion(Si_,IV,T)    !H CT "ionization"
     +       - x_(Si_V)  * xHII * nHtot * ct_ion(Si_,V,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Si_V,Si_,V)           !photoI+Auger
     +       - x_(Si_V)  * Gamma(Si_,V)                      !photoionization
     +       + x_(Si_IV) * n_e * colion(Si_,IV,T)            !col. ionization
     +       - x_(Si_V)  * n_e * colion(Si_,V,T)             !col. ionization
     +       + x_(Si_VI) * x_(He_I) * nHtot * he_ct_r(Si_,VI,T) !He CT "rec"
     +       - x_(Si_V)  * x_(He_I) * nHtot * he_ct_r(Si_,V,T)  !He CT "rec" 
     +       + x_(Si_IV) * x_(He_II) * nHtot * he_ct_i(Si_,IV,T)!He CT "ionization"
     +       - x_(Si_V)  * x_(He_II) * nHtot * he_ct_i(Si_,V,T) !He CT "ionization"

      dx_(Si_VI)  =
     +         x_(Si_VII) * n_e * allrec_mat(Si_,VII)          !rad + 3bod + dr
     +       - x_(Si_VI)  * n_e * allrec_mat(Si_,VI)           !rad + 3bod + dr
     +       + x_(Si_VII) * x_(H_I) * nHtot * ct_rec(Si_,VII,T) !H CT "rec"
     +       - x_(Si_VI)  * x_(H_I) * nHtot * ct_rec(Si_,VI,T)  !H CT "rec" 
     +       + x_(Si_V)   * xHII * nHtot * ct_ion(Si_,V,T)      !H CT "ionization"
     +       - x_(Si_VI)  * xHII * nHtot * ct_ion(Si_,VI,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Si_VI,Si_,VI)           !photoI+Auger
     +       - x_(Si_VI)  * Gamma(Si_,VI)                      !photoionization
     +       + x_(Si_V)   * n_e * colion(Si_,V,T)              !col. ionization
     +       - x_(Si_VI)  * n_e * colion(Si_,VI,T)             !col. ionization
     +       + x_(Si_VII) * x_(He_I) * nHtot * he_ct_r(Si_,VII,T) !He CT "rec"
     +       - x_(Si_VI)  * x_(He_I) * nHtot * he_ct_r(Si_,VI,T)  !He CT "rec" 
     +       + x_(Si_V)   * x_(He_II) * nHtot * he_ct_i(Si_,V,T)  !He CT "ionization"
     +       - x_(Si_VI)  * x_(He_II) * nHtot * he_ct_i(Si_,VI,T) !He CT "ionization"

      dx_(Si_VII) =
     +         x_(Si_VIII) * n_e * allrec_mat(Si_,VIII)        !rad + 3bod + dr
     +       - x_(Si_VII)  * n_e * allrec_mat(Si_,VII)         !rad + 3bod + dr
     +       + x_(Si_VIII) * x_(H_I) * nHtot * ct_rec(Si_,VIII,T)!H CT "rec"
     +       - x_(Si_VII)  * x_(H_I) * nHtot * ct_rec(Si_,VII,T) !H CT "rec" 
     +       + x_(Si_VI)   * xHII * nHtot * ct_ion(Si_,VI,T)    !H CT "ionization"
     +       - x_(Si_VII)  * xHII * nHtot * ct_ion(Si_,VII,T)   !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Si_VII,Si_,VII)           !photoI+Auger
     +       - x_(Si_VII)  * Gamma(Si_,VII)                    !photoionization
     +       + x_(Si_VI)   * n_e * colion(Si_,VI,T)            !col. ionization
     +       - x_(Si_VII)  * n_e * colion(Si_,VII,T)           !col. ionization
     +       + x_(Si_VIII) * x_(He_I) * nHtot * he_ct_r(Si_,VIII,T)!He CT "rec"
     +       - x_(Si_VII)  * x_(He_I) * nHtot * he_ct_r(Si_,VII,T) !He CT "rec" 
     +       + x_(Si_VI)   * x_(He_II) * nHtot * he_ct_i(Si_,VI,T) !He CT "ionization"
     +       - x_(Si_VII)  * x_(He_II) * nHtot * he_ct_i(Si_,VII,T)!He CT "ionization"

      dx_(Si_VIII)=
     +         x_(Si_IX)   * n_e * allrec_mat(Si_,IX)          !rad + 3bod + dr
     +       - x_(Si_VIII) * n_e * allrec_mat(Si_,VIII)        !rad + 3bod + dr
     +       + x_(Si_IX)   * x_(H_I) * nHtot * ct_rec(Si_,IX,T)  !H CT "rec"
     +       - x_(Si_VIII) * x_(H_I) * nHtot * ct_rec(Si_,VIII,T)!H CT "rec" 
     +       + x_(Si_VII)  * xHII * nHtot * ct_ion(Si_,VII,T)   !H CT "ionization"
     +       - x_(Si_VIII) * xHII * nHtot * ct_ion(Si_,VIII,T)  !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Si_VIII,Si_,VIII)           !photoI+Auger
     +       - x_(Si_VIII) * Gamma(Si_,VIII)                   !photoionization
     +       + x_(Si_VII)  * n_e * colion(Si_,VII,T)           !col. ionization
     +       - x_(Si_VIII) * n_e * colion(Si_,VIII,T)          !col. ionization
     +       + x_(Si_IX)   * x_(He_I) * nHtot * he_ct_r(Si_,IX,T)  !He CT "rec"
     +       - x_(Si_VIII) * x_(He_I) * nHtot * he_ct_r(Si_,VIII,T)!He CT "rec" 
     +       + x_(Si_VII)  * x_(He_II) * nHtot * he_ct_i(Si_,VII,T) !He CT "ionization"
     +       - x_(Si_VIII) * x_(He_II) * nHtot * he_ct_i(Si_,VIII,T)!He CT "ionization"

      dx_(Si_IX)   =
     +         x_(Si_X)    * n_e * allrec_mat(Si_,X)           !rad + 3bod + dr
     +       - x_(Si_IX)   * n_e * allrec_mat(Si_,IX)          !rad + 3bod + dr
     +       + x_(Si_X)    * x_(H_I) * nHtot * ct_rec(Si_,X,T)  !H CT "rec"
     +       - x_(Si_IX)   * x_(H_I) * nHtot * ct_rec(Si_,IX,T) !H CT "rec" 
     +       + x_(Si_VIII) * xHII * nHtot * ct_ion(Si_,VIII,T)  !H CT "ionization"
     +       - x_(Si_IX)   * xHII * nHtot * ct_ion(Si_,IX,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Si_IX,Si_,IX)           !photoI+Auger
     +       - x_(Si_IX)   * Gamma(Si_,IX)                     !photoionization
     +       + x_(Si_VIII) * n_e * colion(Si_,VIII,T)          !col. ionization
     +       - x_(Si_IX)   * n_e * colion(Si_,IX,T)            !col. ionization
     +       + x_(Si_X)    * x_(He_I) * nHtot * he_ct_r(Si_,X,T)  !He CT "rec"
     +       - x_(Si_IX)   * x_(He_I) * nHtot * he_ct_r(Si_,IX,T) !He CT "rec" 
     +       + x_(Si_VIII) * x_(He_II) * nHtot * he_ct_i(Si_,VIII,T)!He CT "ionization"
     +       - x_(Si_IX)   * x_(He_II) * nHtot * he_ct_i(Si_,IX,T)  !He CT "ionization"

      dx_(Si_X)   = 
     +         x_(Si_XI) * n_e * allrec_mat(Si_,XI)          !rad + 3bod + dr
     +       - x_(Si_X)  * n_e * allrec_mat(Si_,X)           !rad + 3bod + dr
     +       + x_(Si_XI) * x_(H_I) * nHtot * ct_rec(Si_,XI,T) !H CT "rec"
     +       - x_(Si_X)  * x_(H_I) * nHtot * ct_rec(Si_,X,T)  !H CT "rec" 
     +       + x_(Si_IX) * xHII * nHtot * ct_ion(Si_,IX,T)    !H CT "ionization"
     +       - x_(Si_X)  * xHII * nHtot * ct_ion(Si_,X,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Si_X,Si_,X)           !photoI+Auger
     +       - x_(Si_X)  * Gamma(Si_,X)                      !photoionization
     +       + x_(Si_IX) * n_e * colion(Si_,IX,T)            !col. ionization
     +       - x_(Si_X)  * n_e * colion(Si_,X,T)             !col. ionization
     +       + x_(Si_XI) * x_(He_I) * nHtot * he_ct_r(Si_,XI,T) !He CT "rec"
     +       - x_(Si_X)  * x_(He_I) * nHtot * he_ct_r(Si_,X,T)  !He CT "rec" 
     +       + x_(Si_IX) * x_(He_II) * nHtot * he_ct_i(Si_,IX,T)!He CT "ionization"
     +       - x_(Si_X)  * x_(He_II) * nHtot * he_ct_i(Si_,X,T) !He CT "ionization"

      dx_(Si_XI)  = 
     +         x_(Si_XII) * n_e * allrec_mat(Si_,XII)          !rad + 3bod + dr
     +       - x_(Si_XI)  * n_e * allrec_mat(Si_,XI)           !rad + 3bod + dr
     +       + x_(Si_XII) * x_(H_I) * nHtot * ct_rec(Si_,XII,T) !H CT "rec"
     +       - x_(Si_XI)  * x_(H_I) * nHtot * ct_rec(Si_,XI,T)  !H CT "rec" 
     +       + x_(Si_X)   * xHII * nHtot * ct_ion(Si_,X,T)      !H CT "ionization"
     +       - x_(Si_XI)  * xHII * nHtot * ct_ion(Si_,XI,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Si_XI,Si_,XI)           !photoI+Auger
     +       - x_(Si_XI)  * Gamma(Si_,XI)                      !photoionization
     +       + x_(Si_X)   * n_e * colion(Si_,X,T)              !col. ionization
     +       - x_(Si_XI)  * n_e * colion(Si_,XI,T)             !col. ionization
     +       + x_(Si_XII) * x_(He_I) * nHtot * he_ct_r(Si_,XII,T) !He CT "rec"
     +       - x_(Si_XI)  * x_(He_I) * nHtot * he_ct_r(Si_,XI,T)  !He CT "rec" 
     +       + x_(Si_X)   * x_(He_II) * nHtot * he_ct_i(Si_,X,T)  !He CT "ionization"
     +       - x_(Si_XI)  * x_(He_II) * nHtot * he_ct_i(Si_,XI,T) !He CT "ionization"

      dx_(Si_XII) = 
     +         x_(Si_XIII) * n_e * allrec_mat(Si_,XIII)        !rad + 3bod + dr
     +       - x_(Si_XII)  * n_e * allrec_mat(Si_,XII)         !rad + 3bod + dr
     +       + x_(Si_XIII) * x_(H_I) * nHtot * ct_rec(Si_,XIII,T) !H CT "rec"
     +       - x_(Si_XII)  * x_(H_I) * nHtot * ct_rec(Si_,XII,T)  !H CT "rec" 
     +       + x_(Si_XI)   * xHII * nHtot * ct_ion(Si_,XI,T)    !H CT "ionization"
     +       - x_(Si_XII)  * xHII * nHtot * ct_ion(Si_,XII,T)   !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Si_XII,Si_,XII)           !photoI+Auger
     +       - x_(Si_XII)  * Gamma(Si_,XII)                    !photoionization
     +       + x_(Si_XI)   * n_e * colion(Si_,XI,T)            !col. ionization
     +       - x_(Si_XII)  * n_e * colion(Si_,XII,T)           !col. ionization
     +       + x_(Si_XIII) * x_(He_I) * nHtot * he_ct_r(Si_,XIII,T) !He CT "rec"
     +       - x_(Si_XII)  * x_(He_I) * nHtot * he_ct_r(Si_,XII,T)  !He CT "rec" 
     +       + x_(Si_XI)   * x_(He_II) * nHtot * he_ct_i(Si_,XI,T)  !He CT "ionization"
     +       - x_(Si_XII)  * x_(He_II) * nHtot * he_ct_i(Si_,XII,T) !He CT "ionization"

      dx_(Si_XIII) =
     +         x_(Si_XIV)  * n_e * allrec_mat(Si_,XIV)         !rad + 3bod + dr
     +       - x_(Si_XIII) * n_e * allrec_mat(Si_,XIII)        !rad + 3bod + dr
     +       + x_(Si_XIV)  * x_(H_I) * nHtot * ct_rec(Si_,XIV,T) !H CT "rec"
     +       - x_(Si_XIII) * x_(H_I) * nHtot * ct_rec(Si_,XIII,T)!H CT "rec" 
     +       + x_(Si_XII)  * xHII * nHtot * ct_ion(Si_,XII,T)   !H CT "ionization"
     +       - x_(Si_XIII) * xHII * nHtot * ct_ion(Si_,XIII,T)  !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Si_XIII,Si_,XIII)           !photoI+Auger
     +       - x_(Si_XIII) * Gamma(Si_,XIII)                   !photoionization
     +       + x_(Si_XII)  * n_e * colion(Si_,XII,T)           !col. ionization
     +       - x_(Si_XIII) * n_e * colion(Si_,XIII,T)          !col. ionization
     +       + x_(Si_XIV)  * x_(He_I) * nHtot * he_ct_r(Si_,XIV,T) !He CT "rec"
     +       - x_(Si_XIII) * x_(He_I) * nHtot * he_ct_r(Si_,XIII,T)!He CT "rec" 
     +       + x_(Si_XII)  * x_(He_II) * nHtot * he_ct_i(Si_,XII,T)   !He CT "ionization"
     +       - x_(Si_XIII) * x_(He_II) * nHtot * he_ct_i(Si_,XIII,T)  !He CT "ionization"

      dx_(Si_XIV) = 
     +         xSiXV   * n_e * allrec_mat(Si_,XV)              !rad + 3bod + dr
     +       - x_(Si_XIV)  * n_e * allrec_mat(Si_,XIV)         !rad + 3bod + dr
     +       + xSiXV   * x_(H_I) * nHtot * ct_rec(Si_,XV,T)     !H CT "rec"
     +       - x_(Si_XIV)  * x_(H_I) * nHtot * ct_rec(Si_,XIV,T)!H CT "rec" 
     +       + x_(Si_XIII) * xHII * nHtot * ct_ion(Si_,XIII,T)  !H CT "ionization"
     +       - x_(Si_XIV)  * xHII * nHtot * ct_ion(Si_,XIV,T)   !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Si_XIV,Si_,XIV)           !photoI+Auger
     +       - x_(Si_XIV)  * Gamma(Si_,XIV)                    !photoionization
     +       + x_(Si_XIII) * n_e * colion(Si_,XIII,T)          !col. ionization
     +       - x_(Si_XIV)  * n_e * colion(Si_,XIV,T)           !col. ionization
     +       + xSiXV   * x_(He_I) * nHtot * he_ct_r(Si_,XV,T)     !He CT "rec"
     +       - x_(Si_XIV)  * x_(He_I) * nHtot * he_ct_r(Si_,XIV,T)!He CT "rec" 
     +       + x_(Si_XIII) * x_(He_II) * nHtot * he_ct_i(Si_,XIII,T)!He CT "ionization"
     +       - x_(Si_XIV)  * x_(He_II) * nHtot * he_ct_i(Si_,XIV,T) !He CT "ionization"

c================
c     Sulfur
c================

      dx_(S_I)   =
     +         x_(S_II) * n_e * allrec_mat(S_,II)          !rad + 3bod + dr
     +       + x_(S_II) * x_(H_I) * nHtot * ct_rec(S_,II,T) !H CT "rec" 
     +       - x_(S_I)  * xHII * nHtot * ct_ion(S_,I,T)     !H CT "ionization"
     +       - x_(S_I)  * Gamma(S_,I)                      !photoionization
     +       - x_(S_I)  * n_e * colion(S_,I,T)             !col. ionization
     +       + x_(S_II) * x_(He_I) * nHtot * he_ct_r(S_,II,T) !He CT "rec" 
     +       - x_(S_I)  * x_(He_II) * nHtot * he_ct_i(S_,I,T) !He CT "ionization"

      dx_(S_II)  =
     +         x_(S_III) * n_e * allrec_mat(S_,III)          !rad + 3bod + dr
     +       - x_(S_II)  * n_e * allrec_mat(S_,II)           !rad + 3bod + dr
     +       + x_(S_III) * x_(H_I) * nHtot * ct_rec(S_,III,T) !H CT "rec"
     +       - x_(S_II)  * x_(H_I) * nHtot * ct_rec(S_,II,T)  !H CT "rec"
     +       + x_(S_I)   * xHII * nHtot * ct_ion(S_,I,T)      !H CT "ionization"
     +       - x_(S_II)  * xHII * nHtot * ct_ion(S_,II,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, S_II,S_,II)           !photoI+Auger
     +       - x_(S_II)  * Gamma(S_,II)                      !photoionization
     +       + x_(S_I)   * n_e * colion(S_,I,T)              !col. ionization
     +       - x_(S_II)  * n_e * colion(S_,II,T)             !col. ionization
     +       + x_(S_III) * x_(He_I) * nHtot * he_ct_r(S_,III,T) !He CT "rec"
     +       - x_(S_II)  * x_(He_I) * nHtot * he_ct_r(S_,II,T)  !He CT "rec"
     +       + x_(S_I)   * x_(He_II) * nHtot * he_ct_i(S_,I,T)  !He CT "ionization"
     +       - x_(S_II)  * x_(He_II) * nHtot * he_ct_i(S_,II,T) !He CT "ionization"

      dx_(S_III) =
     +         x_(S_IV)  * n_e * allrec_mat(S_,IV)           !rad + 3bod + dr
     +       - x_(S_III) * n_e * allrec_mat(S_,III)          !rad + 3bod + dr
     +       + x_(S_IV)  * x_(H_I) * nHtot * ct_rec(S_,IV,T)  !H CT "rec"
     +       - x_(S_III) * x_(H_I) * nHtot * ct_rec(S_,III,T) !H CT "rec" 
     +       + x_(S_II)  * xHII * nHtot * ct_ion(S_,II,T)     !H CT "ionization"
     +       - x_(S_III) * xHII * nHtot * ct_ion(S_,III,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, S_III,S_,III)           !photoI+Auger
     +       - x_(S_III) * Gamma(S_,III)                     !photoionization
     +       + x_(S_II)  * n_e * colion(S_,II,T)             !col. ionization
     +       - x_(S_III) * n_e * colion(S_,III,T)            !col. ionization
     +       + x_(S_IV)  * x_(He_I) * nHtot * he_ct_r(S_,IV,T)  !He CT "rec"
     +       - x_(S_III) * x_(He_I) * nHtot * he_ct_r(S_,III,T) !He CT "rec" 
     +       + x_(S_II)  * x_(He_II) * nHtot * he_ct_i(S_,II,T) !He CT "ionization"
     +       - x_(S_III) * x_(He_II) * nHtot * he_ct_i(S_,III,T)!He CT "ionization"

      dx_(S_IV)  =
     +         x_(S_V)   * n_e * allrec_mat(S_,V)           !rad + 3bod + dr
     +       - x_(S_IV)  * n_e * allrec_mat(S_,IV)          !rad + 3bod + dr
     +       + x_(S_V)   * x_(H_I) * nHtot * ct_rec(S_,V,T)  !H CT "rec"
     +       - x_(S_IV)  * x_(H_I) * nHtot * ct_rec(S_,IV,T) !H CT "rec" 
     +       + x_(S_III) * xHII * nHtot * ct_ion(S_,III,T)   !H CT "ionization"
     +       - x_(S_IV)  * xHII * nHtot * ct_ion(S_,IV,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, S_IV,S_,IV)           !photoI+Auger
     +       - x_(S_IV)  * Gamma(S_,IV)                     !photoionization
     +       + x_(S_III) * n_e * colion(S_,III,T)           !col. ionization
     +       - x_(S_IV)  * n_e * colion(S_,IV,T)            !col. ionization
     +       + x_(S_V)   * x_(He_I) * nHtot * he_ct_r(S_,V,T)  !He CT "rec"
     +       - x_(S_IV)  * x_(He_I) * nHtot * he_ct_r(S_,IV,T) !He CT "rec" 
     +       + x_(S_III) * x_(He_II) * nHtot * he_ct_i(S_,III,T)!He CT "ionization"
     +       - x_(S_IV)  * x_(He_II) * nHtot * he_ct_i(S_,IV,T) !He CT "ionization"

      dx_(S_V)  = 
     +         x_(S_VI) * n_e * allrec_mat(S_,VI)          !rad + 3bod + dr
     +       - x_(S_V)  * n_e * allrec_mat(S_,V)           !rad + 3bod + dr
     +       + x_(S_VI) * x_(H_I) * nHtot * ct_rec(S_,VI,T) !H CT "rec"
     +       - x_(S_V)  * x_(H_I) * nHtot * ct_rec(S_,V,T)  !H CT "rec" 
     +       + x_(S_IV) * xHII * nHtot * ct_ion(S_,IV,T)    !H CT "ionization"
     +       - x_(S_V)  * xHII * nHtot * ct_ion(S_,V,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, S_V,S_,V)           !photoI+Auger
     +       - x_(S_V)  * Gamma(S_,V)                      !photoionization
     +       + x_(S_IV) * n_e * colion(S_,IV,T)            !col. ionization
     +       - x_(S_V)  * n_e * colion(S_,V,T)             !col. ionization
     +       + x_(S_VI) * x_(He_I) * nHtot * he_ct_r(S_,VI,T) !He CT "rec"
     +       - x_(S_V)  * x_(He_I) * nHtot * he_ct_r(S_,V,T)  !He CT "rec" 
     +       + x_(S_IV) * x_(He_II) * nHtot * he_ct_i(S_,IV,T)!He CT "ionization"
     +       - x_(S_V)  * x_(He_II) * nHtot * he_ct_i(S_,V,T) !He CT "ionization"

      dx_(S_VI)  =
     +         x_(S_VII) * n_e * allrec_mat(S_,VII)          !rad + 3bod + dr
     +       - x_(S_VI)  * n_e * allrec_mat(S_,VI)           !rad + 3bod + dr
     +       + x_(S_VII) * x_(H_I) * nHtot * ct_rec(S_,VII,T) !H CT "rec"
     +       - x_(S_VI)  * x_(H_I) * nHtot * ct_rec(S_,VI,T)  !H CT "rec" 
     +       + x_(S_V)   * xHII * nHtot * ct_ion(S_,V,T)      !H CT "ionization"
     +       - x_(S_VI)  * xHII * nHtot * ct_ion(S_,VI,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, S_VI,S_,VI)           !photoI+Auger
     +       - x_(S_VI)  * Gamma(S_,VI)                      !photoionization
     +       + x_(S_V)   * n_e * colion(S_,V,T)              !col. ionization
     +       - x_(S_VI)  * n_e * colion(S_,VI,T)             !col. ionization
     +       + x_(S_VII) * x_(He_I) * nHtot * he_ct_r(S_,VII,T) !He CT "rec"
     +       - x_(S_VI)  * x_(He_I) * nHtot * he_ct_r(S_,VI,T)  !He CT "rec" 
     +       + x_(S_V)   * x_(He_II) * nHtot * he_ct_i(S_,V,T)  !He CT "ionization"
     +       - x_(S_VI)  * x_(He_II) * nHtot * he_ct_i(S_,VI,T) !He CT "ionization"

      dx_(S_VII) =
     +         x_(S_VIII) * n_e * allrec_mat(S_,VIII)          !rad + 3bod + dr
     +       - x_(S_VII)  * n_e * allrec_mat(S_,VII)           !rad + 3bod + dr
     +       + x_(S_VIII) * x_(H_I) * nHtot * ct_rec(S_,VIII,T) !H CT "rec"
     +       - x_(S_VII)  * x_(H_I) * nHtot * ct_rec(S_,VII,T)  !H CT "rec" 
     +       + x_(S_VI)   * xHII * nHtot * ct_ion(S_,VI,T)      !H CT "ionization"
     +       - x_(S_VII)  * xHII * nHtot * ct_ion(S_,VII,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, S_VII,S_,VII)           !photoI+Auger
     +       - x_(S_VII)  * Gamma(S_,VII)                      !photoionization
     +       + x_(S_VI)   * n_e * colion(S_,VI,T)              !col. ionization
     +       - x_(S_VII)  * n_e * colion(S_,VII,T)             !col. ionization
     +       + x_(S_VIII) * x_(He_I) * nHtot * he_ct_r(S_,VIII,T) !He CT "rec"
     +       - x_(S_VII)  * x_(He_I) * nHtot * he_ct_r(S_,VII,T)  !He CT "rec" 
     +       + x_(S_VI)   * x_(He_II) * nHtot * he_ct_i(S_,VI,T)  !He CT "ionization"
     +       - x_(S_VII)  * x_(He_II) * nHtot * he_ct_i(S_,VII,T) !He CT "ionization"

      dx_(S_VIII)=
     +         x_(S_IX)   * n_e * allrec_mat(S_,IX)            !rad + 3bod + dr
     +       - x_(S_VIII) * n_e * allrec_mat(S_,VIII)          !rad + 3bod + dr
     +       + x_(S_IX)   * x_(H_I) * nHtot * ct_rec(S_,IX,T)   !H CT "rec"
     +       - x_(S_VIII) * x_(H_I) * nHtot * ct_rec(S_,VIII,T) !H CT "rec" 
     +       + x_(S_VII)  * xHII * nHtot * ct_ion(S_,VII,T)     !H CT "ionization"
     +       - x_(S_VIII) * xHII * nHtot * ct_ion(S_,VIII,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, S_VIII,S_,VIII)           !photoI+Auger
     +       - x_(S_VIII) * Gamma(S_,VIII)                     !photoionization
     +       + x_(S_VII)  * n_e * colion(S_,VII,T)             !col. ionization
     +       - x_(S_VIII) * n_e * colion(S_,VIII,T)            !col. ionization
     +       + x_(S_IX)   * x_(He_I) * nHtot * he_ct_r(S_,IX,T)   !He CT "rec"
     +       - x_(S_VIII) * x_(He_I) * nHtot * he_ct_r(S_,VIII,T) !He CT "rec" 
     +       + x_(S_VII)  * x_(He_II) * nHtot * he_ct_i(S_,VII,T) !He CT "ionization"
     +       - x_(S_VIII) * x_(He_II) * nHtot * he_ct_i(S_,VIII,T)!He CT "ionization"

      dx_(S_IX)   =
     +         x_(S_X)    * n_e * allrec_mat(S_,X)           !rad + 3bod + dr
     +       - x_(S_IX)   * n_e * allrec_mat(S_,IX)          !rad + 3bod + dr
     +       + x_(S_X)    * x_(H_I) * nHtot * ct_rec(S_,X,T)  !H CT "rec"
     +       - x_(S_IX)   * x_(H_I) * nHtot * ct_rec(S_,IX,T) !H CT "rec" 
     +       + x_(S_VIII) * xHII * nHtot * ct_ion(S_,VIII,T)  !H CT "ionization"
     +       - x_(S_IX)   * xHII * nHtot * ct_ion(S_,IX,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, S_IX,S_,IX)           !photoI+Auger
     +       - x_(S_IX)   * Gamma(S_,IX)                     !photoionization
     +       + x_(S_VIII) * n_e * colion(S_,VIII,T)          !col. ionization
     +       - x_(S_IX)   * n_e * colion(S_,IX,T)            !col. ionization
     +       + x_(S_X)    * x_(He_I) * nHtot * he_ct_r(S_,X,T)  !He CT "rec"
     +       - x_(S_IX)   * x_(He_I) * nHtot * he_ct_r(S_,IX,T) !He CT "rec" 
     +       + x_(S_VIII) * x_(He_II) * nHtot * he_ct_i(S_,VIII,T)!He CT "ionization"
     +       - x_(S_IX)   * x_(He_II) * nHtot * he_ct_i(S_,IX,T)  !He CT "ionization"

      dx_(S_X)   = 
     +         x_(S_XI) * n_e * allrec_mat(S_,XI)          !rad + 3bod + dr
     +       - x_(S_X)  * n_e * allrec_mat(S_,X)           !rad + 3bod + dr
     +       + x_(S_XI) * x_(H_I) * nHtot * ct_rec(S_,XI,T) !H CT "rec"
     +       - x_(S_X)  * x_(H_I) * nHtot * ct_rec(S_,X,T)  !H CT "rec" 
     +       + x_(S_IX) * xHII * nHtot * ct_ion(S_,IX,T)    !H CT "ionization"
     +       - x_(S_X)  * xHII * nHtot * ct_ion(S_,X,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, S_X,S_,X)           !photoI+Auger
     +       - x_(S_X)  * Gamma(S_,X)                      !photoionization
     +       + x_(S_IX) * n_e * colion(S_,IX,T)            !col. ionization
     +       - x_(S_X)  * n_e * colion(S_,X,T)             !col. ionization
     +       + x_(S_XI) * x_(He_I) * nHtot * he_ct_r(S_,XI,T) !He CT "rec"
     +       - x_(S_X)  * x_(He_I) * nHtot * he_ct_r(S_,X,T)  !He CT "rec" 
     +       + x_(S_IX) * x_(He_II) * nHtot * he_ct_i(S_,IX,T)!He CT "ionization"
     +       - x_(S_X)  * x_(He_II) * nHtot * he_ct_i(S_,X,T) !He CT "ionization"

      dx_(S_XI)  = 
     +         x_(S_XII) * n_e * allrec_mat(S_,XII)          !rad + 3bod + dr
     +       - x_(S_XI)  * n_e * allrec_mat(S_,XI)           !rad + 3bod + dr
     +       + x_(S_XII) * x_(H_I) * nHtot * ct_rec(S_,XII,T) !H CT "rec"
     +       - x_(S_XI)  * x_(H_I) * nHtot * ct_rec(S_,XI,T)  !H CT "rec" 
     +       + x_(S_X)   * xHII * nHtot * ct_ion(S_,X,T)      !H CT "ionization"
     +       - x_(S_XI)  * xHII * nHtot * ct_ion(S_,XI,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, S_XI,S_,XI)           !photoI+Auger
     +       - x_(S_XI)  * Gamma(S_,XI)                      !photoionization
     +       + x_(S_X)   * n_e * colion(S_,X,T)              !col. ionization
     +       - x_(S_XI)  * n_e * colion(S_,XI,T)             !col. ionization
     +       + x_(S_XII) * x_(He_I) * nHtot * he_ct_r(S_,XII,T) !He CT "rec"
     +       - x_(S_XI)  * x_(He_I) * nHtot * he_ct_r(S_,XI,T)  !He CT "rec" 
     +       + x_(S_X)   * x_(He_II) * nHtot * he_ct_i(S_,X,T)  !He CT "ionization"
     +       - x_(S_XI)  * x_(He_II) * nHtot * he_ct_i(S_,XI,T) !He CT "ionization"

      dx_(S_XII) = 
     +         x_(S_XIII) * n_e * allrec_mat(S_,XIII)          !rad + 3bod + dr
     +       - x_(S_XII)  * n_e * allrec_mat(S_,XII)           !rad + 3bod + dr
     +       + x_(S_XIII) * x_(H_I) * nHtot * ct_rec(S_,XIII,T) !H CT "rec"
     +       - x_(S_XII)  * x_(H_I) * nHtot * ct_rec(S_,XII,T)  !H CT "rec" 
     +       + x_(S_XI)   * xHII * nHtot * ct_ion(S_,XI,T)      !H CT "ionization"
     +       - x_(S_XII)  * xHII * nHtot * ct_ion(S_,XII,T)     !H CT "ionization
     +       + Auger_pop_rate(nsize, x_, S_XII,S_,XII)           !photoI+Auger
     +       - x_(S_XII)  * Gamma(S_,XII)                      !photoionization
     +       + x_(S_XI)   * n_e * colion(S_,XI,T)              !col. ionization
     +       - x_(S_XII)  * n_e * colion(S_,XII,T)             !col. ionization
     +       + x_(S_XIII) * x_(He_I) * nHtot * he_ct_r(S_,XIII,T) !He CT "rec"
     +       - x_(S_XII)  * x_(He_I) * nHtot * he_ct_r(S_,XII,T)  !He CT "rec" 
     +       + x_(S_XI)   * x_(He_II) * nHtot * he_ct_i(S_,XI,T)  !He CT "ionization"
     +       - x_(S_XII)  * x_(He_II) * nHtot * he_ct_i(S_,XII,T) !He CT "ionization"

      dx_(S_XIII) =
     +         x_(S_XIV)  * n_e * allrec_mat(S_,XIV)           !rad + 3bod + dr
     +       - x_(S_XIII) * n_e * allrec_mat(S_,XIII)          !rad + 3bod + dr
     +       + x_(S_XIV)  * x_(H_I) * nHtot * ct_rec(S_,XIV,T)  !H CT "rec"
     +       - x_(S_XIII) * x_(H_I) * nHtot * ct_rec(S_,XIII,T) !H CT "rec" 
     +       + x_(S_XII)  * xHII * nHtot * ct_ion(S_,XII,T)     !H CT "ionization"
     +       - x_(S_XIII) * xHII * nHtot * ct_ion(S_,XIII,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, S_XIII,S_,XIII)           !photoI+Auger
     +       - x_(S_XIII) * Gamma(S_,XIII)                     !photoionization
     +       + x_(S_XII)  * n_e * colion(S_,XII,T)             !col. ionization
     +       - x_(S_XIII) * n_e * colion(S_,XIII,T)            !col. ionization
     +       + x_(S_XIV)  * x_(He_I) * nHtot * he_ct_r(S_,XIV,T)  !He CT "rec"
     +       - x_(S_XIII) * x_(He_I) * nHtot * he_ct_r(S_,XIII,T) !He CT "rec" 
     +       + x_(S_XII)  * x_(He_II) * nHtot * he_ct_i(S_,XII,T) !He CT "ionization"
     +       - x_(S_XIII) * x_(He_II) * nHtot * he_ct_i(S_,XIII,T)!He CT "ionization"

      dx_(S_XIV) = 
     +         x_(S_XV)   * n_e * allrec_mat(S_,XV)           !rad + 3bod + dr
     +       - x_(S_XIV)  * n_e * allrec_mat(S_,XIV)          !rad + 3bod + dr
     +       + x_(S_XV)   * x_(H_I) * nHtot * ct_rec(S_,XV,T)  !H CT "rec"
     +       - x_(S_XIV)  * x_(H_I) * nHtot * ct_rec(S_,XIV,T) !H CT "rec" 
     +       + x_(S_XIII) * xHII * nHtot * ct_ion(S_,XIII,T)   !H CT "ionization"
     +       - x_(S_XIV)  * xHII * nHtot * ct_ion(S_,XIV,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, S_XIV,S_,XIV)           !photoI+Auger
     +       - x_(S_XIV)  * Gamma(S_,XIV)                     !photoionization
     +       + x_(S_XIII) * n_e * colion(S_,XIII,T)           !col. ionization
     +       - x_(S_XIV)  * n_e * colion(S_,XIV,T)            !col. ionization
     +       + x_(S_XV)   * x_(He_I) * nHtot * he_ct_r(S_,XV,T)  !He CT "rec"
     +       - x_(S_XIV)  * x_(He_I) * nHtot * he_ct_r(S_,XIV,T) !He CT "rec" 
     +       + x_(S_XIII) * x_(He_II) * nHtot * he_ct_i(S_,XIII,T)!He CT "ionization"
     +       - x_(S_XIV)  * x_(He_II) * nHtot * he_ct_i(S_,XIV,T) !He CT "ionization"

      dx_(S_XV) = 
     +         x_(S_XVI) * n_e * allrec_mat(S_,XVI)          !rad + 3bod + dr
     +       - x_(S_XV)  * n_e * allrec_mat(S_,XV)           !rad + 3bod + dr
     +       + x_(S_XVI) * x_(H_I) * nHtot * ct_rec(S_,XVI,T) !H CT "rec"
     +       - x_(S_XV)  * x_(H_I) * nHtot * ct_rec(S_,XV,T)  !H CT "rec" 
     +       + x_(S_XIV) * xHII * nHtot * ct_ion(S_,XIV,T)    !H CT "ionization"
     +       - x_(S_XV)  * xHII * nHtot * ct_ion(S_,XV,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, S_XV,S_,XV)           !photoI+Auger
     +       - x_(S_XV)  * Gamma(S_,XV)                      !photoionization
     +       + x_(S_XIV) * n_e * colion(S_,XIV,T)            !col. ionization
     +       - x_(S_XV)  * n_e * colion(S_,XV,T)             !col. ionization
     +       + x_(S_XVI) * x_(He_I) * nHtot * he_ct_r(S_,XVI,T) !He CT "rec"
     +       - x_(S_XV)  * x_(He_I) * nHtot * he_ct_r(S_,XV,T)  !He CT "rec" 
     +       + x_(S_XIV) * x_(He_II) * nHtot * he_ct_i(S_,XIV,T)!He CT "ionization"
     +       - x_(S_XV)  * x_(He_II) * nHtot * he_ct_i(S_,XV,T) !He CT "ionization"

      dx_(S_XVI) =
     +         xSXVII * n_e * allrec_mat(S_,XVII)             !rad + 3bod + dr
     +       - x_(S_XVI)  * n_e * allrec_mat(S_,XVI)          !rad + 3bod + dr
     +       + xSXVII * x_(H_I) * nHtot * ct_rec(S_,XVII,T)    !H CT "rec"
     +       - x_(S_XVI)  * x_(H_I) * nHtot * ct_rec(S_,XVI,T) !H CT "rec" 
     +       + x_(S_XV)   * xHII * nHtot * ct_ion(S_,XV,T)     !H CT "ionization"
     +       - x_(S_XVI)  * xHII * nHtot * ct_ion(S_,XVI,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, S_XVI,S_,XVI)           !photoI+Auger
     +       - x_(S_XVI)  * Gamma(S_,XVI)                     !photoionization
     +       + x_(S_XV)   * n_e * colion(S_,XV,T)             !col. ionization
     +       - x_(S_XVI)  * n_e * colion(S_,XVI,T)            !col. ionization 
     +       + xSXVII * x_(He_I) * nHtot * he_ct_r(S_,XVII,T)    !He CT "rec"
     +       - x_(S_XVI)  * x_(He_I) * nHtot * he_ct_r(S_,XVI,T) !He CT "rec" 
     +       + x_(S_XV)   * x_(He_II) * nHtot * he_ct_i(S_,XV,T) !He CT "ionization"
     +       - x_(S_XVI)  * x_(He_II) * nHtot * he_ct_i(S_,XVI,T)!He CT "ionization"

c================
c     Iron
c================

      dx_(Fe_I)   =
     +         x_(Fe_II) * n_e * allrec_mat(Fe_,II)          !rad + 3bod + dr
     +       + x_(Fe_II) * x_(H_I) * nHtot * ct_rec(Fe_,II,T) !H CT "rec" 
     +       - x_(Fe_I)  * xHII * nHtot * ct_ion(Fe_,I,T)     !H CT "ionization"
     +       - x_(Fe_I)  * Gamma(Fe_,I)                      !photoionization
     +       - x_(Fe_I)  * n_e * colion(Fe_,I,T)             !col. ionization
     +       + x_(Fe_II) * x_(He_I) * nHtot * he_ct_r(Fe_,II,T) !He CT "rec" 
     +       - x_(Fe_I)  * x_(He_II) * nHtot * he_ct_i(Fe_,I,T) !He CT "ionization"

      dx_(Fe_II)  =
     +         x_(Fe_III) * n_e * allrec_mat(Fe_,III)          !rad + 3bod + dr
     +       - x_(Fe_II)  * n_e * allrec_mat(Fe_,II)           !rad + 3bod + dr
     +       + x_(Fe_III) * x_(H_I) * nHtot * ct_rec(Fe_,III,T) !H CT "rec"
     +       - x_(Fe_II)  * x_(H_I) * nHtot * ct_rec(Fe_,II,T)  !H CT "rec"
     +       + x_(Fe_I)   * xHII * nHtot * ct_ion(Fe_,I,T)      !H CT "ionization"
     +       - x_(Fe_II)  * xHII * nHtot * ct_ion(Fe_,II,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_II,Fe_,II)           !photoI+Auger
     +       - x_(Fe_II)  * Gamma(Fe_,II)                      !photoionization
     +       + x_(Fe_I)   * n_e * colion(Fe_,I,T)              !col. ionization
     +       - x_(Fe_II)  * n_e * colion(Fe_,II,T)             !col. ionization
     +       + x_(Fe_III) * x_(He_I) * nHtot * he_ct_r(Fe_,III,T) !He CT "rec"
     +       - x_(Fe_II)  * x_(He_I) * nHtot * he_ct_r(Fe_,II,T)  !He CT "rec"
     +       + x_(Fe_I)   * x_(He_II) * nHtot * he_ct_i(Fe_,I,T)  !He CT "ionization"
     +       - x_(Fe_II)  * x_(He_II) * nHtot * he_ct_i(Fe_,II,T) !He CT "ionization"

      dx_(Fe_III) =
     +         x_(Fe_IV)  * n_e * allrec_mat(Fe_,IV)           !rad + 3bod + dr
     +       - x_(Fe_III) * n_e * allrec_mat(Fe_,III)          !rad + 3bod + dr
     +       + x_(Fe_IV)  * x_(H_I) * nHtot * ct_rec(Fe_,IV,T)  !H CT "rec"
     +       - x_(Fe_III) * x_(H_I) * nHtot * ct_rec(Fe_,III,T) !H CT "rec" 
     +       + x_(Fe_II)  * xHII * nHtot * ct_ion(Fe_,II,T)     !H CT "ionization"
     +       - x_(Fe_III) * xHII * nHtot * ct_ion(Fe_,III,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_III,Fe_,III)           !photoI+Auger
     +       - x_(Fe_III) * Gamma(Fe_,III)                     !photoionization
     +       + x_(Fe_II)  * n_e * colion(Fe_,II,T)             !col. ionization
     +       - x_(Fe_III) * n_e * colion(Fe_,III,T)            !col. ionization
     +       + x_(Fe_IV)  * x_(He_I) * nHtot * he_ct_r(Fe_,IV,T)  !He CT "rec"
     +       - x_(Fe_III) * x_(He_I) * nHtot * he_ct_r(Fe_,III,T) !He CT "rec" 
     +       + x_(Fe_II)  * x_(He_II) * nHtot * he_ct_i(Fe_,II,T) !He CT "ionization"
     +       - x_(Fe_III) * x_(He_II) * nHtot * he_ct_i(Fe_,III,T)!He CT "ionization"

      dx_(Fe_IV)  =
     +         x_(Fe_V)   * n_e * allrec_mat(Fe_,V)           !rad + 3bod + dr
     +       - x_(Fe_IV)  * n_e * allrec_mat(Fe_,IV)          !rad + 3bod + dr
     +       + x_(Fe_V)   * x_(H_I) * nHtot * ct_rec(Fe_,V,T)  !H CT "rec"
     +       - x_(Fe_IV)  * x_(H_I) * nHtot * ct_rec(Fe_,IV,T) !H CT "rec" 
     +       + x_(Fe_III) * xHII * nHtot * ct_ion(Fe_,III,T)   !H CT "ionization"
     +       - x_(Fe_IV)  * xHII * nHtot * ct_ion(Fe_,IV,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_IV,Fe_,IV)           !photoI+Auger
     +       - x_(Fe_IV)  * Gamma(Fe_,IV)                     !photoionization
     +       + x_(Fe_III) * n_e * colion(Fe_,III,T)           !col. ionization
     +       - x_(Fe_IV)  * n_e * colion(Fe_,IV,T)            !col. ionization
     +       + x_(Fe_V)   * x_(He_I) * nHtot * he_ct_r(Fe_,V,T)  !He CT "rec"
     +       - x_(Fe_IV)  * x_(He_I) * nHtot * he_ct_r(Fe_,IV,T) !He CT "rec" 
     +       + x_(Fe_III) * x_(He_II) * nHtot * he_ct_i(Fe_,III,T)!He CT "ionization"
     +       - x_(Fe_IV)  * x_(He_II) * nHtot * he_ct_i(Fe_,IV,T) !He CT "ionization"

      dx_(Fe_V)  = 
     +         x_(Fe_VI) * n_e * allrec_mat(Fe_,VI)          !rad + 3bod + dr
     +       - x_(Fe_V)  * n_e * allrec_mat(Fe_,V)           !rad + 3bod + dr
     +       + x_(Fe_VI) * x_(H_I) * nHtot * ct_rec(Fe_,VI,T) !H CT "rec"
     +       - x_(Fe_V)  * x_(H_I) * nHtot * ct_rec(Fe_,V,T)  !H CT "rec" 
     +       + x_(Fe_IV) * xHII * nHtot * ct_ion(Fe_,IV,T)    !H CT "ionization"
     +       - x_(Fe_V)  * xHII * nHtot * ct_ion(Fe_,V,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_V,Fe_,V)           !photoI+Auger
     +       - x_(Fe_V)  * Gamma(Fe_,V)                      !photoionization
     +       + x_(Fe_IV) * n_e * colion(Fe_,IV,T)            !col. ionization
     +       - x_(Fe_V)  * n_e * colion(Fe_,V,T)             !col. ionization
     +       + x_(Fe_VI) * x_(He_I) * nHtot * he_ct_r(Fe_,VI,T) !He CT "rec"
     +       - x_(Fe_V)  * x_(He_I) * nHtot * he_ct_r(Fe_,V,T)  !He CT "rec" 
     +       + x_(Fe_IV) * x_(He_II) * nHtot * he_ct_i(Fe_,IV,T)!He CT "ionization"
     +       - x_(Fe_V)  * x_(He_II) * nHtot * he_ct_i(Fe_,V,T) !He CT "ionization"

      dx_(Fe_VI)  =
     +         x_(Fe_VII) * n_e * allrec_mat(Fe_,VII)          !rad + 3bod + dr
     +       - x_(Fe_VI)  * n_e * allrec_mat(Fe_,VI)           !rad + 3bod + dr
     +       + x_(Fe_VII) * x_(H_I) * nHtot * ct_rec(Fe_,VII,T) !H CT "rec"
     +       - x_(Fe_VI)  * x_(H_I) * nHtot * ct_rec(Fe_,VI,T)  !H CT "rec" 
     +       + x_(Fe_V)   * xHII * nHtot * ct_ion(Fe_,V,T)      !H CT "ionization"
     +       - x_(Fe_VI)  * xHII * nHtot * ct_ion(Fe_,VI,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_VI,Fe_,VI)           !photoI+Auger
     +       - x_(Fe_VI)  * Gamma(Fe_,VI)                      !photoionization
     +       + x_(Fe_V)   * n_e * colion(Fe_,V,T)              !col. ionization
     +       - x_(Fe_VI)  * n_e * colion(Fe_,VI,T)             !col. ionization
     +       + x_(Fe_VII) * x_(He_I) * nHtot * he_ct_r(Fe_,VII,T) !He CT "rec"
     +       - x_(Fe_VI)  * x_(He_I) * nHtot * he_ct_r(Fe_,VI,T)  !He CT "rec" 
     +       + x_(Fe_V)   * x_(He_II) * nHtot * he_ct_i(Fe_,V,T)  !He CT "ionization"
     +       - x_(Fe_VI)  * x_(He_II) * nHtot * he_ct_i(Fe_,VI,T) !He CT "ionization"

      dx_(Fe_VII) =
     +         x_(Fe_VIII) * n_e * allrec_mat(Fe_,VIII)        !rad + 3bod + dr
     +       - x_(Fe_VII)  * n_e * allrec_mat(Fe_,VII)         !rad + 3bod + dr
     +       + x_(Fe_VIII) * x_(H_I) * nHtot * ct_rec(Fe_,VIII,T) !H CT "rec"
     +       - x_(Fe_VII)  * x_(H_I) * nHtot * ct_rec(Fe_,VII,T)  !H CT "rec" 
     +       + x_(Fe_VI)   * xHII * nHtot * ct_ion(Fe_,VI,T)    !H CT "ionization"
     +       - x_(Fe_VII)  * xHII * nHtot * ct_ion(Fe_,VII,T)   !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_VII,Fe_,VII)           !photoI+Auger
     +       - x_(Fe_VII)  * Gamma(Fe_,VII)                    !photoionization
     +       + x_(Fe_VI)   * n_e * colion(Fe_,VI,T)            !col. ionization
     +       - x_(Fe_VII)  * n_e * colion(Fe_,VII,T)           !col. ionization
     +       + x_(Fe_VIII) * x_(He_I) * nHtot * he_ct_r(Fe_,VIII,T) !He CT "rec"
     +       - x_(Fe_VII)  * x_(He_I) * nHtot * he_ct_r(Fe_,VII,T)  !He CT "rec" 
     +       + x_(Fe_VI)   * x_(He_II) * nHtot * he_ct_i(Fe_,VI,T)  !He CT "ionization"
     +       - x_(Fe_VII)  * x_(He_II) * nHtot * he_ct_i(Fe_,VII,T) !He CT "ionization"

      dx_(Fe_VIII)=
     +         x_(Fe_IX)   * n_e * allrec_mat(Fe_,IX)          !rad + 3bod + dr
     +       - x_(Fe_VIII) * n_e * allrec_mat(Fe_,VIII)        !rad + 3bod + dr
     +       + x_(Fe_IX)   * x_(H_I) * nHtot * ct_rec(Fe_,IX,T)   !H CT "rec"
     +       - x_(Fe_VIII) * x_(H_I) * nHtot * ct_rec(Fe_,VIII,T) !H CT "rec" 
     +       + x_(Fe_VII)  * xHII * nHtot * ct_ion(Fe_,VII,T)   !H CT "ionization"
     +       - x_(Fe_VIII) * xHII * nHtot * ct_ion(Fe_,VIII,T)  !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_VIII,Fe_,VIII)           !photoI+Auger
     +       - x_(Fe_VIII) * Gamma(Fe_,VIII)                   !photoionization
     +       + x_(Fe_VII)  * n_e * colion(Fe_,VII,T)           !col. ionization
     +       - x_(Fe_VIII) * n_e * colion(Fe_,VIII,T)          !col. ionization
     +       + x_(Fe_IX)   * x_(He_I) * nHtot * he_ct_r(Fe_,IX,T)   !He CT "rec"
     +       - x_(Fe_VIII) * x_(He_I) * nHtot * he_ct_r(Fe_,VIII,T) !He CT "rec" 
     +       + x_(Fe_VII)  * x_(He_II) * nHtot * he_ct_i(Fe_,VII,T) !He CT "ionization"
     +       - x_(Fe_VIII) * x_(He_II) * nHtot * he_ct_i(Fe_,VIII,T)!He CT "ionization"

      dx_(Fe_IX)   =
     +         x_(Fe_X)    * n_e * allrec_mat(Fe_,X)           !rad + 3bod + dr
     +       - x_(Fe_IX)   * n_e * allrec_mat(Fe_,IX)          !rad + 3bod + dr
     +       + x_(Fe_X)    * x_(H_I) * nHtot * ct_rec(Fe_,X,T)  !H CT "rec"
     +       - x_(Fe_IX)   * x_(H_I) * nHtot * ct_rec(Fe_,IX,T) !H CT "rec" 
     +       + x_(Fe_VIII) * xHII * nHtot * ct_ion(Fe_,VIII,T)  !H CT "ionization"
     +       - x_(Fe_IX)   * xHII * nHtot * ct_ion(Fe_,IX,T)    !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_IX,Fe_,IX)           !photoI+Auger
     +       - x_(Fe_IX)   * Gamma(Fe_,IX)                     !photoionization
     +       + x_(Fe_VIII) * n_e * colion(Fe_,VIII,T)          !col. ionization
     +       - x_(Fe_IX)   * n_e * colion(Fe_,IX,T)            !col. ionization
     +       + x_(Fe_X)    * x_(He_I) * nHtot * he_ct_r(Fe_,X,T)  !He CT "rec"
     +       - x_(Fe_IX)   * x_(He_I) * nHtot * he_ct_r(Fe_,IX,T) !He CT "rec" 
     +       + x_(Fe_VIII) * x_(He_II) * nHtot * he_ct_i(Fe_,VIII,T)!He CT "ionization"
     +       - x_(Fe_IX)   * x_(He_II) * nHtot * he_ct_i(Fe_,IX,T)  !He CT "ionization"

      dx_(Fe_X)   = 
     +         x_(Fe_XI) * n_e * allrec_mat(Fe_,XI)          !rad + 3bod + dr
     +       - x_(Fe_X)  * n_e * allrec_mat(Fe_,X)           !rad + 3bod + dr
     +       + x_(Fe_XI) * x_(H_I) * nHtot * ct_rec(Fe_,XI,T) !H CT "rec"
     +       - x_(Fe_X)  * x_(H_I) * nHtot * ct_rec(Fe_,X,T)  !H CT "rec" 
     +       + x_(Fe_IX) * xHII * nHtot * ct_ion(Fe_,IX,T)    !H CT "ionization"
     +       - x_(Fe_X)  * xHII * nHtot * ct_ion(Fe_,X,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_X,Fe_,X)           !photoI+Auger
     +       - x_(Fe_X)  * Gamma(Fe_,X)                      !photoionization
     +       + x_(Fe_IX) * n_e * colion(Fe_,IX,T)            !col. ionization
     +       - x_(Fe_X)  * n_e * colion(Fe_,X,T)             !col. ionization
     +       + x_(Fe_XI) * x_(He_I) * nHtot * he_ct_r(Fe_,XI,T) !He CT "rec"
     +       - x_(Fe_X)  * x_(He_I) * nHtot * he_ct_r(Fe_,X,T)  !He CT "rec" 
     +       + x_(Fe_IX) * x_(He_II) * nHtot * he_ct_i(Fe_,IX,T)!He CT "ionization"
     +       - x_(Fe_X)  * x_(He_II) * nHtot * he_ct_i(Fe_,X,T) !He CT "ionization"

      dx_(Fe_XI)  = 
     +         x_(Fe_XII) * n_e * allrec_mat(Fe_,XII)          !rad + 3bod + dr
     +       - x_(Fe_XI)  * n_e * allrec_mat(Fe_,XI)           !rad + 3bod + dr
     +       + x_(Fe_XII) * x_(H_I) * nHtot * ct_rec(Fe_,XII,T) !H CT "rec"
     +       - x_(Fe_XI)  * x_(H_I) * nHtot * ct_rec(Fe_,XI,T)  !H CT "rec" 
     +       + x_(Fe_X)   * xHII * nHtot * ct_ion(Fe_,X,T)      !H CT "ionization"
     +       - x_(Fe_XI)  * xHII * nHtot * ct_ion(Fe_,XI,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_XI,Fe_,XI)           !photoI+Auger
     +       - x_(Fe_XI)  * Gamma(Fe_,XI)                      !photoionization
     +       + x_(Fe_X)   * n_e * colion(Fe_,X,T)              !col. ionization
     +       - x_(Fe_XI)  * n_e * colion(Fe_,XI,T)             !col. ionization
     +       + x_(Fe_XII) * x_(He_I) * nHtot * he_ct_r(Fe_,XII,T) !He CT "rec"
     +       - x_(Fe_XI)  * x_(He_I) * nHtot * he_ct_r(Fe_,XI,T)  !He CT "rec" 
     +       + x_(Fe_X)   * x_(He_II) * nHtot * he_ct_i(Fe_,X,T)  !He CT "ionization"
     +       - x_(Fe_XI)  * x_(He_II) * nHtot * he_ct_i(Fe_,XI,T) !He CT "ionization"

      dx_(Fe_XII) = 
     +         x_(Fe_XIII) * n_e * allrec_mat(Fe_,XIII)        !rad + 3bod + dr
     +       - x_(Fe_XII)  * n_e * allrec_mat(Fe_,XII)         !rad + 3bod + dr
     +       + x_(Fe_XIII) * x_(H_I) * nHtot * ct_rec(Fe_,XIII,T)!H CT "rec"
     +       - x_(Fe_XII)  * x_(H_I) * nHtot * ct_rec(Fe_,XII,T) !H CT "rec" 
     +       + x_(Fe_XI)   * xHII * nHtot * ct_ion(Fe_,XI,T)    !H CT "ionization"
     +       - x_(Fe_XII)  * xHII * nHtot * ct_ion(Fe_,XII,T)   !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_XII,Fe_,XII)           !photoI+Auger
     +       - x_(Fe_XII)  * Gamma(Fe_,XII)                    !photoionization
     +       + x_(Fe_XI)   * n_e * colion(Fe_,XI,T)            !col. ionization
     +       - x_(Fe_XII)  * n_e * colion(Fe_,XII,T)           !col. ionization
     +       + x_(Fe_XIII) * x_(He_I) * nHtot * he_ct_r(Fe_,XIII,T)!He CT "rec"
     +       - x_(Fe_XII)  * x_(He_I) * nHtot * he_ct_r(Fe_,XII,T) !He CT "rec" 
     +       + x_(Fe_XI)   * x_(He_II) * nHtot * he_ct_i(Fe_,XI,T) !He CT "ionization"
     +       - x_(Fe_XII)  * x_(He_II) * nHtot * he_ct_i(Fe_,XII,T)!He CT "ionization"

      dx_(Fe_XIII) =
     +         x_(Fe_XIV)  * n_e * allrec_mat(Fe_,XIV)         !rad + 3bod + dr
     +       - x_(Fe_XIII) * n_e * allrec_mat(Fe_,XIII)        !rad + 3bod + dr
     +       + x_(Fe_XIV)  * x_(H_I) * nHtot * ct_rec(Fe_,XIV,T) !H CT "rec"
     +       - x_(Fe_XIII) * x_(H_I) * nHtot * ct_rec(Fe_,XIII,T)!H CT "rec" 
     +       + x_(Fe_XII)  * xHII * nHtot * ct_ion(Fe_,XII,T)   !H CT "ionization"
     +       - x_(Fe_XIII) * xHII * nHtot * ct_ion(Fe_,XIII,T)  !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_XIII,Fe_,XIII)           !photoI+Auger
     +       - x_(Fe_XIII) * Gamma(Fe_,XIII)                   !photoionization
     +       + x_(Fe_XII)  * n_e * colion(Fe_,XII,T)           !col. ionization
     +       - x_(Fe_XIII) * n_e * colion(Fe_,XIII,T)          !col. ionization
     +       + x_(Fe_XIV)  * x_(He_I) * nHtot * he_ct_r(Fe_,XIV,T) !He CT "rec"
     +       - x_(Fe_XIII) * x_(He_I) * nHtot * he_ct_r(Fe_,XIII,T)!He CT "rec" 
     +       + x_(Fe_XII)  * x_(He_II) * nHtot * he_ct_i(Fe_,XII,T) !He CT "ionization"
     +       - x_(Fe_XIII) * x_(He_II) * nHtot * he_ct_i(Fe_,XIII,T)!He CT "ionization"

      dx_(Fe_XIV) = 
     +         x_(Fe_XV)   * n_e * allrec_mat(Fe_,XV)          !rad + 3bod + dr
     +       - x_(Fe_XIV)  * n_e * allrec_mat(Fe_,XIV)         !rad + 3bod + dr
     +       + x_(Fe_XV)   * x_(H_I) * nHtot * ct_rec(Fe_,XV,T)  !H CT "rec"
     +       - x_(Fe_XIV)  * x_(H_I) * nHtot * ct_rec(Fe_,XIV,T) !H CT "rec" 
     +       + x_(Fe_XIII) * xHII * nHtot * ct_ion(Fe_,XIII,T)  !H CT "ionization"
     +       - x_(Fe_XIV)  * xHII * nHtot * ct_ion(Fe_,XIV,T)   !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_XIV,Fe_,XIV)           !photoI+Auger
     +       - x_(Fe_XIV)  * Gamma(Fe_,XIV)                    !photoionization
     +       + x_(Fe_XIII) * n_e * colion(Fe_,XIII,T)          !col. ionization
     +       - x_(Fe_XIV)  * n_e * colion(Fe_,XIV,T)           !col. ionization
     +       + x_(Fe_XV)   * x_(He_I) * nHtot * he_ct_r(Fe_,XV,T)  !He CT "rec"
     +       - x_(Fe_XIV)  * x_(He_I) * nHtot * he_ct_r(Fe_,XIV,T) !He CT "rec" 
     +       + x_(Fe_XIII) * x_(He_II) * nHtot * he_ct_i(Fe_,XIII,T) !He CT "ionization"
     +       - x_(Fe_XIV)  * x_(He_II) * nHtot * he_ct_i(Fe_,XIV,T)  !He CT "ionization"

      dx_(Fe_XV) = 
     +         x_(Fe_XVI) * n_e * allrec_mat(Fe_,XVI)          !rad + 3bod + dr
     +       - x_(Fe_XV)  * n_e * allrec_mat(Fe_,XV)           !rad + 3bod + dr
     +       + x_(Fe_XVI) * x_(H_I) * nHtot * ct_rec(Fe_,XVI,T) !H CT "rec"
     +       - x_(Fe_XV)  * x_(H_I) * nHtot * ct_rec(Fe_,XV,T)  !H CT "rec" 
     +       + x_(Fe_XIV) * xHII * nHtot * ct_ion(Fe_,XIV,T)    !H CT "ionization"
     +       - x_(Fe_XV)  * xHII * nHtot * ct_ion(Fe_,XV,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_XV,Fe_,XV)           !photoI+Auger
     +       - x_(Fe_XV)  * Gamma(Fe_,XV)                      !photoionization
     +       + x_(Fe_XIV) * n_e * colion(Fe_,XIV,T)            !col. ionization
     +       - x_(Fe_XV)  * n_e * colion(Fe_,XV,T)             !col. ionization
     +       + x_(Fe_XVI) * x_(He_I) * nHtot * he_ct_r(Fe_,XVI,T) !He CT "rec"
     +       - x_(Fe_XV)  * x_(He_I) * nHtot * he_ct_r(Fe_,XV,T)  !He CT "rec" 
     +       + x_(Fe_XIV) * x_(He_II) * nHtot * he_ct_i(Fe_,XIV,T)!He CT "ionization"
     +       - x_(Fe_XV)  * x_(He_II) * nHtot * he_ct_i(Fe_,XV,T) !He CT "ionization"

      dx_(Fe_XVI) = 
     +         x_(Fe_XVII) * n_e * allrec_mat(Fe_,XVII)        !rad + 3bod + dr
     +       - x_(Fe_XVI)  * n_e * allrec_mat(Fe_,XVI)         !rad + 3bod + dr
     +       + x_(Fe_XVII) * x_(H_I) * nHtot * ct_rec(Fe_,XVII,T)!H CT "rec"
     +       - x_(Fe_XVI)  * x_(H_I) * nHtot * ct_rec(Fe_,XVI,T) !H CT "rec" 
     +       + x_(Fe_XV)   * xHII * nHtot * ct_ion(Fe_,XV,T)    !H CT "ionization"
     +       - x_(Fe_XVI)  * xHII * nHtot * ct_ion(Fe_,XVI,T)   !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_XVI,Fe_,XVI)           !photoI+Auger
     +       - x_(Fe_XVI)  * Gamma(Fe_,XVI)                    !photoionization
     +       + x_(Fe_XV)   * n_e * colion(Fe_,XV,T)            !col. ionization
     +       - x_(Fe_XVI)  * n_e * colion(Fe_,XVI,T)           !col. ionization
     +       + x_(Fe_XVII) * x_(He_I) * nHtot * he_ct_r(Fe_,XVII,T)!He CT "rec"
     +       - x_(Fe_XVI)  * x_(He_I) * nHtot * he_ct_r(Fe_,XVI,T) !He CT "rec" 
     +       + x_(Fe_XV)   * x_(He_II) * nHtot * he_ct_i(Fe_,XV,T) !He CT "ionization"
     +       - x_(Fe_XVI)  * x_(He_II) * nHtot * he_ct_i(Fe_,XVI,T)!He CT "ionization"


      dx_(Fe_XVII) = 
     +         x_(Fe_XVIII) * n_e * allrec_mat(Fe_,XVIII)      !rad + 3bod + dr
     +       - x_(Fe_XVII)  * n_e * allrec_mat(Fe_,XVII)       !rad + 3bod + dr
     +       + x_(Fe_XVIII) * x_(H_I) * nHtot * ct_rec(Fe_,XVIII,T) !H CT "rec"
     +       - x_(Fe_XVII)  * x_(H_I) * nHtot * ct_rec(Fe_,XVII,T)  !H CT "rec" 
     +       + x_(Fe_XVI)   * xHII * nHtot * ct_ion(Fe_,XVI,T)  !H CT "ionization"
     +       - x_(Fe_XVII)  * xHII * nHtot * ct_ion(Fe_,XVII,T) !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_XVII,Fe_,XVII)           !photoI+Auger
     +       - x_(Fe_XVII)  * Gamma(Fe_,XVII)                  !photoionization
     +       + x_(Fe_XVI)   * n_e * colion(Fe_,XVI,T)          !col. ionization
     +       - x_(Fe_XVII)  * n_e * colion(Fe_,XVII,T)         !col. ionization
     +       + x_(Fe_XVIII) * x_(He_I) * nHtot * he_ct_r(Fe_,XVIII,T) !He CT "rec"
     +       - x_(Fe_XVII)  * x_(He_I) * nHtot * he_ct_r(Fe_,XVII,T)  !He CT "rec" 
     +       + x_(Fe_XVI)   * x_(He_II) * nHtot * he_ct_i(Fe_,XVI,T)  !He CT "ionization"
     +       - x_(Fe_XVII)  * x_(He_II) * nHtot * he_ct_i(Fe_,XVII,T) !He CT "ionization"

      dx_(Fe_XVIII) = 
     +         x_(Fe_XIX)   * n_e * allrec_mat(Fe_,XIX)        !rad + 3bod + dr
     +       - x_(Fe_XVIII) * n_e * allrec_mat(Fe_,XVIII)      !rad + 3bod + dr
     +       + x_(Fe_XIX)   * x_(H_I) * nHtot * ct_rec(Fe_,XIX,T)   !H CT "rec"
     +       - x_(Fe_XVIII) * x_(H_I) * nHtot * ct_rec(Fe_,XVIII,T) !H CT "rec" 
     +       + x_(Fe_XVII)  * xHII * nHtot * ct_ion(Fe_,XVII,T) !H CT "ionization"
     +       - x_(Fe_XVIII) * xHII * nHtot * ct_ion(Fe_,XVIII,T)!H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_XVIII,Fe_,XVIII)           !photoI+Auger
     +       - x_(Fe_XVIII) * Gamma(Fe_,XVIII)                 !photoionization
     +       + x_(Fe_XVII)  * n_e * colion(Fe_,XVII,T)         !col. ionization
     +       - x_(Fe_XVIII) * n_e * colion(Fe_,XVIII,T)        !col. ionization
     +       + x_(Fe_XIX)   * x_(He_I) * nHtot * he_ct_r(Fe_,XIX,T)   !He CT "rec"
     +       - x_(Fe_XVIII) * x_(He_I) * nHtot * he_ct_r(Fe_,XVIII,T) !He CT "rec" 
     +       + x_(Fe_XVII)  * x_(He_II) * nHtot * he_ct_i(Fe_,XVII,T) !He CT "ionization"
     +       - x_(Fe_XVIII) * x_(He_II) * nHtot * he_ct_i(Fe_,XVIII,T)!He CT "ionization"

      dx_(Fe_XIX) = 
     +         x_(Fe_XX)    * n_e * allrec_mat(Fe_,XX)         !rad + 3bod + dr
     +       - x_(Fe_XIX)   * n_e * allrec_mat(Fe_,XIX)        !rad + 3bod + dr
     +       + x_(Fe_XX)    * x_(H_I) * nHtot * ct_rec(Fe_,XX,T)  !H CT "rec"
     +       - x_(Fe_XIX)   * x_(H_I) * nHtot * ct_rec(Fe_,XIX,T) !H CT "rec" 
     +       + x_(Fe_XVIII) * xHII * nHtot * ct_ion(Fe_,XVIII,T)!H CT "ionization"
     +       - x_(Fe_XIX)   * xHII * nHtot * ct_ion(Fe_,XIX,T)  !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_XIX,Fe_,XIX)           !photoI+Auger
     +       - x_(Fe_XIX)   * Gamma(Fe_,XIX)                   !photoionization
     +       + x_(Fe_XVIII) * n_e * colion(Fe_,XVIII,T)        !col. ionization
     +       - x_(Fe_XIX)   * n_e * colion(Fe_,XIX,T)          !col. ionization
     +       + x_(Fe_XX)    * x_(He_I) * nHtot * he_ct_r(Fe_,XX,T)  !He CT "rec"
     +       - x_(Fe_XIX)   * x_(He_I) * nHtot * he_ct_r(Fe_,XIX,T) !He CT "rec" 
     +       + x_(Fe_XVIII) * x_(He_II) * nHtot * he_ct_i(Fe_,XVIII,T)!He CT "ionization"
     +       - x_(Fe_XIX)   * x_(He_II) * nHtot * he_ct_i(Fe_,XIX,T)  !He CT "ionization"

      dx_(Fe_XX) = 
     +         x_(Fe_XXI) * n_e * allrec_mat(Fe_,XXI)          !rad + 3bod + dr
     +       - x_(Fe_XX)  * n_e * allrec_mat(Fe_,XX)           !rad + 3bod + dr
     +       + x_(Fe_XXI) * x_(H_I) * nHtot * ct_rec(Fe_,XXI,T) !H CT "rec"
     +       - x_(Fe_XX)  * x_(H_I) * nHtot * ct_rec(Fe_,XX,T)  !H CT "rec" 
     +       + x_(Fe_XIX) * xHII * nHtot * ct_ion(Fe_,XIX,T)    !H CT "ionization"
     +       - x_(Fe_XX)  * xHII * nHtot * ct_ion(Fe_,XX,T)     !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_XX,Fe_,XX)           !photoI+Auger
     +       - x_(Fe_XX)  * Gamma(Fe_,XX)                      !photoionization
     +       + x_(Fe_XIX) * n_e * colion(Fe_,XIX,T)            !col. ionization
     +       - x_(Fe_XX)  * n_e * colion(Fe_,XX,T)             !col. ionization
     +       + x_(Fe_XXI) * x_(He_I) * nHtot * he_ct_r(Fe_,XXI,T) !He CT "rec"
     +       - x_(Fe_XX)  * x_(He_I) * nHtot * he_ct_r(Fe_,XX,T)  !He CT "rec" 
     +       + x_(Fe_XIX) * x_(He_II) * nHtot * he_ct_i(Fe_,XIX,T)!He CT "ionization"
     +       - x_(Fe_XX)  * x_(He_II) * nHtot * he_ct_i(Fe_,XX,T) !He CT "ionization"

      dx_(Fe_XXI) = 
     +         x_(Fe_XXII) * n_e * allrec_mat(Fe_,XXII)        !rad + 3bod + dr
     +       - x_(Fe_XXI)  * n_e * allrec_mat(Fe_,XXI)         !rad + 3bod + dr
     +       + x_(Fe_XXII) * x_(H_I) * nHtot * ct_rec(Fe_,XXII,T) !H CT "rec"
     +       - x_(Fe_XXI)  * x_(H_I) * nHtot * ct_rec(Fe_,XXI,T)  !H CT "rec" 
     +       + x_(Fe_XX)   * xHII * nHtot * ct_ion(Fe_,XX,T)    !H CT "ionization"
     +       - x_(Fe_XXI)  * xHII * nHtot * ct_ion(Fe_,XXI,T)   !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_XXI,Fe_,XXI)           !photoI+Auger
     +       - x_(Fe_XXI)  * Gamma(Fe_,XXI)                    !photoionization
     +       + x_(Fe_XX)   * n_e * colion(Fe_,XX,T)            !col. ionization
     +       - x_(Fe_XXI)  * n_e * colion(Fe_,XXI,T)           !col. ionization
     +       + x_(Fe_XXII) * x_(He_I) * nHtot * he_ct_r(Fe_,XXII,T) !He CT "rec"
     +       - x_(Fe_XXI)  * x_(He_I) * nHtot * he_ct_r(Fe_,XXI,T)  !He CT "rec" 
     +       + x_(Fe_XX)   * x_(He_II) * nHtot * he_ct_i(Fe_,XX,T)  !He CT "ionization"
     +       - x_(Fe_XXI)  * x_(He_II) * nHtot * he_ct_i(Fe_,XXI,T) !He CT "ionization"

      dx_(Fe_XXII) = 
     +         x_(Fe_XXIII) * n_e * allrec_mat(Fe_,XXIII)      !rad + 3bod + dr
     +       - x_(Fe_XXII)  * n_e * allrec_mat(Fe_,XXII)       !rad + 3bod + dr
     +       + x_(Fe_XXIII) * x_(H_I) * nHtot * ct_rec(Fe_,XXIII,T) !H CT "rec"
     +       - x_(Fe_XXII)  * x_(H_I) * nHtot * ct_rec(Fe_,XXII,T)  !H CT "rec" 
     +       + x_(Fe_XXI)   * xHII * nHtot * ct_ion(Fe_,XXI,T)  !H CT "ionization"
     +       - x_(Fe_XXII)  * xHII * nHtot * ct_ion(Fe_,XXII,T) !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_XXII,Fe_,XXII)           !photoI+Auger
     +       - x_(Fe_XXII)  * Gamma(Fe_,XXII)                  !photoionization
     +       + x_(Fe_XXI)   * n_e * colion(Fe_,XXI,T)          !col. ionization
     +       - x_(Fe_XXII)  * n_e * colion(Fe_,XXII,T)         !col. ionization
     +       + x_(Fe_XXIII) * x_(He_I) * nHtot * he_ct_r(Fe_,XXIII,T) !He CT "rec"
     +       - x_(Fe_XXII)  * x_(He_I) * nHtot * he_ct_r(Fe_,XXII,T)  !He CT "rec" 
     +       + x_(Fe_XXI)   * x_(He_II) * nHtot * he_ct_i(Fe_,XXI,T)  !He CT "ionization"
     +       - x_(Fe_XXII)  * x_(He_II) * nHtot * he_ct_i(Fe_,XXII,T) !He CT "ionization"

      dx_(Fe_XXIII) = 
     +         x_(Fe_XXIV)  * n_e * allrec_mat(Fe_,XXIV)       !rad + 3bod + dr
     +       - x_(Fe_XXIII) * n_e * allrec_mat(Fe_,XXIII)      !rad + 3bod + dr
     +       + x_(Fe_XXIV)  * x_(H_I) * nHtot * ct_rec(Fe_,XXIV,T)  !H CT "rec"
     +       - x_(Fe_XXIII) * x_(H_I) * nHtot * ct_rec(Fe_,XXIII,T) !H CT "rec" 
     +       + x_(Fe_XXII)  * xHII * nHtot * ct_ion(Fe_,XXII,T) !H CT "ionization"
     +       - x_(Fe_XXIII) * xHII * nHtot * ct_ion(Fe_,XXIII,T)!H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_XXIII,Fe_,XXIII)           !photoI+Auger
     +       - x_(Fe_XXIII) * Gamma(Fe_,XXIII)                 !photoionization
     +       + x_(Fe_XXII)  * n_e * colion(Fe_,XXII,T)         !col. ionization
     +       - x_(Fe_XXIII) * n_e * colion(Fe_,XXIII,T)        !col. ionization
     +       + x_(Fe_XXIV)  * x_(He_I) * nHtot * he_ct_r(Fe_,XXIV,T)  !He CT "rec"
     +       - x_(Fe_XXIII) * x_(He_I) * nHtot * he_ct_r(Fe_,XXIII,T) !He CT "rec" 
     +       + x_(Fe_XXII)  * x_(He_II) * nHtot * he_ct_i(Fe_,XXII,T) !He CT "ionization"
     +       - x_(Fe_XXIII) * x_(He_II) * nHtot * he_ct_i(Fe_,XXIII,T)!He CT "ionization"

      dx_(Fe_XXIV) = 
     +         x_(Fe_XXV)   * n_e * allrec_mat(Fe_,XXV)        !rad + 3bod + dr
     +       - x_(Fe_XXIV)  * n_e * allrec_mat(Fe_,XXIV)       !rad + 3bod + dr
     +       + x_(Fe_XXV)   * x_(H_I) * nHtot * ct_rec(Fe_,XXV,T)  !H CT "rec"
     +       - x_(Fe_XXIV)  * x_(H_I) * nHtot * ct_rec(Fe_,XXIV,T) !H CT "rec" 
     +       + x_(Fe_XXIII) * xHII * nHtot * ct_ion(Fe_,XXIII,T)!H CT "ionization"
     +       - x_(Fe_XXIV)  * xHII * nHtot * ct_ion(Fe_,XXIV,T) !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_XXIV,Fe_,XXIV)           !photoI+Auger
     +       - x_(Fe_XXIV)  * Gamma(Fe_,XXIV)                  !photoionization
     +       + x_(Fe_XXIII) * n_e * colion(Fe_,XXIII,T)        !col. ionization
     +       - x_(Fe_XXIV)  * n_e * colion(Fe_,XXIV,T)         !col. ionization
     +       + x_(Fe_XXV)   * x_(He_I) * nHtot * he_ct_r(Fe_,XXV,T)  !He CT "rec"
     +       - x_(Fe_XXIV)  * x_(He_I) * nHtot * he_ct_r(Fe_,XXIV,T) !He CT "rec" 
     +       + x_(Fe_XXIII) * x_(He_II) * nHtot * he_ct_i(Fe_,XXIII,T)!He CT "ionization"
     +       - x_(Fe_XXIV)  * x_(He_II) * nHtot * he_ct_i(Fe_,XXIV,T) !He CT "ionization"

      dx_(Fe_XXV) = 
     +         x_(Fe_XXVI) * n_e * allrec_mat(Fe_,XXVI)        !rad + 3bod + dr
     +       - x_(Fe_XXV)  * n_e * allrec_mat(Fe_,XXV)         !rad + 3bod + dr
     +       + x_(Fe_XXVI) * x_(H_I) * nHtot * ct_rec(Fe_,XXVI,T) !H CT "rec"
     +       - x_(Fe_XXV)  * x_(H_I) * nHtot * ct_rec(Fe_,XXV,T)  !H CT "rec" 
     +       + x_(Fe_XXIV) * xHII * nHtot * ct_ion(Fe_,XXIV,T)  !H CT "ionization"
     +       - x_(Fe_XXV)  * xHII * nHtot * ct_ion(Fe_,XXV,T)   !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_XXV,Fe_,XXV)           !photoI+Auger
     +       - x_(Fe_XXV)  * Gamma(Fe_,XXV)                    !photoionization
     +       + x_(Fe_XXIV) * n_e * colion(Fe_,XXIV,T)          !col. ionization
     +       - x_(Fe_XXV)  * n_e * colion(Fe_,XXV,T)           !col. ionization
     +       + x_(Fe_XXVI) * x_(He_I) * nHtot * he_ct_r(Fe_,XXVI,T) !He CT "rec"
     +       - x_(Fe_XXV)  * x_(He_I) * nHtot * he_ct_r(Fe_,XXV,T)  !He CT "rec" 
     +       + x_(Fe_XXIV) * x_(He_II) * nHtot * he_ct_i(Fe_,XXIV,T)!He CT "ionization"
     +       - x_(Fe_XXV)  * x_(He_II) * nHtot * he_ct_i(Fe_,XXV,T) !He CT "ionization"

      dx_(Fe_XXVI) = 
     +         xFeXXVII    * n_e * allrec_mat(Fe_,XXVII)       !rad + 3bod + dr
     +       - x_(Fe_XXVI) * n_e * allrec_mat(Fe_,XXVI)        !rad + 3bod + dr
     +       + xFeXXVII    * x_(H_I) * nHtot * ct_rec(Fe_,XXVII,T) !H CT "rec"
     +       - x_(Fe_XXVI) * x_(H_I) * nHtot * ct_rec(Fe_,XXVI,T)  !H CT "rec" 
     +       + x_(Fe_XXV)  * xHII * nHtot * ct_ion(Fe_,XXV,T)   !H CT "ionization"
     +       - x_(Fe_XXVI) * xHII * nHtot * ct_ion(Fe_,XXVI,T)  !H CT "ionization"
     +       + Auger_pop_rate(nsize, x_, Fe_XXVI,Fe_,XXVI)           !photoI+Auger
     +       - x_(Fe_XXVI) * Gamma(Fe_,XXVI)                   !photoionization
     +       + x_(Fe_XXV)  * n_e * colion(Fe_,XXV,T)           !col. ionization
     +       - x_(Fe_XXVI) * n_e * colion(Fe_,XXVI,T)          !col. ionization
     +       + xFeXXVII    * x_(He_I) * nHtot * he_ct_r(Fe_,XXVII,T) !He CT "rec"
     +       - x_(Fe_XXVI) * x_(He_I) * nHtot * he_ct_r(Fe_,XXVI,T)  !He CT "rec" 
     +       + x_(Fe_XXV)  * x_(He_II) * nHtot * he_ct_i(Fe_,XXV,T)  !He CT "ionization"
     +       - x_(Fe_XXVI) * x_(He_II) * nHtot * he_ct_i(Fe_,XXVI,T) !He CT "ionization"

      return
      end



c  =========================================================================
c  =========================================================================
c  =========================================================================
c  =========================================================================
c  =========================================================================
c  =========================================================================
c  =========================================================================
c  =========================================================================
c  =========================================================================
c  =========================================================================


c  =========================================================================
      subroutine allrec_mat_set(re,allrec_vec)
c  =========================================================================
      implicit none

c     organizes the vector re() set by Hagai's recombination code
c     in a matrix such that (e.g.) allrec_mat(C_,IV) is the CIV->CIII
c     recombination coefficient.

      double precision re(300), allrec_vec(1:30,0:30)

      integer H_, He_, C_, N_, O_, Si_, S_ !for atomic numbers
      integer Ne_, Mg_, Al_, Ar_, Ca_, Fe_

      integer I,II,III,IV,V,VI,VII,VIII,IX,X,XI,XII,XIII
      integer XIV,XV,XVI,XVII  !for number of electrons
      integer XVIII, XIX, XX, XXI, XXII, XXIII, XXIV, XXV
      integer XXVI, XXVII, XXVIII, XXIX, XXX, XXXI

      integer j,k !indices for loops


      H_  = 1
      He_ = 2 
      C_  = 6
      N_  = 7
      O_  = 8
      Ne_ = 10
      Mg_ = 12
      Al_ = 13
      Si_ = 14
      S_  = 16
      Ar_ = 18
      Ca_ = 20
      Fe_ = 26

      I      = 0
      II     = 1
      III    = 2
      IV     = 3
      V      = 4
      VI     = 5
      VII    = 6
      VIII   = 7
      IX     = 8
      X      = 9
      XI     = 10
      XII    = 11
      XIII   = 12
      XIV    = 13
      XV     = 14
      XVI    = 15
      XVII   = 16
      XVIII  = 17
      XIX    = 18
      XX     = 19
      XXI    = 20
      XXII   = 21
      XXIII  = 22
      XXIV   = 23
      XXV    = 24
      XXVI   = 25
      XXVII  = 26
      XXVIII = 27
      XXIX   = 28
      XXX    = 29
      XXXI   = 30


c     initialize vector:
      do k=1,30
         do j=0,30
            allrec_vec(k,j) = 0
         enddo
      enddo

c     set values:
c     (no particular logic, thats just the way it is in Hagai's code)
      
c     Hydrogen
      allrec_vec(H_,II)     = RE(1)

c     Helium
      allrec_vec(He_,II)    = RE(2)
      allrec_vec(He_,III)   = RE(3)

c     Carbon
      allrec_vec(C_,II)     = RE(10)
      allrec_vec(C_,III)    = RE(11)
      allrec_vec(C_,IV)     = RE(12)
      allrec_vec(C_,V)      = RE(13)
      allrec_vec(C_,VI)     = RE(14)
      allrec_vec(C_,VII)    = RE(15)

c     Nitrogen
      allrec_vec(N_,II)     = RE(16)
      allrec_vec(N_,III)    = RE(17)
      allrec_vec(N_,IV)     = RE(18)
      allrec_vec(N_,V)      = RE(19)
      allrec_vec(N_,VI)     = RE(20)
      allrec_vec(N_,VII)    = RE(21)
      allrec_vec(N_,VIII)   = RE(97)

c     Oxygen
      allrec_vec(O_,II)     = RE(4)
      allrec_vec(O_,III)    = RE(5)
      allrec_vec(O_,IV)     = RE(6)
      allrec_vec(O_,V)      = RE(7)
      allrec_vec(O_,VI)     = RE(8)
      allrec_vec(O_,VII)    = RE(9)
      allrec_vec(O_,VIII)   = RE(98)
      allrec_vec(O_,IX)     = RE(99)

c     Neon
      allrec_vec(Ne_,II)    = RE(48)
      allrec_vec(Ne_,III)   = RE(49)
      allrec_vec(Ne_,IV)    = RE(50)
      allrec_vec(Ne_,V)     = RE(51)
      allrec_vec(Ne_,VI)    = RE(52)
      allrec_vec(Ne_,VII)   = RE(53)
      allrec_vec(Ne_,VIII)  = RE(54)
      allrec_vec(Ne_,IX)    = RE(55)
      allrec_vec(Ne_,X)     = RE(56)
      allrec_vec(Ne_,XI)    = RE(57)

c     Magnesiu
      allrec_vec(Mg_,II)    = RE(36)
      allrec_vec(Mg_,III)   = RE(37)
      allrec_vec(Mg_,IV)    = RE(38)
      allrec_vec(Mg_,V)     = RE(39)
      allrec_vec(Mg_,VI)    = RE(40)
      allrec_vec(Mg_,VII)   = RE(41)
      allrec_vec(Mg_,VIII)  = RE(42)
      allrec_vec(Mg_,IX)    = RE(43)
      allrec_vec(Mg_,X)     = RE(44)
      allrec_vec(Mg_,XI)    = RE(45)
      allrec_vec(Mg_,XII)   = RE(46)
      allrec_vec(Mg_,XIII)  = RE(47)

c     Aluminum
      allrec_vec(Al_,II)    = RE(131)
      allrec_vec(Al_,III)   = RE(132)
      allrec_vec(Al_,IV)    = RE(133)
      allrec_vec(Al_,V)     = RE(134)
      allrec_vec(Al_,VI)    = RE(135)
      allrec_vec(Al_,VII)   = RE(136)
      allrec_vec(Al_,VIII)  = RE(137)
      allrec_vec(Al_,IX)    = RE(138)
      allrec_vec(Al_,X)     = RE(139)
      allrec_vec(Al_,XI)    = RE(140)
      allrec_vec(Al_,XII)   = RE(141)
      allrec_vec(Al_,XIII)  = RE(142)
      allrec_vec(Al_,XIV)   = RE(143)

c     Silicon
      allrec_vec(Si_,II)    = RE(70)
      allrec_vec(Si_,III)   = RE(71)
      allrec_vec(Si_,IV)    = RE(72)
      allrec_vec(Si_,V)     = RE(73)
      allrec_vec(Si_,VI)    = RE(74)
      allrec_vec(Si_,VII)   = RE(75)
      allrec_vec(Si_,VIII)  = RE(76)
      allrec_vec(Si_,IX)    = RE(77)
      allrec_vec(Si_,X)     = RE(78)
      allrec_vec(Si_,XI)    = RE(79)
      allrec_vec(Si_,XII)   = RE(80)
      allrec_vec(Si_,XIII)  = RE(81)
      allrec_vec(Si_,XIV)   = RE(82)
      allrec_vec(Si_,XV)    = RE(83)

c     Sulfur
      allrec_vec(S_,II)     = RE(58)
      allrec_vec(S_,III)    = RE(59)
      allrec_vec(S_,IV)     = RE(60)
      allrec_vec(S_,V)      = RE(61)
      allrec_vec(S_,VI)     = RE(62)
      allrec_vec(S_,VII)    = RE(63)
      allrec_vec(S_,VIII)   = RE(64)
      allrec_vec(S_,IX)     = RE(65)
      allrec_vec(S_,X)      = RE(66)
      allrec_vec(S_,XI)     = RE(67)
      allrec_vec(S_,XII)    = RE(68)
      allrec_vec(S_,XIII)   = RE(69)
      allrec_vec(S_,XIV)    = RE(100)
      allrec_vec(S_,XV)     = RE(101)
      allrec_vec(S_,XVI)    = RE(102)
      allrec_vec(S_,XVII)   = RE(103)

c     Argon
      allrec_vec(Ar_,II)    = RE(181)
      allrec_vec(Ar_,III)   = RE(182)
      allrec_vec(Ar_,IV)    = RE(183)
      allrec_vec(Ar_,V)     = RE(184)
      allrec_vec(Ar_,VI)    = RE(185)
      allrec_vec(Ar_,VII)   = RE(186)
      allrec_vec(Ar_,VIII)  = RE(187)
      allrec_vec(Ar_,IX)    = RE(188)
      allrec_vec(Ar_,X)     = RE(189)
      allrec_vec(Ar_,XI)    = RE(190)
      allrec_vec(Ar_,XII)   = RE(191)
      allrec_vec(Ar_,XIII)  = RE(192)
      allrec_vec(Ar_,XIV)   = RE(193)
      allrec_vec(Ar_,XV)    = RE(194)
      allrec_vec(Ar_,XVI)   = RE(195)
      allrec_vec(Ar_,XVII)  = RE(196)
      allrec_vec(Ar_,XVIII) = RE(197)
      allrec_vec(Ar_,XIX)   = RE(198)


c     Calcium
      allrec_vec(Ca_,II)    = RE(201)
      allrec_vec(Ca_,III)   = RE(202)
      allrec_vec(Ca_,IV)    = RE(203)
      allrec_vec(Ca_,V)     = RE(204)
      allrec_vec(Ca_,VI)    = RE(205)
      allrec_vec(Ca_,VII)   = RE(206)
      allrec_vec(Ca_,VIII)  = RE(207)
      allrec_vec(Ca_,IX)    = RE(208)
      allrec_vec(Ca_,X)     = RE(209)
      allrec_vec(Ca_,XI)    = RE(210)
      allrec_vec(Ca_,XII)   = RE(211)
      allrec_vec(Ca_,XIII)  = RE(212)
      allrec_vec(Ca_,XIV)   = RE(213)
      allrec_vec(Ca_,XV)    = RE(214)
      allrec_vec(Ca_,XVI)   = RE(215)
      allrec_vec(Ca_,XVII)  = RE(216)
      allrec_vec(Ca_,XVIII) = RE(217)
      allrec_vec(Ca_,XIX)   = RE(218)
      allrec_vec(Ca_,XX)    = RE(219)
      allrec_vec(Ca_,XXI)   = RE(220)

c     Iron
      allrec_vec(Fe_,II)    = RE(22)
      allrec_vec(Fe_,III)   = RE(23)
      allrec_vec(Fe_,IV)    = RE(24)
      allrec_vec(Fe_,V)     = RE(25)
      allrec_vec(Fe_,VI)    = RE(26)
      allrec_vec(Fe_,VII)   = RE(27)
      allrec_vec(Fe_,VIII)  = RE(28)
      allrec_vec(Fe_,IX)    = RE(29)
      allrec_vec(Fe_,X)     = RE(30)
      allrec_vec(Fe_,XI)    = RE(31)
      allrec_vec(Fe_,XII)   = RE(32)
      allrec_vec(Fe_,XIII)  = RE(33)
      allrec_vec(Fe_,XIV)   = RE(34)
      allrec_vec(Fe_,XV)    = RE(35)
      allrec_vec(Fe_,XVI)   = RE(85)
      allrec_vec(Fe_,XVII)  = RE(86)
      allrec_vec(Fe_,XVIII) = RE(87)
      allrec_vec(Fe_,XIX)   = RE(88)
      allrec_vec(Fe_,XX)    = RE(89)
      allrec_vec(Fe_,XXI)   = RE(90)
      allrec_vec(Fe_,XXII)  = RE(91)
      allrec_vec(Fe_,XXIII) = RE(92)
      allrec_vec(Fe_,XXIV)  = RE(93)
      allrec_vec(Fe_,XXV)   = RE(94)
      allrec_vec(Fe_,XXVI)  = RE(95)
      allrec_vec(Fe_,XXVII) = RE(96)

      return
      end
