      function ct_ion(nz,state,T)
      implicit none
*      http://www-cfadc.phy.ornl.gov/astro/ps/data/

c state: as defined in FNC.f: electron deficiency relative to neutral state.
c for example, for CIV: ctrec(6,3,1e4)
 
      double precision CTi, T
      integer nz,state,state1
      double precision ct_ion

      state1 = state+1
      ct_ion = 0
c     if not fully ionized and with possible num of electrons:
      if ((state1.gt.0).and.(state1.le.nz)) then
c        call the Kingdon and Ferland routine:
         if (state1.lt.4) then   !in the CTIon database... 
            call  HCTIon(state1,nz,T,CTi)
            ct_ion = CTi
         endif
      endif
 
      return
      end

******************************************************************************
      subroutine HCTIon(ion,nelem,te,CTi)
*     ion is stage of ionization, 1 for atom
*     nelem is atomic number of element, 2 up to 30
*     Example:  O + H+ => O+ + H is HCTIon(1,8)
      double precision te, CTi
      integer ion , nelem
      common/CTIon/ CTIon(7,4,30)
*
*     local variables
      double precision tused 
      integer ipIon
*
      ipIon = ion
*
*     Make sure te is between temp. boundaries; set constant outside of range
      tused = max( te,CTIon(5,ipIon,nelem) )
      tused = min( tused , CTIon(6,ipIon,nelem) )
      tused = tused * 1e-4
*
*     the interpolation equation
      CTi = CTIon(1,ipIon,nelem)* 1e-9 * 
     1 (tused**CTIon(2,ipIon,nelem)) *
     2 (1. + 
     3 CTIon(3,ipIon,nelem) * exp(CTIon(4,ipIon,nelem)*tused) ) *
     4 exp(-CTIon(7,ipIon,nelem)/tused)
*
      if (tused.eq.0) then
         CTi = 0
      endif
      end

********************************************************************************
      block data ctdata1
*
      double precision CTIon
*     second dimension is ionization stage,
*     1=+0 for parent, etc
*     third dimension is atomic number of atom
      common/CTIon/ CTIon(7,4,30)
c      double precision CTRecomb
*     second dimension is ionization stage,
*     1=+1 for parent, etc
*     third dimension is atomic number of atom
c      common/CTRecomb/ CTRecomb(6,4,30)
*
*     local variables
      integer i
*
*     digital form of the fits to the charge transfer
*     ionization rate coefficients 
*
*     Note: First parameter is in units of 1e-9!
*     Note: Seventh parameter is in units of 1e4 K
*     ionization
      data (CTIon(i,1,3),i=1,7)/2.84e-3,1.99,375.54,-54.07,1e2,1e4,0.0/
      data (CTIon(i,2,3),i=1,7)/7*0./
      data (CTIon(i,3,3),i=1,7)/7*0./
      data (CTIon(i,1,4),i=1,7)/7*0./
      data (CTIon(i,2,4),i=1,7)/7*0./
      data (CTIon(i,3,4),i=1,7)/7*0./
      data (CTIon(i,1,5),i=1,7)/7*0./
      data (CTIon(i,2,5),i=1,7)/7*0./
      data (CTIon(i,3,5),i=1,7)/7*0./
      data (CTIon(i,1,6),i=1,7)/1.07e-6,3.15,176.43,-4.29,1e3,1e5,0.0/
      data (CTIon(i,2,6),i=1,7)/7*0./
      data (CTIon(i,3,6),i=1,7)/7*0./
      data (CTIon(i,1,7),i=1,7)/4.55e-3,-0.29,-0.92,-8.38,1e2,5e4,1.086/
      data (CTIon(i,2,7),i=1,7)/7*0./
      data (CTIon(i,3,7),i=1,7)/7*0./
      data (CTIon(i,1,8),i=1,7)/7.40e-2,0.47,24.37,-0.74,1e1,1e4,0.023/
      data (CTIon(i,2,8),i=1,7)/7*0./
      data (CTIon(i,3,8),i=1,7)/7*0./
      data (CTIon(i,1,9),i=1,7)/7*0./
      data (CTIon(i,2,9),i=1,7)/7*0./
      data (CTIon(i,3,9),i=1,7)/7*0./
      data (CTIon(i,1,10),i=1,7)/7*0./
      data (CTIon(i,2,10),i=1,7)/7*0./
      data (CTIon(i,3,10),i=1,7)/7*0./
      data (CTIon(i,1,11),i=1,7)/3.34e-6,9.31,2632.31,-3.04,1e3,2e4,0.0/
      data (CTIon(i,2,11),i=1,7)/7*0./
      data (CTIon(i,3,11),i=1,7)/7*0./
      data (CTIon(i,1,12),i=1,7)/9.76e-3,3.14,55.54,-1.12,5e3,3e4,0.0/
      data (CTIon(i,2,12),i=1,7)/7.60e-5,0.00,-1.97,-4.32,1e4,3e5,1.670/
      data (CTIon(i,3,12),i=1,7)/7*0./
      data (CTIon(i,1,13),i=1,7)/7*0./
      data (CTIon(i,2,13),i=1,7)/7*0./
      data (CTIon(i,3,13),i=1,7)/7*0./
      data (CTIon(i,1,14),i=1,7)/0.92,1.15,0.80,-0.24,1e3,2e5,0.0/
      data (CTIon(i,2,14),i=1,7)/2.26,7.36e-2,-0.43,-0.11,2e3,1e5,
     1 3.031/
      data (CTIon(i,3,14),i=1,7)/7*0./
      data (CTIon(i,1,15),i=1,7)/7*0./
      data (CTIon(i,2,15),i=1,7)/7*0./
      data (CTIon(i,3,15),i=1,7)/7*0./
      data (CTIon(i,1,16),i=1,7)/1.00e-5,0.00,0.00,0.00,1e3,1e4,0.0/
      data (CTIon(i,2,16),i=1,7)/7*0./
      data (CTIon(i,3,16),i=1,7)/7*0./
      data (CTIon(i,1,17),i=1,7)/7*0./
      data (CTIon(i,2,17),i=1,7)/7*0./
      data (CTIon(i,3,17),i=1,7)/7*0./
      data (CTIon(i,1,18),i=1,7)/7*0./
      data (CTIon(i,2,18),i=1,7)/7*0./
      data (CTIon(i,3,18),i=1,7)/7*0./
      data (CTIon(i,1,19),i=1,7)/7*0./
      data (CTIon(i,2,19),i=1,7)/7*0./
      data (CTIon(i,3,19),i=1,7)/7*0./
      data (CTIon(i,1,20),i=1,7)/7*0./
      data (CTIon(i,2,20),i=1,7)/7*0./
      data (CTIon(i,3,20),i=1,7)/7*0./
      data (CTIon(i,1,21),i=1,7)/7*0./
      data (CTIon(i,2,21),i=1,7)/7*0./
      data (CTIon(i,3,21),i=1,7)/7*0./
      data (CTIon(i,1,22),i=1,7)/7*0./
      data (CTIon(i,2,22),i=1,7)/7*0./
      data (CTIon(i,3,22),i=1,7)/7*0./
      data (CTIon(i,1,23),i=1,7)/7*0./
      data (CTIon(i,2,23),i=1,7)/7*0./
      data (CTIon(i,3,23),i=1,7)/7*0./
      data (CTIon(i,1,24),i=1,7)/7*0./
      data (CTIon(i,2,24),i=1,7)/4.39,0.61,-0.89,-3.56,1e3,3e4,3.349/
      data (CTIon(i,3,24),i=1,7)/7*0./
      data (CTIon(i,1,25),i=1,7)/7*0./
      data (CTIon(i,2,25),i=1,7)/2.83e-1,6.80e-3,6.44e-2,-9.70,1e3,3e4,
     1 2.368/
      data (CTIon(i,3,25),i=1,7)/7*0./
      data (CTIon(i,1,26),i=1,7)/7*0./
      data (CTIon(i,2,26),i=1,7)/2.10,7.72e-2,-0.41,-7.31,1e4,1e5,3.005/
      data (CTIon(i,3,26),i=1,7)/7*0./
      data (CTIon(i,1,27),i=1,7)/7*0./
      data (CTIon(i,2,27),i=1,7)/1.20e-2,3.49,24.41,-1.26,1e3,3e4,4.044/
      data (CTIon(i,3,27),i=1,7)/7*0./
      data (CTIon(i,1,28),i=1,7)/7*0./
      data (CTIon(i,2,28),i=1,7)/7*0./
      data (CTIon(i,3,28),i=1,7)/7*0./
      data (CTIon(i,1,29),i=1,7)/7*0./
      data (CTIon(i,2,29),i=1,7)/7*0./
      data (CTIon(i,3,29),i=1,7)/7*0./
      data (CTIon(i,1,30),i=1,7)/7*0./
      data (CTIon(i,2,30),i=1,7)/7*0./
      data (CTIon(i,3,30),i=1,7)/7*0./
*
*     digital form of the fits to the charge transfer
*     recombination rate coefficients (total)
*
*     Note: First parameter is in units of 1e-9!
*     recombination
c      data (CTRecomb(i,1,2),i=1,6)/7.47e-6,2.06,9.93,-3.89,6e3,1e5/
c      data (CTRecomb(i,2,2),i=1,6)/1.00e-5,0.,0.,0.,1e3,1e7/
c      data (CTRecomb(i,1,3),i=1,6)/6*0./
c      data (CTRecomb(i,2,3),i=1,6)/1.26,0.96,3.02,-0.65,1e3,3e4/
c      data (CTRecomb(i,3,3),i=1,6)/1.00e-5,0.,0.,0.,2e3,5e4/
c      data (CTRecomb(i,1,4),i=1,6)/6*0./
c      data (CTRecomb(i,2,4),i=1,6)/1.00e-5,0.,0.,0.,2e3,5e4/
c      data (CTRecomb(i,3,4),i=1,6)/1.00e-5,0.,0.,0.,2e3,5e4/
c      data (CTRecomb(i,4,4),i=1,6)/5.17,0.82,-0.69,-1.12,2e3,5e4/
c      data (CTRecomb(i,1,5),i=1,6)/6*0./
c      data (CTRecomb(i,2,5),i=1,6)/2.00e-2,0.,0.,0.,1e3,1e9/
c      data (CTRecomb(i,3,5),i=1,6)/1.00e-5,0.,0.,0.,2e3,5e4/
c      data (CTRecomb(i,4,5),i=1,6)/2.74,0.93,-0.61,-1.13,2e3,5e4/
c      data (CTRecomb(i,1,6),i=1,6)/4.88e-7,3.25,-1.12,-0.21,5.5e3,1e5/
c      data (CTRecomb(i,2,6),i=1,6)/1.67e-4,2.79,304.72,-4.07,5e3,5e4/
c      data (CTRecomb(i,3,6),i=1,6)/3.25,0.21,0.19,-3.29,1e3,1e5/
c      data (CTRecomb(i,4,6),i=1,6)/332.46,-0.11,-9.95e-1,-1.58e-3,1e1,
c     1 1e5/
c      data (CTRecomb(i,1,7),i=1,6)/1.01e-3,-0.29,-0.92,-8.38,1e2,5e4/
c      data (CTRecomb(i,2,7),i=1,6)/3.05e-1,0.60,2.65,-0.93,1e3,1e5/
c      data (CTRecomb(i,3,7),i=1,6)/4.54,0.57,-0.65,-0.89,1e1,1e5/
c      data (CTRecomb(i,4,7),i=1,6)/2.95,0.55,-0.39,-1.07,1e3,1e6/
c      data (CTRecomb(i,1,8),i=1,6)/1.04,3.15e-2,-0.61,-9.73,1e1,1e4/
c      data (CTRecomb(i,2,8),i=1,6)/1.04,0.27,2.02,-5.92,1e2,1e5/
c      data (CTRecomb(i,3,8),i=1,6)/3.98,0.26,0.56,-2.62,1e3,5e4/
c      data (CTRecomb(i,4,8),i=1,6)/2.52e-1,0.63,2.08,-4.16,1e3,3e4/
c      data (CTRecomb(i,1,9),i=1,6)/6*0./
c      data (CTRecomb(i,2,9),i=1,6)/1.00e-5,0.,0.,0.,2e3,5e4/
c      data (CTRecomb(i,3,9),i=1,6)/9.86,0.29,-0.21,-1.15,2e3,5e4/
c      data (CTRecomb(i,4,9),i=1,6)/7.15e-1,1.21,-0.70,-0.85,2e3,5e4/
c      data (CTRecomb(i,1,10),i=1,6)/6*0./
c      data (CTRecomb(i,2,10),i=1,6)/1.00e-5,0.,0.,0.,5e3,5e4/
c      data (CTRecomb(i,3,10),i=1,6)/14.73,4.52e-2,-0.84,-0.31,5e3,5e4/
c      data (CTRecomb(i,4,10),i=1,6)/6.47,0.54,3.59,-5.22,1e3,3e4/
c      data (CTRecomb(i,1,11),i=1,6)/6*0./
c      data (CTRecomb(i,2,11),i=1,6)/1.00e-5,0.,0.,0.,2e3,5e4/
c      data (CTRecomb(i,3,11),i=1,6)/1.33,1.15,1.20,-0.32,2e3,5e4/
c      data (CTRecomb(i,4,11),i=1,6)/1.01e-1,1.34,10.05,-6.41,2e3,5e4/
c      data (CTRecomb(i,1,12),i=1,6)/6*0./
c      data (CTRecomb(i,2,12),i=1,6)/8.58e-5,2.49e-3,2.93e-2,-4.33,1e3,
c     1 3e4/
c      data (CTRecomb(i,3,12),i=1,6)/6.49,0.53,2.82,-7.63,1e3,3e4/
c      data (CTRecomb(i,4,12),i=1,6)/6.36,0.55,3.86,-5.19,1e3,3e4/
c      data (CTRecomb(i,1,13),i=1,6)/6*0./
c      data (CTRecomb(i,2,13),i=1,6)/1.00e-5,0.,0.,0.,1e3,3e4/
c      data (CTRecomb(i,3,13),i=1,6)/7.11e-5,4.12,1.72e4,-22.24,1e3,3e4/
c      data (CTRecomb(i,4,13),i=1,6)/7.52e-1,0.77,6.24,-5.67,1e3,3e4/
c      data (CTRecomb(i,1,14),i=1,6)/6*0./
c      data (CTRecomb(i,2,14),i=1,6)/6.77,7.36e-2,-0.43,-0.11,5e2,1e5/
c      data (CTRecomb(i,3,14),i=1,6)/4.90e-1,-8.74e-2,-0.36,-0.79,1e3,
c     1 3e4/
c      data (CTRecomb(i,4,14),i=1,6)/7.58,0.37,1.06,-4.09,1e3,5e4/
c      data (CTRecomb(i,1,15),i=1,6)/6*0./
c      data (CTRecomb(i,2,15),i=1,6)/1.74e-4,3.84,36.06,-0.97,1e3,3e4/
c      data (CTRecomb(i,3,15),i=1,6)/9.46e-2,-5.58e-2,0.77,-6.43,1e3,3e4/
c      data (CTRecomb(i,4,15),i=1,6)/5.37,0.47,2.21,-8.52,1e3,3e4/
c      data (CTRecomb(i,1,16),i=1,6)/3.82e-7,11.10,2.57e4,-8.22,1e3,1e4/
c      data (CTRecomb(i,2,16),i=1,6)/1.00e-5,0.,0.,0.,1e3,3e4/
c      data (CTRecomb(i,3,16),i=1,6)/2.29,4.02e-2,1.59,-6.06,1e3,3e4/
c      data (CTRecomb(i,4,16),i=1,6)/6.44,0.13,2.69,-5.69,1e3,3e4/
c      data (CTRecomb(i,1,17),i=1,6)/6*0./
c      data (CTRecomb(i,2,17),i=1,6)/1.00e-5,0.,0.,0.,1e3,3e4/
c      data (CTRecomb(i,3,17),i=1,6)/1.88,0.32,1.77,-5.70,1e3,3e4/
c      data (CTRecomb(i,4,17),i=1,6)/7.27,0.29,1.04,-10.14,1e3,3e4/
c      data (CTRecomb(i,1,18),i=1,6)/6*0./
c      data (CTRecomb(i,2,18),i=1,6)/1.00e-5,0.,0.,0.,1e3,3e4/
c      data (CTRecomb(i,3,18),i=1,6)/4.57,0.27,-0.18,-1.57,1e3,3e4/
c      data (CTRecomb(i,4,18),i=1,6)/6.37,0.85,10.21,-6.22,1e3,3e4/
c      data (CTRecomb(i,1,19),i=1,6)/6*0./
c      data (CTRecomb(i,2,19),i=1,6)/1.00e-5,0.,0.,0.,1e3,3e4/
c      data (CTRecomb(i,3,19),i=1,6)/4.76,0.44,-0.56,-0.88,1e3,3e4/
c      data (CTRecomb(i,4,19),i=1,6)/1.00e-5,0.,0.,0.,1e3,3e4/
c      data (CTRecomb(i,1,20),i=1,6)/6*0./
c      data (CTRecomb(i,2,20),i=1,6)/0.,0.,0.,0.,1e1,1e9/
c      data (CTRecomb(i,3,20),i=1,6)/3.17e-2,2.12,12.06,-0.40,1e3,3e4/
c      data (CTRecomb(i,4,20),i=1,6)/2.68,0.69,-0.68,-4.47,1e3,3e4/
c      data (CTRecomb(i,1,21),i=1,6)/6*0./
c      data (CTRecomb(i,2,21),i=1,6)/0.,0.,0.,0.,1e1,1e9/
c      data (CTRecomb(i,3,21),i=1,6)/7.22e-3,2.34,411.50,-13.24,1e3,3e4/
c      data (CTRecomb(i,4,21),i=1,6)/1.20e-1,1.48,4.00,-9.33,1e3,3e4/
c      data (CTRecomb(i,1,22),i=1,6)/6*0./
c      data (CTRecomb(i,2,22),i=1,6)/0.,0.,0.,0.,1e1,1e9/
c      data (CTRecomb(i,3,22),i=1,6)/6.34e-1,6.87e-3,0.18,-8.04,1e3,3e4/
c      data (CTRecomb(i,4,22),i=1,6)/4.37e-3,1.25,40.02,-8.05,1e3,3e4/
c      data (CTRecomb(i,1,23),i=1,6)/6*0./
c      data (CTRecomb(i,2,23),i=1,6)/1.00e-5,0.,0.,0.,1e3,3e4/
c      data (CTRecomb(i,3,23),i=1,6)/5.12,-2.18e-2,-0.24,-0.83,1e3,3e4/
c      data (CTRecomb(i,4,23),i=1,6)/1.96e-1,-8.53e-3,0.28,-6.46,1e3,3e4/
c      data (CTRecomb(i,1,24),i=1,6)/6*0./
c      data (CTRecomb(i,2,24),i=1,6)/5.27e-1,0.61,-0.89,-3.56,1e3,3e4/
c      data (CTRecomb(i,3,24),i=1,6)/10.90,0.24,0.26,-11.94,1e3,3e4/
c      data (CTRecomb(i,4,24),i=1,6)/1.18,0.20,0.77,-7.09,1e3,3e4/
c      data (CTRecomb(i,1,25),i=1,6)/6*0./
c      data (CTRecomb(i,2,25),i=1,6)/1.65e-1,6.80e-3,6.44e-2,-9.70,1e3,
c     1 3e4/
c      data (CTRecomb(i,3,25),i=1,6)/14.20,0.34,-0.41,-1.19,1e3,3e4/
c      data (CTRecomb(i,4,25),i=1,6)/4.43e-1,0.91,10.76,-7.49,1e3,3e4/
c      data (CTRecomb(i,1,26),i=1,6)/6*0./
c      data (CTRecomb(i,2,26),i=1,6)/1.26,7.72e-2,-0.41,-7.31,1e3,1e5/
c      data (CTRecomb(i,3,26),i=1,6)/3.42,0.51,-2.06,-8.99,1e3,1e5/
c      data (CTRecomb(i,4,26),i=1,6)/14.60,3.57e-2,-0.92,-0.37,1e3,3e4/
c      data (CTRecomb(i,1,27),i=1,6)/6*0./
c      data (CTRecomb(i,2,27),i=1,6)/5.30,0.24,-0.91,-0.47,1e3,3e4/
c      data (CTRecomb(i,3,27),i=1,6)/3.26,0.87,2.85,-9.23,1e3,3e4/
c      data (CTRecomb(i,4,27),i=1,6)/1.03,0.58,-0.89,-0.66,1e3,3e4/
c      data (CTRecomb(i,1,28),i=1,6)/6*0./
c      data (CTRecomb(i,2,28),i=1,6)/1.05,1.28,6.54,-1.81,1e3,1e5/
c      data (CTRecomb(i,3,28),i=1,6)/9.73,0.35,0.90,-5.33,1e3,3e4/
c      data (CTRecomb(i,4,28),i=1,6)/6.14,0.25,-0.91,-0.42,1e3,3e4/
c      data (CTRecomb(i,1,29),i=1,6)/6*0./
c      data (CTRecomb(i,2,29),i=1,6)/1.47e-3,3.51,23.91,-0.93,1e3,3e4/
c      data (CTRecomb(i,3,29),i=1,6)/9.26,0.37,0.40,-10.73,1e3,3e4/
c      data (CTRecomb(i,4,29),i=1,6)/11.59,0.20,0.80,-6.62,1e3,3e4/
c      data (CTRecomb(i,1,30),i=1,6)/6*0./
c      data (CTRecomb(i,2,30),i=1,6)/1.00e-5,0.,0.,0.,1e3,3e4/
c      data (CTRecomb(i,3,30),i=1,6)/6.96e-4,4.24,26.06,-1.24,1e3,3e4/
c      data (CTRecomb(i,4,30),i=1,6)/1.33e-2,1.56,-0.92,-1.20,1e3,3e4/
*
      end
